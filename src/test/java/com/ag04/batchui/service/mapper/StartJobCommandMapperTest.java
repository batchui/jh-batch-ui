package com.ag04.batchui.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class StartJobCommandMapperTest {

    private StartJobCommandMapper startJobCommandMapper;

    @BeforeEach
    public void setUp() {
        startJobCommandMapper = new StartJobCommandMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(startJobCommandMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(startJobCommandMapper.fromId(null)).isNull();
    }
}
