package com.ag04.batchui.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class BatchJobExecutionMapperTest {

    private BatchJobExecutionMapper batchJobExecutionMapper;

    @BeforeEach
    public void setUp() {
        batchJobExecutionMapper = new BatchJobExecutionMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(batchJobExecutionMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(batchJobExecutionMapper.fromId(null)).isNull();
    }
}
