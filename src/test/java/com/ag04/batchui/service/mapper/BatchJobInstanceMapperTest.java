package com.ag04.batchui.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class BatchJobInstanceMapperTest {

    private BatchJobInstanceMapper batchJobInstanceMapper;

    @BeforeEach
    public void setUp() {
        batchJobInstanceMapper = new BatchJobInstanceMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(batchJobInstanceMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(batchJobInstanceMapper.fromId(null)).isNull();
    }
}
