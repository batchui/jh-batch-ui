package com.ag04.batchui.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class BatchStepExecutionMapperTest {

    private BatchStepExecutionMapper batchStepExecutionMapper;

    @BeforeEach
    public void setUp() {
        batchStepExecutionMapper = new BatchStepExecutionMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(batchStepExecutionMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(batchStepExecutionMapper.fromId(null)).isNull();
    }
}
