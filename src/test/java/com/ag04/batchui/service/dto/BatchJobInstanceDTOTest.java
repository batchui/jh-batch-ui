package com.ag04.batchui.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.ag04.batchui.web.rest.TestUtil;

public class BatchJobInstanceDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BatchJobInstanceDTO.class);
        BatchJobInstanceDTO batchJobInstanceDTO1 = new BatchJobInstanceDTO();
        batchJobInstanceDTO1.setId(1L);
        BatchJobInstanceDTO batchJobInstanceDTO2 = new BatchJobInstanceDTO();
        assertThat(batchJobInstanceDTO1).isNotEqualTo(batchJobInstanceDTO2);
        batchJobInstanceDTO2.setId(batchJobInstanceDTO1.getId());
        assertThat(batchJobInstanceDTO1).isEqualTo(batchJobInstanceDTO2);
        batchJobInstanceDTO2.setId(2L);
        assertThat(batchJobInstanceDTO1).isNotEqualTo(batchJobInstanceDTO2);
        batchJobInstanceDTO1.setId(null);
        assertThat(batchJobInstanceDTO1).isNotEqualTo(batchJobInstanceDTO2);
    }
}
