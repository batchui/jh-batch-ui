package com.ag04.batchui.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.ag04.batchui.web.rest.TestUtil;

public class StartJobCommandDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(StartJobCommandDTO.class);
        StartJobCommandDTO startJobCommandDTO1 = new StartJobCommandDTO();
        startJobCommandDTO1.setId(1L);
        StartJobCommandDTO startJobCommandDTO2 = new StartJobCommandDTO();
        assertThat(startJobCommandDTO1).isNotEqualTo(startJobCommandDTO2);
        startJobCommandDTO2.setId(startJobCommandDTO1.getId());
        assertThat(startJobCommandDTO1).isEqualTo(startJobCommandDTO2);
        startJobCommandDTO2.setId(2L);
        assertThat(startJobCommandDTO1).isNotEqualTo(startJobCommandDTO2);
        startJobCommandDTO1.setId(null);
        assertThat(startJobCommandDTO1).isNotEqualTo(startJobCommandDTO2);
    }
}
