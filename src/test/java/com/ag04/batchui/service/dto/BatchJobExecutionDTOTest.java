package com.ag04.batchui.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.ag04.batchui.web.rest.TestUtil;

public class BatchJobExecutionDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BatchJobExecutionDTO.class);
        BatchJobExecutionDTO batchJobExecutionDTO1 = new BatchJobExecutionDTO();
        batchJobExecutionDTO1.setId(1L);
        BatchJobExecutionDTO batchJobExecutionDTO2 = new BatchJobExecutionDTO();
        assertThat(batchJobExecutionDTO1).isNotEqualTo(batchJobExecutionDTO2);
        batchJobExecutionDTO2.setId(batchJobExecutionDTO1.getId());
        assertThat(batchJobExecutionDTO1).isEqualTo(batchJobExecutionDTO2);
        batchJobExecutionDTO2.setId(2L);
        assertThat(batchJobExecutionDTO1).isNotEqualTo(batchJobExecutionDTO2);
        batchJobExecutionDTO1.setId(null);
        assertThat(batchJobExecutionDTO1).isNotEqualTo(batchJobExecutionDTO2);
    }
}
