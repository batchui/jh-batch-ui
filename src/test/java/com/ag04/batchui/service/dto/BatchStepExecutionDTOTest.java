package com.ag04.batchui.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.ag04.batchui.web.rest.TestUtil;

public class BatchStepExecutionDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(BatchStepExecutionDTO.class);
        BatchStepExecutionDTO batchStepExecutionDTO1 = new BatchStepExecutionDTO();
        batchStepExecutionDTO1.setId(1L);
        BatchStepExecutionDTO batchStepExecutionDTO2 = new BatchStepExecutionDTO();
        assertThat(batchStepExecutionDTO1).isNotEqualTo(batchStepExecutionDTO2);
        batchStepExecutionDTO2.setId(batchStepExecutionDTO1.getId());
        assertThat(batchStepExecutionDTO1).isEqualTo(batchStepExecutionDTO2);
        batchStepExecutionDTO2.setId(2L);
        assertThat(batchStepExecutionDTO1).isNotEqualTo(batchStepExecutionDTO2);
        batchStepExecutionDTO1.setId(null);
        assertThat(batchStepExecutionDTO1).isNotEqualTo(batchStepExecutionDTO2);
    }
}
