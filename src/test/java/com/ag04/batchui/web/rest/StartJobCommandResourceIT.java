package com.ag04.batchui.web.rest;

import com.ag04.batchui.BatchuiAppTest;
import com.ag04.batchui.dbqueue.domain.QueueStatus;
import com.ag04.batchui.dbqueue.domain.StartJobCommand;
import com.ag04.batchui.dbqueue.repository.StartJobCommandRepository;
import com.ag04.batchui.service.StartJobCommandService;
import com.ag04.batchui.service.dto.StartJobCommandDTO;
import com.ag04.batchui.service.mapper.StartJobCommandMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.*;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link StartJobCommandResource} REST controller.
 */
@SpringBootTest(classes = BatchuiAppTest.class)
@AutoConfigureMockMvc
@WithMockUser
public class StartJobCommandResourceIT {

    private static final String DEFAULT_JOB_NAME = "AAAAAAAAAA";
    private static final String UPDATED_JOB_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_JOB_PARAMS = "AAAAAAAAAA";
    private static final String UPDATED_JOB_PARAMS = "BBBBBBBBBB";

    private static final QueueStatus DEFAULT_STATUS = QueueStatus.NOT_ATTEMPTED;
    private static final QueueStatus UPDATED_STATUS = QueueStatus.ERROR;

    private static final LocalDateTime DEFAULT_NEXT_ATTEMPT_TIME = LocalDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final LocalDateTime UPDATED_NEXT_ATTEMPT_TIME = LocalDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final LocalDateTime SMALLER_NEXT_ATTEMPT_TIME = LocalDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final Integer DEFAULT_ATTEMPT_COUNT = 1;
    private static final Integer UPDATED_ATTEMPT_COUNT = 2;
    private static final Integer SMALLER_ATTEMPT_COUNT = 1 - 1;

    private static final LocalDateTime DEFAULT_LAST_ATTEMPT_TIME = LocalDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final LocalDateTime UPDATED_LAST_ATTEMPT_TIME = LocalDateTime.now(ZoneId.systemDefault()).withNano(0);
    private static final LocalDateTime SMALLER_LAST_ATTEMPT_TIME = LocalDateTime.ofInstant(Instant.ofEpochMilli(-1L), ZoneOffset.UTC);

    private static final String DEFAULT_LAST_ATTEMPT_ERROR_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_LAST_ATTEMPT_ERROR_MESSAGE = "BBBBBBBBBB";

    @Autowired
    private StartJobCommandRepository startJobCommandRepository;

    @Autowired
    private StartJobCommandMapper startJobCommandMapper;

    @Autowired
    private StartJobCommandService startJobCommandService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restStartJobCommandMockMvc;

    private StartJobCommand startJobCommand;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static StartJobCommand createEntity(EntityManager em) {
        StartJobCommand startJobCommand = new StartJobCommand()
            .jobName(DEFAULT_JOB_NAME)
            .jobParams(DEFAULT_JOB_PARAMS)
            .status(DEFAULT_STATUS)
            .nextAttemptTime(DEFAULT_NEXT_ATTEMPT_TIME)
            .attemptCount(DEFAULT_ATTEMPT_COUNT)
            .lastAttemptTime(DEFAULT_LAST_ATTEMPT_TIME)
            .lastAttemptErrorMessage(DEFAULT_LAST_ATTEMPT_ERROR_MESSAGE);
        return startJobCommand;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static StartJobCommand createUpdatedEntity(EntityManager em) {
        StartJobCommand startJobCommand = new StartJobCommand()
            .jobName(UPDATED_JOB_NAME)
            .jobParams(UPDATED_JOB_PARAMS)
            .status(UPDATED_STATUS)
            .nextAttemptTime(UPDATED_NEXT_ATTEMPT_TIME)
            .attemptCount(UPDATED_ATTEMPT_COUNT)
            .lastAttemptTime(UPDATED_LAST_ATTEMPT_TIME)
            .lastAttemptErrorMessage(UPDATED_LAST_ATTEMPT_ERROR_MESSAGE);
        return startJobCommand;
    }

    @BeforeEach
    public void initTest() {
        startJobCommand = createEntity(em);
    }

    @Test
    @Transactional
    public void createStartJobCommand() throws Exception {
        int databaseSizeBeforeCreate = startJobCommandRepository.findAll().size();

        // Create the StartJobCommand
        StartJobCommandDTO startJobCommandDTO = startJobCommandMapper.toDto(startJobCommand);
        restStartJobCommandMockMvc.perform(post("/api/start-job-commands")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(startJobCommandDTO)))
            .andExpect(status().isCreated());

        // Validate the StartJobCommand in the database
        List<StartJobCommand> startJobCommandList = startJobCommandRepository.findAll();
        assertThat(startJobCommandList).hasSize(databaseSizeBeforeCreate + 1);
        StartJobCommand testStartJobCommand = startJobCommandList.get(startJobCommandList.size() - 1);
        assertThat(testStartJobCommand.getJobName()).isEqualTo(DEFAULT_JOB_NAME);
        assertThat(testStartJobCommand.getJobParams()).isEqualTo(DEFAULT_JOB_PARAMS);
        assertThat(testStartJobCommand.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testStartJobCommand.getNextAttemptTime()).isEqualTo(DEFAULT_NEXT_ATTEMPT_TIME);
        assertThat(testStartJobCommand.getAttemptCount()).isEqualTo(DEFAULT_ATTEMPT_COUNT);
        assertThat(testStartJobCommand.getLastAttemptTime()).isEqualTo(DEFAULT_LAST_ATTEMPT_TIME);
        assertThat(testStartJobCommand.getLastAttemptErrorMessage()).isEqualTo(DEFAULT_LAST_ATTEMPT_ERROR_MESSAGE);
    }

    @Test
    @Transactional
    public void createStartJobCommandWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = startJobCommandRepository.findAll().size();

        // Create the StartJobCommand with an existing ID
        startJobCommand.setId(1L);
        StartJobCommandDTO startJobCommandDTO = startJobCommandMapper.toDto(startJobCommand);

        // An entity with an existing ID cannot be created, so this API call must fail
        restStartJobCommandMockMvc.perform(post("/api/start-job-commands")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(startJobCommandDTO)))
            .andExpect(status().isBadRequest());

        // Validate the StartJobCommand in the database
        List<StartJobCommand> startJobCommandList = startJobCommandRepository.findAll();
        assertThat(startJobCommandList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void checkJobNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = startJobCommandRepository.findAll().size();
        // set the field null
        startJobCommand.setJobName(null);

        // Create the StartJobCommand, which fails.
        StartJobCommandDTO startJobCommandDTO = startJobCommandMapper.toDto(startJobCommand);

        restStartJobCommandMockMvc.perform(post("/api/start-job-commands")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(startJobCommandDTO)))
            .andExpect(status().isBadRequest());

        List<StartJobCommand> startJobCommandList = startJobCommandRepository.findAll();
        assertThat(startJobCommandList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllStartJobCommands() throws Exception {
        // Initialize the database
        startJobCommandRepository.saveAndFlush(startJobCommand);

        // Get all the startJobCommandList
        restStartJobCommandMockMvc.perform(get("/api/start-job-commands?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(startJobCommand.getId().intValue())))
            .andExpect(jsonPath("$.[*].jobName").value(hasItem(DEFAULT_JOB_NAME)))
            .andExpect(jsonPath("$.[*].jobParams").value(hasItem(DEFAULT_JOB_PARAMS.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())))
            .andExpect(jsonPath("$.[*].attemptCount").value(hasItem(DEFAULT_ATTEMPT_COUNT)))
            .andExpect(jsonPath("$.[*].lastAttemptErrorMessage").value(hasItem(DEFAULT_LAST_ATTEMPT_ERROR_MESSAGE)));
            //.andExpect(jsonPath("$.[*].nextAttemptTime").value(hasItem(sameInstant(DEFAULT_NEXT_ATTEMPT_TIME))))
            //.andExpect(jsonPath("$.[*].lastAttemptTime").value(hasItem(sameInstant(DEFAULT_LAST_ATTEMPT_TIME))));
    }
    
    @Test
    @Transactional
    public void getStartJobCommand() throws Exception {
        // Initialize the database
        startJobCommandRepository.saveAndFlush(startJobCommand);

        // Get the startJobCommand
        restStartJobCommandMockMvc.perform(get("/api/start-job-commands/{id}", startJobCommand.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(startJobCommand.getId().intValue()))
            .andExpect(jsonPath("$.jobName").value(DEFAULT_JOB_NAME))
            .andExpect(jsonPath("$.jobParams").value(DEFAULT_JOB_PARAMS.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()))
            .andExpect(jsonPath("$.attemptCount").value(DEFAULT_ATTEMPT_COUNT))
            .andExpect(jsonPath("$.lastAttemptErrorMessage").value(DEFAULT_LAST_ATTEMPT_ERROR_MESSAGE));
            //.andExpect(jsonPath("$.lastAttemptTime").value(sameInstant(DEFAULT_LAST_ATTEMPT_TIME)))
            //.andExpect(jsonPath("$.nextAttemptTime").value(sameInstant(DEFAULT_NEXT_ATTEMPT_TIME)));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultStartJobCommandShouldNotBeFound(String filter) throws Exception {
        restStartJobCommandMockMvc.perform(get("/api/start-job-commands?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restStartJobCommandMockMvc.perform(get("/api/start-job-commands/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingStartJobCommand() throws Exception {
        // Get the startJobCommand
        restStartJobCommandMockMvc.perform(get("/api/start-job-commands/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateStartJobCommand() throws Exception {
        // Initialize the database
        startJobCommandRepository.saveAndFlush(startJobCommand);

        int databaseSizeBeforeUpdate = startJobCommandRepository.findAll().size();

        // Update the startJobCommand
        StartJobCommand updatedStartJobCommand = startJobCommandRepository.findById(startJobCommand.getId()).get();
        // Disconnect from session so that the updates on updatedStartJobCommand are not directly saved in db
        em.detach(updatedStartJobCommand);
        updatedStartJobCommand
            .jobName(UPDATED_JOB_NAME)
            .jobParams(UPDATED_JOB_PARAMS)
            .status(UPDATED_STATUS)
            .nextAttemptTime(UPDATED_NEXT_ATTEMPT_TIME)
            .attemptCount(UPDATED_ATTEMPT_COUNT)
            .lastAttemptTime(UPDATED_LAST_ATTEMPT_TIME)
            .lastAttemptErrorMessage(UPDATED_LAST_ATTEMPT_ERROR_MESSAGE);
        StartJobCommandDTO startJobCommandDTO = startJobCommandMapper.toDto(updatedStartJobCommand);

        restStartJobCommandMockMvc.perform(put("/api/start-job-commands")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(startJobCommandDTO)))
            .andExpect(status().isOk());

        // Validate the StartJobCommand in the database
        List<StartJobCommand> startJobCommandList = startJobCommandRepository.findAll();
        assertThat(startJobCommandList).hasSize(databaseSizeBeforeUpdate);
        StartJobCommand testStartJobCommand = startJobCommandList.get(startJobCommandList.size() - 1);
        assertThat(testStartJobCommand.getJobName()).isEqualTo(UPDATED_JOB_NAME);
        assertThat(testStartJobCommand.getJobParams()).isEqualTo(UPDATED_JOB_PARAMS);
        assertThat(testStartJobCommand.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testStartJobCommand.getNextAttemptTime()).isEqualTo(UPDATED_NEXT_ATTEMPT_TIME);
        assertThat(testStartJobCommand.getAttemptCount()).isEqualTo(UPDATED_ATTEMPT_COUNT);
        assertThat(testStartJobCommand.getLastAttemptTime()).isEqualTo(UPDATED_LAST_ATTEMPT_TIME);
        assertThat(testStartJobCommand.getLastAttemptErrorMessage()).isEqualTo(UPDATED_LAST_ATTEMPT_ERROR_MESSAGE);
    }

    @Test
    @Transactional
    public void updateNonExistingStartJobCommand() throws Exception {
        int databaseSizeBeforeUpdate = startJobCommandRepository.findAll().size();

        // Create the StartJobCommand
        StartJobCommandDTO startJobCommandDTO = startJobCommandMapper.toDto(startJobCommand);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restStartJobCommandMockMvc.perform(put("/api/start-job-commands")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(startJobCommandDTO)))
            .andExpect(status().isBadRequest());

        // Validate the StartJobCommand in the database
        List<StartJobCommand> startJobCommandList = startJobCommandRepository.findAll();
        assertThat(startJobCommandList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteStartJobCommand() throws Exception {
        // Initialize the database
        startJobCommandRepository.saveAndFlush(startJobCommand);

        int databaseSizeBeforeDelete = startJobCommandRepository.findAll().size();

        // Delete the startJobCommand
        restStartJobCommandMockMvc.perform(delete("/api/start-job-commands/{id}", startJobCommand.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<StartJobCommand> startJobCommandList = startJobCommandRepository.findAll();
        assertThat(startJobCommandList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
