package com.ag04.batchui.web.rest;

import com.ag04.batchui.BatchuiAppTest;
import com.ag04.batchui.domain.BatchJobExecution;
import com.ag04.batchui.domain.BatchStepExecution;
import com.ag04.batchui.repository.BatchStepExecutionRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZonedDateTime;
import java.time.ZoneOffset;
import java.time.ZoneId;
import java.util.List;

import static com.ag04.batchui.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BatchStepExecutionResource} REST controller.
 */
@SpringBootTest(classes = BatchuiAppTest.class)
@AutoConfigureMockMvc
@WithMockUser
public class BatchStepExecutionResourceIT {

    private static final Long DEFAULT_VERSION = 1L;
    private static final Long UPDATED_VERSION = 2L;

    private static final String DEFAULT_STEP_NAME = "AAAAAAAAAA";
    private static final String UPDATED_STEP_NAME = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_START_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_START_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_END_TIME = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_END_TIME = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final Long DEFAULT_COMMIT_COUNT = 1L;
    private static final Long UPDATED_COMMIT_COUNT = 2L;

    private static final Long DEFAULT_READ_COUNT = 1L;
    private static final Long UPDATED_READ_COUNT = 2L;

    private static final Long DEFAULT_FILTER_COUNT = 1L;
    private static final Long UPDATED_FILTER_COUNT = 2L;

    private static final Long DEFAULT_WRITE_COUNT = 1L;
    private static final Long UPDATED_WRITE_COUNT = 2L;

    private static final Long DEFAULT_READ_SKIP_COUNT = 1L;
    private static final Long UPDATED_READ_SKIP_COUNT = 2L;

    private static final Long DEFAULT_WRITE_SKIP_COUNT = 1L;
    private static final Long UPDATED_WRITE_SKIP_COUNT = 2L;

    private static final Long DEFAULT_PROCESS_SKIP_COUNT = 1L;
    private static final Long UPDATED_PROCESS_SKIP_COUNT = 2L;

    private static final Long DEFAULT_ROLLBACK_COUNT = 1L;
    private static final Long UPDATED_ROLLBACK_COUNT = 2L;

    private static final String DEFAULT_EXIT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_EXIT_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_EXIT_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_EXIT_MESSAGE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_LAST_UPDATED = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_LAST_UPDATED = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    @Autowired
    private BatchStepExecutionRepository batchStepExecutionRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBatchStepExecutionMockMvc;

    private BatchStepExecution batchStepExecution;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BatchStepExecution createEntity(EntityManager em) {
        return createEntity(em, null);
    }

    public static BatchStepExecution createEntity(EntityManager em, BatchJobExecution bje) {
        if (bje == null) {
            bje = BatchJobExecutionResourceIT.createEntity(em);
            em.persist(bje);
        }
        BatchStepExecution batchStepExecution = new BatchStepExecution()
            .version(DEFAULT_VERSION)
            .stepName(DEFAULT_STEP_NAME)
            .startTime(DEFAULT_START_TIME)
            .endTime(DEFAULT_END_TIME)
            .status(DEFAULT_STATUS)
            .commitCount(DEFAULT_COMMIT_COUNT)
            .readCount(DEFAULT_READ_COUNT)
            .filterCount(DEFAULT_FILTER_COUNT)
            .writeCount(DEFAULT_WRITE_COUNT)
            .readSkipCount(DEFAULT_READ_SKIP_COUNT)
            .writeSkipCount(DEFAULT_WRITE_SKIP_COUNT)
            .processSkipCount(DEFAULT_PROCESS_SKIP_COUNT)
            .rollbackCount(DEFAULT_ROLLBACK_COUNT)
            .exitCode(DEFAULT_EXIT_CODE)
            .exitMessage(DEFAULT_EXIT_MESSAGE)
            .lastUpdated(DEFAULT_LAST_UPDATED);
        batchStepExecution.setBatchJobExecution(bje);
        return batchStepExecution;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BatchStepExecution createUpdatedEntity(EntityManager em) {
        BatchStepExecution batchStepExecution = new BatchStepExecution()
            .version(UPDATED_VERSION)
            .stepName(UPDATED_STEP_NAME)
            .startTime(UPDATED_START_TIME)
            .endTime(UPDATED_END_TIME)
            .status(UPDATED_STATUS)
            .commitCount(UPDATED_COMMIT_COUNT)
            .readCount(UPDATED_READ_COUNT)
            .filterCount(UPDATED_FILTER_COUNT)
            .writeCount(UPDATED_WRITE_COUNT)
            .readSkipCount(UPDATED_READ_SKIP_COUNT)
            .writeSkipCount(UPDATED_WRITE_SKIP_COUNT)
            .processSkipCount(UPDATED_PROCESS_SKIP_COUNT)
            .rollbackCount(UPDATED_ROLLBACK_COUNT)
            .exitCode(UPDATED_EXIT_CODE)
            .exitMessage(UPDATED_EXIT_MESSAGE)
            .lastUpdated(UPDATED_LAST_UPDATED);
        return batchStepExecution;
    }

    @BeforeEach
    public void initTest() {
        batchStepExecution = createEntity(em);
    }

    @Test
    @Transactional
    public void getAllBatchStepExecutions() throws Exception {
        // Initialize the database
        batchStepExecutionRepository.saveAndFlush(batchStepExecution);

        // Get all the batchStepExecutionList
        restBatchStepExecutionMockMvc.perform(get("/api/batch-step-executions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(batchStepExecution.getId().intValue())))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION.intValue())))
            .andExpect(jsonPath("$.[*].stepName").value(hasItem(DEFAULT_STEP_NAME)))
            .andExpect(jsonPath("$.[*].startTime").value(hasItem(sameInstant(DEFAULT_START_TIME))))
            .andExpect(jsonPath("$.[*].endTime").value(hasItem(sameInstant(DEFAULT_END_TIME))))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].commitCount").value(hasItem(DEFAULT_COMMIT_COUNT.intValue())))
            .andExpect(jsonPath("$.[*].readCount").value(hasItem(DEFAULT_READ_COUNT.intValue())))
            .andExpect(jsonPath("$.[*].filterCount").value(hasItem(DEFAULT_FILTER_COUNT.intValue())))
            .andExpect(jsonPath("$.[*].writeCount").value(hasItem(DEFAULT_WRITE_COUNT.intValue())))
            .andExpect(jsonPath("$.[*].readSkipCount").value(hasItem(DEFAULT_READ_SKIP_COUNT.intValue())))
            .andExpect(jsonPath("$.[*].writeSkipCount").value(hasItem(DEFAULT_WRITE_SKIP_COUNT.intValue())))
            .andExpect(jsonPath("$.[*].processSkipCount").value(hasItem(DEFAULT_PROCESS_SKIP_COUNT.intValue())))
            .andExpect(jsonPath("$.[*].rollbackCount").value(hasItem(DEFAULT_ROLLBACK_COUNT.intValue())))
            .andExpect(jsonPath("$.[*].exitCode").value(hasItem(DEFAULT_EXIT_CODE)))
            .andExpect(jsonPath("$.[*].exitMessage").value(hasItem(DEFAULT_EXIT_MESSAGE)))
            .andExpect(jsonPath("$.[*].lastUpdated").value(hasItem(sameInstant(DEFAULT_LAST_UPDATED))));
    }
    
    @Test
    @Transactional
    public void getBatchStepExecution() throws Exception {
        // Initialize the database
        batchStepExecutionRepository.saveAndFlush(batchStepExecution);

        // Get the batchStepExecution
        restBatchStepExecutionMockMvc.perform(get("/api/batch-step-executions/{id}", batchStepExecution.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(batchStepExecution.getId().intValue()))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION.intValue()))
            .andExpect(jsonPath("$.stepName").value(DEFAULT_STEP_NAME))
            .andExpect(jsonPath("$.startTime").value(sameInstant(DEFAULT_START_TIME)))
            .andExpect(jsonPath("$.endTime").value(sameInstant(DEFAULT_END_TIME)))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.commitCount").value(DEFAULT_COMMIT_COUNT.intValue()))
            .andExpect(jsonPath("$.readCount").value(DEFAULT_READ_COUNT.intValue()))
            .andExpect(jsonPath("$.filterCount").value(DEFAULT_FILTER_COUNT.intValue()))
            .andExpect(jsonPath("$.writeCount").value(DEFAULT_WRITE_COUNT.intValue()))
            .andExpect(jsonPath("$.readSkipCount").value(DEFAULT_READ_SKIP_COUNT.intValue()))
            .andExpect(jsonPath("$.writeSkipCount").value(DEFAULT_WRITE_SKIP_COUNT.intValue()))
            .andExpect(jsonPath("$.processSkipCount").value(DEFAULT_PROCESS_SKIP_COUNT.intValue()))
            .andExpect(jsonPath("$.rollbackCount").value(DEFAULT_ROLLBACK_COUNT.intValue()))
            .andExpect(jsonPath("$.exitCode").value(DEFAULT_EXIT_CODE))
            .andExpect(jsonPath("$.exitMessage").value(DEFAULT_EXIT_MESSAGE))
            .andExpect(jsonPath("$.lastUpdated").value(sameInstant(DEFAULT_LAST_UPDATED)));
    }

    @Test
    @Transactional
    public void getNonExistingBatchStepExecution() throws Exception {
        // Get the batchStepExecution
        restBatchStepExecutionMockMvc.perform(get("/api/batch-step-executions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

}
