package com.ag04.batchui.web.rest;

import com.ag04.batchui.BatchuiAppTest;
import com.ag04.batchui.domain.*;
import com.ag04.batchui.repository.BatchJobExecutionRepository;
import com.ag04.batchui.service.BatchJobExecutionService;
import com.ag04.batchui.service.dto.BatchJobExecutionDTO;
import com.ag04.batchui.service.mapper.BatchJobExecutionMapper;
import com.ag04.batchui.service.BatchJobExecutionQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BatchJobExecutionResource} REST controller.
 */
@SpringBootTest(classes = BatchuiAppTest.class)

@AutoConfigureMockMvc
@WithMockUser
public class BatchJobExecutionResourceIT {

    private static final Long SMALLER_JOB_EXECUTION_ID = 1L - 1L;

    private static final Long DEFAULT_VERSION = 1L;
    private static final Long UPDATED_VERSION = 2L;
    private static final Long SMALLER_VERSION = 1L - 1L;

    private static final Instant DEFAULT_CREATE_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_CREATE_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_START_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_START_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final Instant DEFAULT_END_TIME = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_END_TIME = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_EXIT_CODE = "AAAAAAAAAA";
    private static final String UPDATED_EXIT_CODE = "BBBBBBBBBB";

    private static final String DEFAULT_EXIT_MESSAGE = "AAAAAAAAAA";
    private static final String UPDATED_EXIT_MESSAGE = "BBBBBBBBBB";

    private static final Instant DEFAULT_LAST_UPDATED = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_LAST_UPDATED = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    private static final String DEFAULT_JOB_CONFIGURATION_LOCATION = "AAAAAAAAAA";
    private static final String UPDATED_JOB_CONFIGURATION_LOCATION = "BBBBBBBBBB";

    @Autowired
    private BatchJobExecutionRepository batchJobExecutionRepository;

    @Autowired
    private BatchJobExecutionMapper batchJobExecutionMapper;

    @Autowired
    private BatchJobExecutionService batchJobExecutionService;

    @Autowired
    private BatchJobExecutionQueryService batchJobExecutionQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBatchJobExecutionMockMvc;

    private BatchJobExecution batchJobExecution;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BatchJobExecution createEntity(EntityManager em) {
        return createEntity(em, null);
    }

    public static BatchJobExecution createEntity(EntityManager em, BatchJobInstance bji) {
        if (bji == null) {
            bji = BatchJobInstanceResourceIT.createEntity(em);
            em.persist(bji);
        }
        BatchJobExecutionParams params = BatchJobExecutionParamsStubFactory.createEntity();
        BatchJobExecutionContext context = BatchJobExecutionContextStubFactory.createEntity();

        BatchJobExecution batchJobExecution = new BatchJobExecution()
                .batchJobInstance(bji)
                .version(DEFAULT_VERSION)
                .createTime(DEFAULT_CREATE_TIME)
                .startTime(DEFAULT_START_TIME)
                .endTime(DEFAULT_END_TIME)
                .status(DEFAULT_STATUS)
                .exitCode(DEFAULT_EXIT_CODE)
                .exitMessage(DEFAULT_EXIT_MESSAGE)
                .lastUpdated(DEFAULT_LAST_UPDATED)
                .jobConfigurationLocation(DEFAULT_JOB_CONFIGURATION_LOCATION)
                .batchJobExecutionParams(params)
                .batchJobExecutionContexts(context);
        BatchStepExecution bse = BatchStepExecutionResourceIT.createEntity(em, batchJobExecution);
        batchJobExecution.addBatchStepExecution(bse);

        return batchJobExecution;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BatchJobExecution createUpdatedEntity(EntityManager em) {
        BatchJobExecution batchJobExecution = new BatchJobExecution()
            .version(UPDATED_VERSION)
            .createTime(UPDATED_CREATE_TIME)
            .startTime(UPDATED_START_TIME)
            .endTime(UPDATED_END_TIME)
            .status(UPDATED_STATUS)
            .exitCode(UPDATED_EXIT_CODE)
            .exitMessage(UPDATED_EXIT_MESSAGE)
            .lastUpdated(UPDATED_LAST_UPDATED)
            .jobConfigurationLocation(UPDATED_JOB_CONFIGURATION_LOCATION);
        return batchJobExecution;
    }

    @BeforeEach
    public void initTest() {
        batchJobExecution = createEntity(em);
    }

    /**
     * Set to ignore because endpoint was changed.
     *
     * @throws Exception
     */
    @Test
    @Disabled
    @Transactional
    public void createBatchJobExecution() throws Exception {
        int databaseSizeBeforeCreate = batchJobExecutionRepository.findAll().size();

        // Create the BatchJobExecution
        BatchJobExecutionDTO batchJobExecutionDTO = batchJobExecutionMapper.toDto(batchJobExecution);
        restBatchJobExecutionMockMvc.perform(post("/api/batch-job-executions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(batchJobExecutionDTO)))
            .andExpect(status().isCreated());

        // Validate the BatchJobExecution in the database
        List<BatchJobExecution> batchJobExecutionList = batchJobExecutionRepository.findAll();
        assertThat(batchJobExecutionList).hasSize(databaseSizeBeforeCreate + 1);
        BatchJobExecution testBatchJobExecution = batchJobExecutionList.get(batchJobExecutionList.size() - 1);
        assertThat(testBatchJobExecution.getVersion()).isEqualTo(DEFAULT_VERSION);
        assertThat(testBatchJobExecution.getCreateTime()).isEqualTo(DEFAULT_CREATE_TIME);
        assertThat(testBatchJobExecution.getStartTime()).isEqualTo(DEFAULT_START_TIME);
        assertThat(testBatchJobExecution.getEndTime()).isEqualTo(DEFAULT_END_TIME);
        assertThat(testBatchJobExecution.getStatus()).isEqualTo(DEFAULT_STATUS);
        assertThat(testBatchJobExecution.getExitCode()).isEqualTo(DEFAULT_EXIT_CODE);
        assertThat(testBatchJobExecution.getExitMessage()).isEqualTo(DEFAULT_EXIT_MESSAGE);
        assertThat(testBatchJobExecution.getLastUpdated()).isEqualTo(DEFAULT_LAST_UPDATED);
        assertThat(testBatchJobExecution.getJobConfigurationLocation()).isEqualTo(DEFAULT_JOB_CONFIGURATION_LOCATION);
    }

    /**
     * Set to ignore since POST endpoint was changed
     * @throws Exception
     */
    @Disabled
    @Test
    @Transactional
    public void createBatchJobExecutionWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = batchJobExecutionRepository.findAll().size();

        // Create the BatchJobExecution with an existing ID
        batchJobExecution.setId(1L);
        BatchJobExecutionDTO batchJobExecutionDTO = batchJobExecutionMapper.toDto(batchJobExecution);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBatchJobExecutionMockMvc.perform(post("/api/batch-job-executions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(batchJobExecutionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BatchJobExecution in the database
        List<BatchJobExecution> batchJobExecutionList = batchJobExecutionRepository.findAll();
        assertThat(batchJobExecutionList).hasSize(databaseSizeBeforeCreate);
    }

    /**
     * Set to ignore since POST endpoint was changed
     * @throws Exception
     */
    @Disabled
    @Test
    @Transactional
    public void checkCreateTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = batchJobExecutionRepository.findAll().size();
        // set the field null
        batchJobExecution.setCreateTime(null);

        // Create the BatchJobExecution, which fails.
        BatchJobExecutionDTO batchJobExecutionDTO = batchJobExecutionMapper.toDto(batchJobExecution);

        restBatchJobExecutionMockMvc.perform(post("/api/batch-job-executions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(batchJobExecutionDTO)))
            .andExpect(status().isBadRequest());

        List<BatchJobExecution> batchJobExecutionList = batchJobExecutionRepository.findAll();
        assertThat(batchJobExecutionList).hasSize(databaseSizeBeforeTest);
    }

    /**
     * Set to ignore since POST endpoint was changed
     * @throws Exception
     */
    @Test
    @Disabled
    @Transactional
    public void checkStartTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = batchJobExecutionRepository.findAll().size();
        // set the field null
        batchJobExecution.setStartTime(null);

        // Create the BatchJobExecution, which fails.
        BatchJobExecutionDTO batchJobExecutionDTO = batchJobExecutionMapper.toDto(batchJobExecution);

        restBatchJobExecutionMockMvc.perform(post("/api/batch-job-executions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(batchJobExecutionDTO)))
            .andExpect(status().isBadRequest());

        List<BatchJobExecution> batchJobExecutionList = batchJobExecutionRepository.findAll();
        assertThat(batchJobExecutionList).hasSize(databaseSizeBeforeTest);
    }

    /**
     * Set to ignore since POST endpoint was changed
     * @throws Exception
     */
    @Test
    @Disabled
    @Transactional
    public void checkEndTimeIsRequired() throws Exception {
        int databaseSizeBeforeTest = batchJobExecutionRepository.findAll().size();
        // set the field null
        batchJobExecution.setEndTime(null);

        // Create the BatchJobExecution, which fails.
        BatchJobExecutionDTO batchJobExecutionDTO = batchJobExecutionMapper.toDto(batchJobExecution);

        restBatchJobExecutionMockMvc.perform(post("/api/batch-job-executions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(batchJobExecutionDTO)))
            .andExpect(status().isBadRequest());

        List<BatchJobExecution> batchJobExecutionList = batchJobExecutionRepository.findAll();
        assertThat(batchJobExecutionList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutions() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList
        restBatchJobExecutionMockMvc.perform(get("/api/batch-job-executions?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(batchJobExecution.getId().intValue())))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION.intValue())))
            .andExpect(jsonPath("$.[*].createTime").value(hasItem(DEFAULT_CREATE_TIME.toString())))
            .andExpect(jsonPath("$.[*].startTime").value(hasItem(DEFAULT_START_TIME.toString())))
            .andExpect(jsonPath("$.[*].endTime").value(hasItem(DEFAULT_END_TIME.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].exitCode").value(hasItem(DEFAULT_EXIT_CODE)))
            .andExpect(jsonPath("$.[*].exitMessage").value(hasItem(DEFAULT_EXIT_MESSAGE)))
            .andExpect(jsonPath("$.[*].lastUpdated").value(hasItem(DEFAULT_LAST_UPDATED.toString())))
            .andExpect(jsonPath("$.[*].jobConfigurationLocation").value(hasItem(DEFAULT_JOB_CONFIGURATION_LOCATION)))
            .andExpect(jsonPath("$.[*].batchStepExecutions").doesNotExist())
            .andExpect(jsonPath("$.[*].batchJobExecutionParams").doesNotExist())
            .andExpect(jsonPath("$.[*].batchJobExecutionContexts").doesNotExist())
        ;
    }
    
    @Test
    @Transactional
    public void getBatchJobExecution() throws Exception {
        // Initialize the database
        BatchStepExecution bse = BatchStepExecutionResourceIT.createEntity(em, batchJobExecution);
        batchJobExecution.addBatchStepExecution(bse);
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get the batchJobExecution
        ResultActions result = restBatchJobExecutionMockMvc.perform(get("/api/batch-job-executions/{id}", batchJobExecution.getId()));
        result.andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(batchJobExecution.getId().intValue()))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION.intValue()))
            .andExpect(jsonPath("$.createTime").value(DEFAULT_CREATE_TIME.toString()))
            .andExpect(jsonPath("$.startTime").value(DEFAULT_START_TIME.toString()))
            .andExpect(jsonPath("$.endTime").value(DEFAULT_END_TIME.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS))
            .andExpect(jsonPath("$.exitCode").value(DEFAULT_EXIT_CODE))
            .andExpect(jsonPath("$.exitMessage").value(DEFAULT_EXIT_MESSAGE))
            .andExpect(jsonPath("$.lastUpdated").value(DEFAULT_LAST_UPDATED.toString()))
            .andExpect(jsonPath("$.jobConfigurationLocation").value(DEFAULT_JOB_CONFIGURATION_LOCATION))
            .andExpect(jsonPath("$.batchJobInstanceId").value(batchJobExecution.getBatchJobInstance().getId()))
            .andExpect(jsonPath("$.batchJobExecutionParams").isArray())
            .andExpect(jsonPath("$.batchJobExecutionParams").isNotEmpty())
            .andExpect(jsonPath("$.batchJobExecutionContexts").isArray())
            .andExpect(jsonPath("$.batchJobExecutionContexts").isNotEmpty())
            .andExpect(jsonPath("$.batchStepExecutions").isArray())
            .andExpect(jsonPath("$.batchStepExecutions").isNotEmpty())
        ;
    }


    @Test
    @Transactional
    public void getBatchJobExecutionsByIdFiltering() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        Long id = batchJobExecution.getId();

        defaultBatchJobExecutionShouldBeFound("id.equals=" + id);
        defaultBatchJobExecutionShouldNotBeFound("id.notEquals=" + id);

        defaultBatchJobExecutionShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultBatchJobExecutionShouldNotBeFound("id.greaterThan=" + id);

        defaultBatchJobExecutionShouldBeFound("id.lessThanOrEqual=" + id);
        defaultBatchJobExecutionShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByVersionIsEqualToSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where version equals to DEFAULT_VERSION
        defaultBatchJobExecutionShouldBeFound("version.equals=" + DEFAULT_VERSION);

        // Get all the batchJobExecutionList where version equals to UPDATED_VERSION
        defaultBatchJobExecutionShouldNotBeFound("version.equals=" + UPDATED_VERSION);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByVersionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where version not equals to DEFAULT_VERSION
        defaultBatchJobExecutionShouldNotBeFound("version.notEquals=" + DEFAULT_VERSION);

        // Get all the batchJobExecutionList where version not equals to UPDATED_VERSION
        defaultBatchJobExecutionShouldBeFound("version.notEquals=" + UPDATED_VERSION);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByVersionIsInShouldWork() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where version in DEFAULT_VERSION or UPDATED_VERSION
        defaultBatchJobExecutionShouldBeFound("version.in=" + DEFAULT_VERSION + "," + UPDATED_VERSION);

        // Get all the batchJobExecutionList where version equals to UPDATED_VERSION
        defaultBatchJobExecutionShouldNotBeFound("version.in=" + UPDATED_VERSION);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByVersionIsNullOrNotNull() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where version is not null
        defaultBatchJobExecutionShouldBeFound("version.specified=true");

        // Get all the batchJobExecutionList where version is null
        defaultBatchJobExecutionShouldNotBeFound("version.specified=false");
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByVersionIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where version is greater than or equal to DEFAULT_VERSION
        defaultBatchJobExecutionShouldBeFound("version.greaterThanOrEqual=" + DEFAULT_VERSION);

        // Get all the batchJobExecutionList where version is greater than or equal to UPDATED_VERSION
        defaultBatchJobExecutionShouldNotBeFound("version.greaterThanOrEqual=" + UPDATED_VERSION);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByVersionIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where version is less than or equal to DEFAULT_VERSION
        defaultBatchJobExecutionShouldBeFound("version.lessThanOrEqual=" + DEFAULT_VERSION);

        // Get all the batchJobExecutionList where version is less than or equal to SMALLER_VERSION
        defaultBatchJobExecutionShouldNotBeFound("version.lessThanOrEqual=" + SMALLER_VERSION);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByVersionIsLessThanSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where version is less than DEFAULT_VERSION
        defaultBatchJobExecutionShouldNotBeFound("version.lessThan=" + DEFAULT_VERSION);

        // Get all the batchJobExecutionList where version is less than UPDATED_VERSION
        defaultBatchJobExecutionShouldBeFound("version.lessThan=" + UPDATED_VERSION);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByVersionIsGreaterThanSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where version is greater than DEFAULT_VERSION
        defaultBatchJobExecutionShouldNotBeFound("version.greaterThan=" + DEFAULT_VERSION);

        // Get all the batchJobExecutionList where version is greater than SMALLER_VERSION
        defaultBatchJobExecutionShouldBeFound("version.greaterThan=" + SMALLER_VERSION);
    }


    @Test
    @Transactional
    public void getAllBatchJobExecutionsByCreateTimeIsEqualToSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where createTime equals to DEFAULT_CREATE_TIME
        defaultBatchJobExecutionShouldBeFound("createTime.equals=" + DEFAULT_CREATE_TIME);

        // Get all the batchJobExecutionList where createTime equals to UPDATED_CREATE_TIME
        defaultBatchJobExecutionShouldNotBeFound("createTime.equals=" + UPDATED_CREATE_TIME);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByCreateTimeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where createTime not equals to DEFAULT_CREATE_TIME
        defaultBatchJobExecutionShouldNotBeFound("createTime.notEquals=" + DEFAULT_CREATE_TIME);

        // Get all the batchJobExecutionList where createTime not equals to UPDATED_CREATE_TIME
        defaultBatchJobExecutionShouldBeFound("createTime.notEquals=" + UPDATED_CREATE_TIME);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByCreateTimeIsInShouldWork() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where createTime in DEFAULT_CREATE_TIME or UPDATED_CREATE_TIME
        defaultBatchJobExecutionShouldBeFound("createTime.in=" + DEFAULT_CREATE_TIME + "," + UPDATED_CREATE_TIME);

        // Get all the batchJobExecutionList where createTime equals to UPDATED_CREATE_TIME
        defaultBatchJobExecutionShouldNotBeFound("createTime.in=" + UPDATED_CREATE_TIME);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByCreateTimeIsNullOrNotNull() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where createTime is not null
        defaultBatchJobExecutionShouldBeFound("createTime.specified=true");

        // Get all the batchJobExecutionList where createTime is null
        defaultBatchJobExecutionShouldNotBeFound("createTime.specified=false");
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByStartTimeIsEqualToSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where startTime equals to DEFAULT_START_TIME
        defaultBatchJobExecutionShouldBeFound("startTime.equals=" + DEFAULT_START_TIME);

        // Get all the batchJobExecutionList where startTime equals to UPDATED_START_TIME
        defaultBatchJobExecutionShouldNotBeFound("startTime.equals=" + UPDATED_START_TIME);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByStartTimeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where startTime not equals to DEFAULT_START_TIME
        defaultBatchJobExecutionShouldNotBeFound("startTime.notEquals=" + DEFAULT_START_TIME);

        // Get all the batchJobExecutionList where startTime not equals to UPDATED_START_TIME
        defaultBatchJobExecutionShouldBeFound("startTime.notEquals=" + UPDATED_START_TIME);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByStartTimeIsInShouldWork() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where startTime in DEFAULT_START_TIME or UPDATED_START_TIME
        defaultBatchJobExecutionShouldBeFound("startTime.in=" + DEFAULT_START_TIME + "," + UPDATED_START_TIME);

        // Get all the batchJobExecutionList where startTime equals to UPDATED_START_TIME
        defaultBatchJobExecutionShouldNotBeFound("startTime.in=" + UPDATED_START_TIME);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByStartTimeIsNullOrNotNull() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where startTime is not null
        defaultBatchJobExecutionShouldBeFound("startTime.specified=true");

        // Get all the batchJobExecutionList where startTime is null
        defaultBatchJobExecutionShouldNotBeFound("startTime.specified=false");
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByEndTimeIsEqualToSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where endTime equals to DEFAULT_END_TIME
        defaultBatchJobExecutionShouldBeFound("endTime.equals=" + DEFAULT_END_TIME);

        // Get all the batchJobExecutionList where endTime equals to UPDATED_END_TIME
        defaultBatchJobExecutionShouldNotBeFound("endTime.equals=" + UPDATED_END_TIME);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByEndTimeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where endTime not equals to DEFAULT_END_TIME
        defaultBatchJobExecutionShouldNotBeFound("endTime.notEquals=" + DEFAULT_END_TIME);

        // Get all the batchJobExecutionList where endTime not equals to UPDATED_END_TIME
        defaultBatchJobExecutionShouldBeFound("endTime.notEquals=" + UPDATED_END_TIME);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByEndTimeIsInShouldWork() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where endTime in DEFAULT_END_TIME or UPDATED_END_TIME
        defaultBatchJobExecutionShouldBeFound("endTime.in=" + DEFAULT_END_TIME + "," + UPDATED_END_TIME);

        // Get all the batchJobExecutionList where endTime equals to UPDATED_END_TIME
        defaultBatchJobExecutionShouldNotBeFound("endTime.in=" + UPDATED_END_TIME);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByEndTimeIsNullOrNotNull() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where endTime is not null
        defaultBatchJobExecutionShouldBeFound("endTime.specified=true");

        // Get all the batchJobExecutionList where endTime is null
        defaultBatchJobExecutionShouldNotBeFound("endTime.specified=false");
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where status equals to DEFAULT_STATUS
        defaultBatchJobExecutionShouldBeFound("status.equals=" + DEFAULT_STATUS);

        // Get all the batchJobExecutionList where status equals to UPDATED_STATUS
        defaultBatchJobExecutionShouldNotBeFound("status.equals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByStatusIsNotEqualToSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where status not equals to DEFAULT_STATUS
        defaultBatchJobExecutionShouldNotBeFound("status.notEquals=" + DEFAULT_STATUS);

        // Get all the batchJobExecutionList where status not equals to UPDATED_STATUS
        defaultBatchJobExecutionShouldBeFound("status.notEquals=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByStatusIsInShouldWork() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where status in DEFAULT_STATUS or UPDATED_STATUS
        defaultBatchJobExecutionShouldBeFound("status.in=" + DEFAULT_STATUS + "," + UPDATED_STATUS);

        // Get all the batchJobExecutionList where status equals to UPDATED_STATUS
        defaultBatchJobExecutionShouldNotBeFound("status.in=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where status is not null
        defaultBatchJobExecutionShouldBeFound("status.specified=true");

        // Get all the batchJobExecutionList where status is null
        defaultBatchJobExecutionShouldNotBeFound("status.specified=false");
    }
                @Test
    @Transactional
    public void getAllBatchJobExecutionsByStatusContainsSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where status contains DEFAULT_STATUS
        defaultBatchJobExecutionShouldBeFound("status.contains=" + DEFAULT_STATUS);

        // Get all the batchJobExecutionList where status contains UPDATED_STATUS
        defaultBatchJobExecutionShouldNotBeFound("status.contains=" + UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByStatusNotContainsSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where status does not contain DEFAULT_STATUS
        defaultBatchJobExecutionShouldNotBeFound("status.doesNotContain=" + DEFAULT_STATUS);

        // Get all the batchJobExecutionList where status does not contain UPDATED_STATUS
        defaultBatchJobExecutionShouldBeFound("status.doesNotContain=" + UPDATED_STATUS);
    }


    @Test
    @Transactional
    public void getAllBatchJobExecutionsByExitCodeIsEqualToSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where exitCode equals to DEFAULT_EXIT_CODE
        defaultBatchJobExecutionShouldBeFound("exitCode.equals=" + DEFAULT_EXIT_CODE);

        // Get all the batchJobExecutionList where exitCode equals to UPDATED_EXIT_CODE
        defaultBatchJobExecutionShouldNotBeFound("exitCode.equals=" + UPDATED_EXIT_CODE);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByExitCodeIsNotEqualToSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where exitCode not equals to DEFAULT_EXIT_CODE
        defaultBatchJobExecutionShouldNotBeFound("exitCode.notEquals=" + DEFAULT_EXIT_CODE);

        // Get all the batchJobExecutionList where exitCode not equals to UPDATED_EXIT_CODE
        defaultBatchJobExecutionShouldBeFound("exitCode.notEquals=" + UPDATED_EXIT_CODE);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByExitCodeIsInShouldWork() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where exitCode in DEFAULT_EXIT_CODE or UPDATED_EXIT_CODE
        defaultBatchJobExecutionShouldBeFound("exitCode.in=" + DEFAULT_EXIT_CODE + "," + UPDATED_EXIT_CODE);

        // Get all the batchJobExecutionList where exitCode equals to UPDATED_EXIT_CODE
        defaultBatchJobExecutionShouldNotBeFound("exitCode.in=" + UPDATED_EXIT_CODE);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByExitCodeIsNullOrNotNull() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where exitCode is not null
        defaultBatchJobExecutionShouldBeFound("exitCode.specified=true");

        // Get all the batchJobExecutionList where exitCode is null
        defaultBatchJobExecutionShouldNotBeFound("exitCode.specified=false");
    }
                @Test
    @Transactional
    public void getAllBatchJobExecutionsByExitCodeContainsSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where exitCode contains DEFAULT_EXIT_CODE
        defaultBatchJobExecutionShouldBeFound("exitCode.contains=" + DEFAULT_EXIT_CODE);

        // Get all the batchJobExecutionList where exitCode contains UPDATED_EXIT_CODE
        defaultBatchJobExecutionShouldNotBeFound("exitCode.contains=" + UPDATED_EXIT_CODE);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByExitCodeNotContainsSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where exitCode does not contain DEFAULT_EXIT_CODE
        defaultBatchJobExecutionShouldNotBeFound("exitCode.doesNotContain=" + DEFAULT_EXIT_CODE);

        // Get all the batchJobExecutionList where exitCode does not contain UPDATED_EXIT_CODE
        defaultBatchJobExecutionShouldBeFound("exitCode.doesNotContain=" + UPDATED_EXIT_CODE);
    }


    @Test
    @Transactional
    public void getAllBatchJobExecutionsByExitMessageIsEqualToSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where exitMessage equals to DEFAULT_EXIT_MESSAGE
        defaultBatchJobExecutionShouldBeFound("exitMessage.equals=" + DEFAULT_EXIT_MESSAGE);

        // Get all the batchJobExecutionList where exitMessage equals to UPDATED_EXIT_MESSAGE
        defaultBatchJobExecutionShouldNotBeFound("exitMessage.equals=" + UPDATED_EXIT_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByExitMessageIsNotEqualToSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where exitMessage not equals to DEFAULT_EXIT_MESSAGE
        defaultBatchJobExecutionShouldNotBeFound("exitMessage.notEquals=" + DEFAULT_EXIT_MESSAGE);

        // Get all the batchJobExecutionList where exitMessage not equals to UPDATED_EXIT_MESSAGE
        defaultBatchJobExecutionShouldBeFound("exitMessage.notEquals=" + UPDATED_EXIT_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByExitMessageIsInShouldWork() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where exitMessage in DEFAULT_EXIT_MESSAGE or UPDATED_EXIT_MESSAGE
        defaultBatchJobExecutionShouldBeFound("exitMessage.in=" + DEFAULT_EXIT_MESSAGE + "," + UPDATED_EXIT_MESSAGE);

        // Get all the batchJobExecutionList where exitMessage equals to UPDATED_EXIT_MESSAGE
        defaultBatchJobExecutionShouldNotBeFound("exitMessage.in=" + UPDATED_EXIT_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByExitMessageIsNullOrNotNull() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where exitMessage is not null
        defaultBatchJobExecutionShouldBeFound("exitMessage.specified=true");

        // Get all the batchJobExecutionList where exitMessage is null
        defaultBatchJobExecutionShouldNotBeFound("exitMessage.specified=false");
    }
                @Test
    @Transactional
    public void getAllBatchJobExecutionsByExitMessageContainsSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where exitMessage contains DEFAULT_EXIT_MESSAGE
        defaultBatchJobExecutionShouldBeFound("exitMessage.contains=" + DEFAULT_EXIT_MESSAGE);

        // Get all the batchJobExecutionList where exitMessage contains UPDATED_EXIT_MESSAGE
        defaultBatchJobExecutionShouldNotBeFound("exitMessage.contains=" + UPDATED_EXIT_MESSAGE);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByExitMessageNotContainsSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where exitMessage does not contain DEFAULT_EXIT_MESSAGE
        defaultBatchJobExecutionShouldNotBeFound("exitMessage.doesNotContain=" + DEFAULT_EXIT_MESSAGE);

        // Get all the batchJobExecutionList where exitMessage does not contain UPDATED_EXIT_MESSAGE
        defaultBatchJobExecutionShouldBeFound("exitMessage.doesNotContain=" + UPDATED_EXIT_MESSAGE);
    }


    @Test
    @Transactional
    public void getAllBatchJobExecutionsByLastUpdatedIsEqualToSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where lastUpdated equals to DEFAULT_LAST_UPDATED
        defaultBatchJobExecutionShouldBeFound("lastUpdated.equals=" + DEFAULT_LAST_UPDATED);

        // Get all the batchJobExecutionList where lastUpdated equals to UPDATED_LAST_UPDATED
        defaultBatchJobExecutionShouldNotBeFound("lastUpdated.equals=" + UPDATED_LAST_UPDATED);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByLastUpdatedIsNotEqualToSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where lastUpdated not equals to DEFAULT_LAST_UPDATED
        defaultBatchJobExecutionShouldNotBeFound("lastUpdated.notEquals=" + DEFAULT_LAST_UPDATED);

        // Get all the batchJobExecutionList where lastUpdated not equals to UPDATED_LAST_UPDATED
        defaultBatchJobExecutionShouldBeFound("lastUpdated.notEquals=" + UPDATED_LAST_UPDATED);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByLastUpdatedIsInShouldWork() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where lastUpdated in DEFAULT_LAST_UPDATED or UPDATED_LAST_UPDATED
        defaultBatchJobExecutionShouldBeFound("lastUpdated.in=" + DEFAULT_LAST_UPDATED + "," + UPDATED_LAST_UPDATED);

        // Get all the batchJobExecutionList where lastUpdated equals to UPDATED_LAST_UPDATED
        defaultBatchJobExecutionShouldNotBeFound("lastUpdated.in=" + UPDATED_LAST_UPDATED);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByLastUpdatedIsNullOrNotNull() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where lastUpdated is not null
        defaultBatchJobExecutionShouldBeFound("lastUpdated.specified=true");

        // Get all the batchJobExecutionList where lastUpdated is null
        defaultBatchJobExecutionShouldNotBeFound("lastUpdated.specified=false");
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByJobConfigurationLocationIsEqualToSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where jobConfigurationLocation equals to DEFAULT_JOB_CONFIGURATION_LOCATION
        defaultBatchJobExecutionShouldBeFound("jobConfigurationLocation.equals=" + DEFAULT_JOB_CONFIGURATION_LOCATION);

        // Get all the batchJobExecutionList where jobConfigurationLocation equals to UPDATED_JOB_CONFIGURATION_LOCATION
        defaultBatchJobExecutionShouldNotBeFound("jobConfigurationLocation.equals=" + UPDATED_JOB_CONFIGURATION_LOCATION);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByJobConfigurationLocationIsNotEqualToSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where jobConfigurationLocation not equals to DEFAULT_JOB_CONFIGURATION_LOCATION
        defaultBatchJobExecutionShouldNotBeFound("jobConfigurationLocation.notEquals=" + DEFAULT_JOB_CONFIGURATION_LOCATION);

        // Get all the batchJobExecutionList where jobConfigurationLocation not equals to UPDATED_JOB_CONFIGURATION_LOCATION
        defaultBatchJobExecutionShouldBeFound("jobConfigurationLocation.notEquals=" + UPDATED_JOB_CONFIGURATION_LOCATION);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByJobConfigurationLocationIsInShouldWork() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where jobConfigurationLocation in DEFAULT_JOB_CONFIGURATION_LOCATION or UPDATED_JOB_CONFIGURATION_LOCATION
        defaultBatchJobExecutionShouldBeFound("jobConfigurationLocation.in=" + DEFAULT_JOB_CONFIGURATION_LOCATION + "," + UPDATED_JOB_CONFIGURATION_LOCATION);

        // Get all the batchJobExecutionList where jobConfigurationLocation equals to UPDATED_JOB_CONFIGURATION_LOCATION
        defaultBatchJobExecutionShouldNotBeFound("jobConfigurationLocation.in=" + UPDATED_JOB_CONFIGURATION_LOCATION);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByJobConfigurationLocationIsNullOrNotNull() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where jobConfigurationLocation is not null
        defaultBatchJobExecutionShouldBeFound("jobConfigurationLocation.specified=true");

        // Get all the batchJobExecutionList where jobConfigurationLocation is null
        defaultBatchJobExecutionShouldNotBeFound("jobConfigurationLocation.specified=false");
    }
                @Test
    @Transactional
    public void getAllBatchJobExecutionsByJobConfigurationLocationContainsSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where jobConfigurationLocation contains DEFAULT_JOB_CONFIGURATION_LOCATION
        defaultBatchJobExecutionShouldBeFound("jobConfigurationLocation.contains=" + DEFAULT_JOB_CONFIGURATION_LOCATION);

        // Get all the batchJobExecutionList where jobConfigurationLocation contains UPDATED_JOB_CONFIGURATION_LOCATION
        defaultBatchJobExecutionShouldNotBeFound("jobConfigurationLocation.contains=" + UPDATED_JOB_CONFIGURATION_LOCATION);
    }

    @Test
    @Transactional
    public void getAllBatchJobExecutionsByJobConfigurationLocationNotContainsSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        // Get all the batchJobExecutionList where jobConfigurationLocation does not contain DEFAULT_JOB_CONFIGURATION_LOCATION
        defaultBatchJobExecutionShouldNotBeFound("jobConfigurationLocation.doesNotContain=" + DEFAULT_JOB_CONFIGURATION_LOCATION);

        // Get all the batchJobExecutionList where jobConfigurationLocation does not contain UPDATED_JOB_CONFIGURATION_LOCATION
        defaultBatchJobExecutionShouldBeFound("jobConfigurationLocation.doesNotContain=" + UPDATED_JOB_CONFIGURATION_LOCATION);
    }


    @Test
    @Transactional
    public void getAllBatchJobExecutionsByBatchStepExecutionIsEqualToSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);
        BatchStepExecution batchStepExecution = BatchStepExecutionResourceIT.createEntity(em, batchJobExecution);
        em.persist(batchStepExecution);
        em.flush();
        /*
        batchJobExecution.addBatchStepExecution(batchStepExecution);
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);
         */
        Long batchStepExecutionId = batchStepExecution.getId();

        // Get all the batchJobExecutionList where batchStepExecution equals to batchStepExecutionId
        defaultBatchJobExecutionShouldBeFound("batchStepExecutionId.equals=" + batchStepExecutionId);

        // Get all the batchJobExecutionList where batchStepExecution equals to batchStepExecutionId + 1
        defaultBatchJobExecutionShouldNotBeFound("batchStepExecutionId.equals=" + (batchStepExecutionId + 1));
    }


    @Test
    @Transactional
    public void getAllBatchJobExecutionsByBatchJobInstanceIsEqualToSomething() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);
        //BatchJobInstance batchJobInstance = BatchJobInstanceResourceIT.createEntity(em);
        //em.persist(batchJobInstance);
        em.flush();
        //batchJobExecution.setBatchJobInstance(batchJobInstance);
        //batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        Long batchJobInstanceId = batchJobExecution.getBatchJobInstance().getId();

        // Get all the batchJobExecutionList where batchJobInstance equals to batchJobInstanceId
        defaultBatchJobExecutionShouldBeFound("batchJobInstanceId.equals=" + batchJobInstanceId);

        // Get all the batchJobExecutionList where batchJobInstance equals to batchJobInstanceId + 1
        defaultBatchJobExecutionShouldNotBeFound("batchJobInstanceId.equals=" + (batchJobInstanceId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultBatchJobExecutionShouldBeFound(String filter) throws Exception {
        restBatchJobExecutionMockMvc.perform(get("/api/batch-job-executions?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(batchJobExecution.getId().intValue())))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION.intValue())))
            .andExpect(jsonPath("$.[*].createTime").value(hasItem(DEFAULT_CREATE_TIME.toString())))
            .andExpect(jsonPath("$.[*].startTime").value(hasItem(DEFAULT_START_TIME.toString())))
            .andExpect(jsonPath("$.[*].endTime").value(hasItem(DEFAULT_END_TIME.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS)))
            .andExpect(jsonPath("$.[*].exitCode").value(hasItem(DEFAULT_EXIT_CODE)))
            .andExpect(jsonPath("$.[*].exitMessage").value(hasItem(DEFAULT_EXIT_MESSAGE)))
            .andExpect(jsonPath("$.[*].lastUpdated").value(hasItem(DEFAULT_LAST_UPDATED.toString())))
            .andExpect(jsonPath("$.[*].jobConfigurationLocation").value(hasItem(DEFAULT_JOB_CONFIGURATION_LOCATION)));

        // Check, that the count call also returns 1
        restBatchJobExecutionMockMvc.perform(get("/api/batch-job-executions/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultBatchJobExecutionShouldNotBeFound(String filter) throws Exception {
        restBatchJobExecutionMockMvc.perform(get("/api/batch-job-executions?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restBatchJobExecutionMockMvc.perform(get("/api/batch-job-executions/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingBatchJobExecution() throws Exception {
        // Get the batchJobExecution
        restBatchJobExecutionMockMvc.perform(get("/api/batch-job-executions/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBatchJobExecution() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        int databaseSizeBeforeUpdate = batchJobExecutionRepository.findAll().size();

        // Update the batchJobExecution
        BatchJobExecution updatedBatchJobExecution = batchJobExecutionRepository.findById(batchJobExecution.getId()).get();
        // Disconnect from session so that the updates on updatedBatchJobExecution are not directly saved in db
        em.detach(updatedBatchJobExecution);
        updatedBatchJobExecution
            .version(UPDATED_VERSION)
            .createTime(UPDATED_CREATE_TIME)
            .startTime(UPDATED_START_TIME)
            .endTime(UPDATED_END_TIME)
            .status(UPDATED_STATUS)
            .exitCode(UPDATED_EXIT_CODE)
            .exitMessage(UPDATED_EXIT_MESSAGE)
            .lastUpdated(UPDATED_LAST_UPDATED)
            .jobConfigurationLocation(UPDATED_JOB_CONFIGURATION_LOCATION);
        BatchJobExecutionDTO batchJobExecutionDTO = batchJobExecutionMapper.toDto(updatedBatchJobExecution);

        restBatchJobExecutionMockMvc.perform(put("/api/batch-job-executions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(batchJobExecutionDTO)))
            .andExpect(status().isOk());

        // Validate the BatchJobExecution in the database
        List<BatchJobExecution> batchJobExecutionList = batchJobExecutionRepository.findAll();
        assertThat(batchJobExecutionList).hasSize(databaseSizeBeforeUpdate);
        BatchJobExecution testBatchJobExecution = batchJobExecutionList.get(batchJobExecutionList.size() - 1);
        assertThat(testBatchJobExecution.getVersion()).isEqualTo(UPDATED_VERSION);
        assertThat(testBatchJobExecution.getCreateTime()).isEqualTo(UPDATED_CREATE_TIME);
        assertThat(testBatchJobExecution.getStartTime()).isEqualTo(UPDATED_START_TIME);
        assertThat(testBatchJobExecution.getEndTime()).isEqualTo(UPDATED_END_TIME);
        assertThat(testBatchJobExecution.getStatus()).isEqualTo(UPDATED_STATUS);
        assertThat(testBatchJobExecution.getExitCode()).isEqualTo(UPDATED_EXIT_CODE);
        assertThat(testBatchJobExecution.getExitMessage()).isEqualTo(UPDATED_EXIT_MESSAGE);
        assertThat(testBatchJobExecution.getLastUpdated()).isEqualTo(UPDATED_LAST_UPDATED);
        assertThat(testBatchJobExecution.getJobConfigurationLocation()).isEqualTo(UPDATED_JOB_CONFIGURATION_LOCATION);
    }

    @Test
    @Transactional
    public void updateNonExistingBatchJobExecution() throws Exception {
        int databaseSizeBeforeUpdate = batchJobExecutionRepository.findAll().size();

        // Create the BatchJobExecution
        BatchJobExecutionDTO batchJobExecutionDTO = batchJobExecutionMapper.toDto(batchJobExecution);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBatchJobExecutionMockMvc.perform(put("/api/batch-job-executions")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(batchJobExecutionDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BatchJobExecution in the database
        List<BatchJobExecution> batchJobExecutionList = batchJobExecutionRepository.findAll();
        assertThat(batchJobExecutionList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBatchJobExecution() throws Exception {
        // Initialize the database
        batchJobExecutionRepository.saveAndFlush(batchJobExecution);

        int databaseSizeBeforeDelete = batchJobExecutionRepository.findAll().size();

        // Delete the batchJobExecution
        restBatchJobExecutionMockMvc.perform(delete("/api/batch-job-executions/{id}", batchJobExecution.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BatchJobExecution> batchJobExecutionList = batchJobExecutionRepository.findAll();
        assertThat(batchJobExecutionList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
