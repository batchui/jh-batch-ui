package com.ag04.batchui.web.rest;

import com.ag04.batchui.BatchuiAppTest;
import com.ag04.batchui.domain.BatchJobInstance;
import com.ag04.batchui.domain.BatchJobExecution;
import com.ag04.batchui.repository.BatchJobInstanceRepository;
import com.ag04.batchui.service.BatchJobInstanceService;
import com.ag04.batchui.service.dto.BatchJobInstanceDTO;
import com.ag04.batchui.service.mapper.BatchJobInstanceMapper;
import com.ag04.batchui.service.BatchJobInstanceQueryService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link BatchJobInstanceResource} REST controller.
 */
@SpringBootTest(classes = BatchuiAppTest.class)

@AutoConfigureMockMvc
@WithMockUser
public class BatchJobInstanceResourceIT {

    private static final Long SMALLER_JOB_INSTANCE_ID = 1L - 1L;

    private static final Long DEFAULT_VERSION = 1L;
    private static final Long UPDATED_VERSION = 2L;
    private static final Long SMALLER_VERSION = 1L - 1L;

    private static final String DEFAULT_JOB_NAME = "AAAAAAAAAA";
    private static final String UPDATED_JOB_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_JOB_KEY = "AAAAAAAAAA";
    private static final String UPDATED_JOB_KEY = "BBBBBBBBBB";

    @Autowired
    private BatchJobInstanceRepository batchJobInstanceRepository;

    @Autowired
    private BatchJobInstanceMapper batchJobInstanceMapper;

    @Autowired
    private BatchJobInstanceService batchJobInstanceService;

    @Autowired
    private BatchJobInstanceQueryService batchJobInstanceQueryService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restBatchJobInstanceMockMvc;

    private BatchJobInstance batchJobInstance;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BatchJobInstance createEntity(EntityManager em) {
        BatchJobInstance batchJobInstance = new BatchJobInstance()
            .version(DEFAULT_VERSION)
            .jobName(DEFAULT_JOB_NAME)
            .jobKey(DEFAULT_JOB_KEY);
        return batchJobInstance;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BatchJobInstance createUpdatedEntity(EntityManager em) {
        BatchJobInstance batchJobInstance = new BatchJobInstance()
            .version(UPDATED_VERSION)
            .jobName(UPDATED_JOB_NAME)
            .jobKey(UPDATED_JOB_KEY);
        return batchJobInstance;
    }

    @BeforeEach
    public void initTest() {
        batchJobInstance = createEntity(em);
    }

    @Test
    @Transactional
    public void createBatchJobInstance() throws Exception {
        int databaseSizeBeforeCreate = batchJobInstanceRepository.findAll().size();

        // Create the BatchJobInstance
        BatchJobInstanceDTO batchJobInstanceDTO = batchJobInstanceMapper.toDto(batchJobInstance);
        restBatchJobInstanceMockMvc.perform(post("/api/batch-job-instances")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(batchJobInstanceDTO)))
            .andExpect(status().isCreated());

        // Validate the BatchJobInstance in the database
        List<BatchJobInstance> batchJobInstanceList = batchJobInstanceRepository.findAll();
        assertThat(batchJobInstanceList).hasSize(databaseSizeBeforeCreate + 1);
        BatchJobInstance testBatchJobInstance = batchJobInstanceList.get(batchJobInstanceList.size() - 1);
        assertThat(testBatchJobInstance.getVersion()).isEqualTo(DEFAULT_VERSION);
        assertThat(testBatchJobInstance.getJobName()).isEqualTo(DEFAULT_JOB_NAME);
        assertThat(testBatchJobInstance.getJobKey()).isEqualTo(DEFAULT_JOB_KEY);
    }

    @Test
    @Transactional
    public void createBatchJobInstanceWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = batchJobInstanceRepository.findAll().size();

        // Create the BatchJobInstance with an existing ID
        batchJobInstance.setId(1L);
        BatchJobInstanceDTO batchJobInstanceDTO = batchJobInstanceMapper.toDto(batchJobInstance);

        // An entity with an existing ID cannot be created, so this API call must fail
        restBatchJobInstanceMockMvc.perform(post("/api/batch-job-instances")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(batchJobInstanceDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BatchJobInstance in the database
        List<BatchJobInstance> batchJobInstanceList = batchJobInstanceRepository.findAll();
        assertThat(batchJobInstanceList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkJobNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = batchJobInstanceRepository.findAll().size();
        // set the field null
        batchJobInstance.setJobName(null);

        // Create the BatchJobInstance, which fails.
        BatchJobInstanceDTO batchJobInstanceDTO = batchJobInstanceMapper.toDto(batchJobInstance);

        restBatchJobInstanceMockMvc.perform(post("/api/batch-job-instances")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(batchJobInstanceDTO)))
            .andExpect(status().isBadRequest());

        List<BatchJobInstance> batchJobInstanceList = batchJobInstanceRepository.findAll();
        assertThat(batchJobInstanceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkJobKeyIsRequired() throws Exception {
        int databaseSizeBeforeTest = batchJobInstanceRepository.findAll().size();
        // set the field null
        batchJobInstance.setJobKey(null);

        // Create the BatchJobInstance, which fails.
        BatchJobInstanceDTO batchJobInstanceDTO = batchJobInstanceMapper.toDto(batchJobInstance);

        restBatchJobInstanceMockMvc.perform(post("/api/batch-job-instances")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(batchJobInstanceDTO)))
            .andExpect(status().isBadRequest());

        List<BatchJobInstance> batchJobInstanceList = batchJobInstanceRepository.findAll();
        assertThat(batchJobInstanceList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllBatchJobInstances() throws Exception {
        // Initialize the database
        batchJobInstanceRepository.saveAndFlush(batchJobInstance);

        // Get all the batchJobInstanceList
        restBatchJobInstanceMockMvc.perform(get("/api/batch-job-instances?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(batchJobInstance.getId().intValue())))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION.intValue())))
            .andExpect(jsonPath("$.[*].jobName").value(hasItem(DEFAULT_JOB_NAME)))
            .andExpect(jsonPath("$.[*].jobKey").value(hasItem(DEFAULT_JOB_KEY)))
            .andExpect(jsonPath("$.[*].batchJobExecutions").doesNotExist());
    }
    
    @Test
    @Transactional
    public void getBatchJobInstance() throws Exception {
        // Initialize the database
        BatchJobExecution bje = BatchJobExecutionResourceIT.createEntity(em, batchJobInstance);
        batchJobInstance.addBatchJobExecution(bje);
        batchJobInstanceRepository.saveAndFlush(batchJobInstance);

        // Get the batchJobInstance
        ResultActions result = restBatchJobInstanceMockMvc.perform(get("/api/batch-job-instances/{id}", batchJobInstance.getId()));
        result.andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(batchJobInstance.getId().intValue()))
            .andExpect(jsonPath("$.version").value(DEFAULT_VERSION.intValue()))
            .andExpect(jsonPath("$.jobName").value(DEFAULT_JOB_NAME))
            .andExpect(jsonPath("$.jobKey").value(DEFAULT_JOB_KEY))
            .andExpect(jsonPath("$.batchJobExecutions").isArray())
            .andExpect(jsonPath("$.batchJobExecutions").isNotEmpty());
    }


    @Test
    @Transactional
    public void getBatchJobInstancesByIdFiltering() throws Exception {
        // Initialize the database
        batchJobInstanceRepository.saveAndFlush(batchJobInstance);

        Long id = batchJobInstance.getId();

        defaultBatchJobInstanceShouldBeFound("id.equals=" + id);
        defaultBatchJobInstanceShouldNotBeFound("id.notEquals=" + id);

        defaultBatchJobInstanceShouldBeFound("id.greaterThanOrEqual=" + id);
        defaultBatchJobInstanceShouldNotBeFound("id.greaterThan=" + id);

        defaultBatchJobInstanceShouldBeFound("id.lessThanOrEqual=" + id);
        defaultBatchJobInstanceShouldNotBeFound("id.lessThan=" + id);
    }

    @Test
    @Transactional
    public void getAllBatchJobInstancesByVersionIsEqualToSomething() throws Exception {
        // Initialize the database
        batchJobInstanceRepository.saveAndFlush(batchJobInstance);

        // Get all the batchJobInstanceList where version equals to DEFAULT_VERSION
        defaultBatchJobInstanceShouldBeFound("version.equals=" + DEFAULT_VERSION);

        // Get all the batchJobInstanceList where version equals to UPDATED_VERSION
        defaultBatchJobInstanceShouldNotBeFound("version.equals=" + UPDATED_VERSION);
    }

    @Test
    @Transactional
    public void getAllBatchJobInstancesByVersionIsNotEqualToSomething() throws Exception {
        // Initialize the database
        batchJobInstanceRepository.saveAndFlush(batchJobInstance);

        // Get all the batchJobInstanceList where version not equals to DEFAULT_VERSION
        defaultBatchJobInstanceShouldNotBeFound("version.notEquals=" + DEFAULT_VERSION);

        // Get all the batchJobInstanceList where version not equals to UPDATED_VERSION
        defaultBatchJobInstanceShouldBeFound("version.notEquals=" + UPDATED_VERSION);
    }

    @Test
    @Transactional
    public void getAllBatchJobInstancesByVersionIsInShouldWork() throws Exception {
        // Initialize the database
        batchJobInstanceRepository.saveAndFlush(batchJobInstance);

        // Get all the batchJobInstanceList where version in DEFAULT_VERSION or UPDATED_VERSION
        defaultBatchJobInstanceShouldBeFound("version.in=" + DEFAULT_VERSION + "," + UPDATED_VERSION);

        // Get all the batchJobInstanceList where version equals to UPDATED_VERSION
        defaultBatchJobInstanceShouldNotBeFound("version.in=" + UPDATED_VERSION);
    }

    @Test
    @Transactional
    public void getAllBatchJobInstancesByVersionIsNullOrNotNull() throws Exception {
        // Initialize the database
        batchJobInstanceRepository.saveAndFlush(batchJobInstance);

        // Get all the batchJobInstanceList where version is not null
        defaultBatchJobInstanceShouldBeFound("version.specified=true");

        // Get all the batchJobInstanceList where version is null
        defaultBatchJobInstanceShouldNotBeFound("version.specified=false");
    }

    @Test
    @Transactional
    public void getAllBatchJobInstancesByVersionIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        batchJobInstanceRepository.saveAndFlush(batchJobInstance);

        // Get all the batchJobInstanceList where version is greater than or equal to DEFAULT_VERSION
        defaultBatchJobInstanceShouldBeFound("version.greaterThanOrEqual=" + DEFAULT_VERSION);

        // Get all the batchJobInstanceList where version is greater than or equal to UPDATED_VERSION
        defaultBatchJobInstanceShouldNotBeFound("version.greaterThanOrEqual=" + UPDATED_VERSION);
    }

    @Test
    @Transactional
    public void getAllBatchJobInstancesByVersionIsLessThanOrEqualToSomething() throws Exception {
        // Initialize the database
        batchJobInstanceRepository.saveAndFlush(batchJobInstance);

        // Get all the batchJobInstanceList where version is less than or equal to DEFAULT_VERSION
        defaultBatchJobInstanceShouldBeFound("version.lessThanOrEqual=" + DEFAULT_VERSION);

        // Get all the batchJobInstanceList where version is less than or equal to SMALLER_VERSION
        defaultBatchJobInstanceShouldNotBeFound("version.lessThanOrEqual=" + SMALLER_VERSION);
    }

    @Test
    @Transactional
    public void getAllBatchJobInstancesByVersionIsLessThanSomething() throws Exception {
        // Initialize the database
        batchJobInstanceRepository.saveAndFlush(batchJobInstance);

        // Get all the batchJobInstanceList where version is less than DEFAULT_VERSION
        defaultBatchJobInstanceShouldNotBeFound("version.lessThan=" + DEFAULT_VERSION);

        // Get all the batchJobInstanceList where version is less than UPDATED_VERSION
        defaultBatchJobInstanceShouldBeFound("version.lessThan=" + UPDATED_VERSION);
    }

    @Test
    @Transactional
    public void getAllBatchJobInstancesByVersionIsGreaterThanSomething() throws Exception {
        // Initialize the database
        batchJobInstanceRepository.saveAndFlush(batchJobInstance);

        // Get all the batchJobInstanceList where version is greater than DEFAULT_VERSION
        defaultBatchJobInstanceShouldNotBeFound("version.greaterThan=" + DEFAULT_VERSION);

        // Get all the batchJobInstanceList where version is greater than SMALLER_VERSION
        defaultBatchJobInstanceShouldBeFound("version.greaterThan=" + SMALLER_VERSION);
    }


    @Test
    @Transactional
    public void getAllBatchJobInstancesByJobNameIsEqualToSomething() throws Exception {
        // Initialize the database
        batchJobInstanceRepository.saveAndFlush(batchJobInstance);

        // Get all the batchJobInstanceList where jobName equals to DEFAULT_JOB_NAME
        defaultBatchJobInstanceShouldBeFound("jobName.equals=" + DEFAULT_JOB_NAME);

        // Get all the batchJobInstanceList where jobName equals to UPDATED_JOB_NAME
        defaultBatchJobInstanceShouldNotBeFound("jobName.equals=" + UPDATED_JOB_NAME);
    }

    @Test
    @Transactional
    public void getAllBatchJobInstancesByJobNameIsNotEqualToSomething() throws Exception {
        // Initialize the database
        batchJobInstanceRepository.saveAndFlush(batchJobInstance);

        // Get all the batchJobInstanceList where jobName not equals to DEFAULT_JOB_NAME
        defaultBatchJobInstanceShouldNotBeFound("jobName.notEquals=" + DEFAULT_JOB_NAME);

        // Get all the batchJobInstanceList where jobName not equals to UPDATED_JOB_NAME
        defaultBatchJobInstanceShouldBeFound("jobName.notEquals=" + UPDATED_JOB_NAME);
    }

    @Test
    @Transactional
    public void getAllBatchJobInstancesByJobNameIsInShouldWork() throws Exception {
        // Initialize the database
        batchJobInstanceRepository.saveAndFlush(batchJobInstance);

        // Get all the batchJobInstanceList where jobName in DEFAULT_JOB_NAME or UPDATED_JOB_NAME
        defaultBatchJobInstanceShouldBeFound("jobName.in=" + DEFAULT_JOB_NAME + "," + UPDATED_JOB_NAME);

        // Get all the batchJobInstanceList where jobName equals to UPDATED_JOB_NAME
        defaultBatchJobInstanceShouldNotBeFound("jobName.in=" + UPDATED_JOB_NAME);
    }

    @Test
    @Transactional
    public void getAllBatchJobInstancesByJobNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        batchJobInstanceRepository.saveAndFlush(batchJobInstance);

        // Get all the batchJobInstanceList where jobName is not null
        defaultBatchJobInstanceShouldBeFound("jobName.specified=true");

        // Get all the batchJobInstanceList where jobName is null
        defaultBatchJobInstanceShouldNotBeFound("jobName.specified=false");
    }
                @Test
    @Transactional
    public void getAllBatchJobInstancesByJobNameContainsSomething() throws Exception {
        // Initialize the database
        batchJobInstanceRepository.saveAndFlush(batchJobInstance);

        // Get all the batchJobInstanceList where jobName contains DEFAULT_JOB_NAME
        defaultBatchJobInstanceShouldBeFound("jobName.contains=" + DEFAULT_JOB_NAME);

        // Get all the batchJobInstanceList where jobName contains UPDATED_JOB_NAME
        defaultBatchJobInstanceShouldNotBeFound("jobName.contains=" + UPDATED_JOB_NAME);
    }

    @Test
    @Transactional
    public void getAllBatchJobInstancesByJobNameNotContainsSomething() throws Exception {
        // Initialize the database
        batchJobInstanceRepository.saveAndFlush(batchJobInstance);

        // Get all the batchJobInstanceList where jobName does not contain DEFAULT_JOB_NAME
        defaultBatchJobInstanceShouldNotBeFound("jobName.doesNotContain=" + DEFAULT_JOB_NAME);

        // Get all the batchJobInstanceList where jobName does not contain UPDATED_JOB_NAME
        defaultBatchJobInstanceShouldBeFound("jobName.doesNotContain=" + UPDATED_JOB_NAME);
    }


    @Test
    @Transactional
    public void getAllBatchJobInstancesByJobKeyIsEqualToSomething() throws Exception {
        // Initialize the database
        batchJobInstanceRepository.saveAndFlush(batchJobInstance);

        // Get all the batchJobInstanceList where jobKey equals to DEFAULT_JOB_KEY
        defaultBatchJobInstanceShouldBeFound("jobKey.equals=" + DEFAULT_JOB_KEY);

        // Get all the batchJobInstanceList where jobKey equals to UPDATED_JOB_KEY
        defaultBatchJobInstanceShouldNotBeFound("jobKey.equals=" + UPDATED_JOB_KEY);
    }

    @Test
    @Transactional
    public void getAllBatchJobInstancesByJobKeyIsNotEqualToSomething() throws Exception {
        // Initialize the database
        batchJobInstanceRepository.saveAndFlush(batchJobInstance);

        // Get all the batchJobInstanceList where jobKey not equals to DEFAULT_JOB_KEY
        defaultBatchJobInstanceShouldNotBeFound("jobKey.notEquals=" + DEFAULT_JOB_KEY);

        // Get all the batchJobInstanceList where jobKey not equals to UPDATED_JOB_KEY
        defaultBatchJobInstanceShouldBeFound("jobKey.notEquals=" + UPDATED_JOB_KEY);
    }

    @Test
    @Transactional
    public void getAllBatchJobInstancesByJobKeyIsInShouldWork() throws Exception {
        // Initialize the database
        batchJobInstanceRepository.saveAndFlush(batchJobInstance);

        // Get all the batchJobInstanceList where jobKey in DEFAULT_JOB_KEY or UPDATED_JOB_KEY
        defaultBatchJobInstanceShouldBeFound("jobKey.in=" + DEFAULT_JOB_KEY + "," + UPDATED_JOB_KEY);

        // Get all the batchJobInstanceList where jobKey equals to UPDATED_JOB_KEY
        defaultBatchJobInstanceShouldNotBeFound("jobKey.in=" + UPDATED_JOB_KEY);
    }

    @Test
    @Transactional
    public void getAllBatchJobInstancesByJobKeyIsNullOrNotNull() throws Exception {
        // Initialize the database
        batchJobInstanceRepository.saveAndFlush(batchJobInstance);

        // Get all the batchJobInstanceList where jobKey is not null
        defaultBatchJobInstanceShouldBeFound("jobKey.specified=true");

        // Get all the batchJobInstanceList where jobKey is null
        defaultBatchJobInstanceShouldNotBeFound("jobKey.specified=false");
    }
                @Test
    @Transactional
    public void getAllBatchJobInstancesByJobKeyContainsSomething() throws Exception {
        // Initialize the database
        batchJobInstanceRepository.saveAndFlush(batchJobInstance);

        // Get all the batchJobInstanceList where jobKey contains DEFAULT_JOB_KEY
        defaultBatchJobInstanceShouldBeFound("jobKey.contains=" + DEFAULT_JOB_KEY);

        // Get all the batchJobInstanceList where jobKey contains UPDATED_JOB_KEY
        defaultBatchJobInstanceShouldNotBeFound("jobKey.contains=" + UPDATED_JOB_KEY);
    }

    @Test
    @Transactional
    public void getAllBatchJobInstancesByJobKeyNotContainsSomething() throws Exception {
        // Initialize the database
        batchJobInstanceRepository.saveAndFlush(batchJobInstance);

        // Get all the batchJobInstanceList where jobKey does not contain DEFAULT_JOB_KEY
        defaultBatchJobInstanceShouldNotBeFound("jobKey.doesNotContain=" + DEFAULT_JOB_KEY);

        // Get all the batchJobInstanceList where jobKey does not contain UPDATED_JOB_KEY
        defaultBatchJobInstanceShouldBeFound("jobKey.doesNotContain=" + UPDATED_JOB_KEY);
    }


    @Test
    @Transactional
    public void getAllBatchJobInstancesByBatchJobExecutionIsEqualToSomething() throws Exception {
        // Initialize the database
        batchJobInstanceRepository.saveAndFlush(batchJobInstance);
        BatchJobExecution batchJobExecution = BatchJobExecutionResourceIT.createEntity(em, batchJobInstance);
        em.persist(batchJobExecution);
        em.flush();
        List<BatchJobInstance> batchJobInstanceList = batchJobInstanceRepository.findAll();
        /*
        batchJobInstance.addBatchJobExecution(batchJobExecution);
        batchJobInstanceRepository.saveAndFlush(batchJobInstance);
        */
        Long batchJobExecutionId = batchJobExecution.getId();

        // Get all the batchJobInstanceList where batchJobExecution equals to batchJobExecutionId
        defaultBatchJobInstanceShouldBeFound("batchJobExecutionId.equals=" + batchJobExecutionId);

        // Get all the batchJobInstanceList where batchJobExecution equals to batchJobExecutionId + 1
        defaultBatchJobInstanceShouldNotBeFound("batchJobExecutionId.equals=" + (batchJobExecutionId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned.
     */
    private void defaultBatchJobInstanceShouldBeFound(String filter) throws Exception {
        restBatchJobInstanceMockMvc.perform(get("/api/batch-job-instances?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(batchJobInstance.getId().intValue())))
            .andExpect(jsonPath("$.[*].version").value(hasItem(DEFAULT_VERSION.intValue())))
            .andExpect(jsonPath("$.[*].jobName").value(hasItem(DEFAULT_JOB_NAME)))
            .andExpect(jsonPath("$.[*].jobKey").value(hasItem(DEFAULT_JOB_KEY)));

        // Check, that the count call also returns 1
        restBatchJobInstanceMockMvc.perform(get("/api/batch-job-instances/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("1"));
    }

    /**
     * Executes the search, and checks that the default entity is not returned.
     */
    private void defaultBatchJobInstanceShouldNotBeFound(String filter) throws Exception {
        restBatchJobInstanceMockMvc.perform(get("/api/batch-job-instances?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());

        // Check, that the count call also returns 0
        restBatchJobInstanceMockMvc.perform(get("/api/batch-job-instances/count?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(content().string("0"));
    }


    @Test
    @Transactional
    public void getNonExistingBatchJobInstance() throws Exception {
        // Get the batchJobInstance
        restBatchJobInstanceMockMvc.perform(get("/api/batch-job-instances/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateBatchJobInstance() throws Exception {
        // Initialize the database
        batchJobInstanceRepository.saveAndFlush(batchJobInstance);

        int databaseSizeBeforeUpdate = batchJobInstanceRepository.findAll().size();

        // Update the batchJobInstance
        BatchJobInstance updatedBatchJobInstance = batchJobInstanceRepository.findById(batchJobInstance.getId()).get();
        // Disconnect from session so that the updates on updatedBatchJobInstance are not directly saved in db
        em.detach(updatedBatchJobInstance);
        updatedBatchJobInstance
            .version(UPDATED_VERSION)
            .jobName(UPDATED_JOB_NAME)
            .jobKey(UPDATED_JOB_KEY);
        BatchJobInstanceDTO batchJobInstanceDTO = batchJobInstanceMapper.toDto(updatedBatchJobInstance);

        restBatchJobInstanceMockMvc.perform(put("/api/batch-job-instances")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(batchJobInstanceDTO)))
            .andExpect(status().isOk());

        // Validate the BatchJobInstance in the database
        List<BatchJobInstance> batchJobInstanceList = batchJobInstanceRepository.findAll();
        assertThat(batchJobInstanceList).hasSize(databaseSizeBeforeUpdate);
        BatchJobInstance testBatchJobInstance = batchJobInstanceList.get(batchJobInstanceList.size() - 1);
        assertThat(testBatchJobInstance.getVersion()).isEqualTo(UPDATED_VERSION);
        assertThat(testBatchJobInstance.getJobName()).isEqualTo(UPDATED_JOB_NAME);
        assertThat(testBatchJobInstance.getJobKey()).isEqualTo(UPDATED_JOB_KEY);
    }

    @Test
    @Transactional
    public void updateNonExistingBatchJobInstance() throws Exception {
        int databaseSizeBeforeUpdate = batchJobInstanceRepository.findAll().size();

        // Create the BatchJobInstance
        BatchJobInstanceDTO batchJobInstanceDTO = batchJobInstanceMapper.toDto(batchJobInstance);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restBatchJobInstanceMockMvc.perform(put("/api/batch-job-instances")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(batchJobInstanceDTO)))
            .andExpect(status().isBadRequest());

        // Validate the BatchJobInstance in the database
        List<BatchJobInstance> batchJobInstanceList = batchJobInstanceRepository.findAll();
        assertThat(batchJobInstanceList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteBatchJobInstance() throws Exception {
        // Initialize the database
        batchJobInstanceRepository.saveAndFlush(batchJobInstance);

        int databaseSizeBeforeDelete = batchJobInstanceRepository.findAll().size();

        // Delete the batchJobInstance
        restBatchJobInstanceMockMvc.perform(delete("/api/batch-job-instances/{id}", batchJobInstance.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<BatchJobInstance> batchJobInstanceList = batchJobInstanceRepository.findAll();
        assertThat(batchJobInstanceList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
