package com.ag04.batchui.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.ag04.batchui.web.rest.TestUtil;

public class BatchJobInstanceTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BatchJobInstance.class);
        BatchJobInstance batchJobInstance1 = new BatchJobInstance();
        batchJobInstance1.setId(1L);
        BatchJobInstance batchJobInstance2 = new BatchJobInstance();
        batchJobInstance2.setId(batchJobInstance1.getId());
        assertThat(batchJobInstance1).isEqualTo(batchJobInstance2);
        batchJobInstance2.setId(2L);
        assertThat(batchJobInstance1).isNotEqualTo(batchJobInstance2);
        batchJobInstance1.setId(null);
        assertThat(batchJobInstance1).isNotEqualTo(batchJobInstance2);
    }
}
