package com.ag04.batchui.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.ag04.batchui.web.rest.TestUtil;

public class BatchStepExecutionTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BatchStepExecution.class);
        BatchStepExecution batchStepExecution1 = new BatchStepExecution();
        batchStepExecution1.setId(1L);
        BatchStepExecution batchStepExecution2 = new BatchStepExecution();
        batchStepExecution2.setId(batchStepExecution1.getId());
        assertThat(batchStepExecution1).isEqualTo(batchStepExecution2);
        batchStepExecution2.setId(2L);
        assertThat(batchStepExecution1).isNotEqualTo(batchStepExecution2);
        batchStepExecution1.setId(null);
        assertThat(batchStepExecution1).isNotEqualTo(batchStepExecution2);
    }
}
