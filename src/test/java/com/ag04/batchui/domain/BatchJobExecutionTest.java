package com.ag04.batchui.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.ag04.batchui.web.rest.TestUtil;

public class BatchJobExecutionTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(BatchJobExecution.class);
        BatchJobExecution batchJobExecution1 = new BatchJobExecution();
        batchJobExecution1.setId(1L);
        BatchJobExecution batchJobExecution2 = new BatchJobExecution();
        batchJobExecution2.setId(batchJobExecution1.getId());
        assertThat(batchJobExecution1).isEqualTo(batchJobExecution2);
        batchJobExecution2.setId(2L);
        assertThat(batchJobExecution1).isNotEqualTo(batchJobExecution2);
        batchJobExecution1.setId(null);
        assertThat(batchJobExecution1).isNotEqualTo(batchJobExecution2);
    }
}
