package com.ag04.batchui.domain;

import com.ag04.batchui.web.rest.BatchJobExecutionResourceIT;

import javax.persistence.EntityManager;

public class BatchJobExecutionContextStubFactory {
    private static final String DEFAULT_SHORT_CONTEXT = "AAAAAAAAAA";
    private static final String UPDATED_SHORT_CONTEXT = "BBBBBBBBBB";

    private static final String DEFAULT_SERIALIZED_CONTEXT = "AAAAAAAAAA";
    private static final String UPDATED_SERIALIZED_CONTEXT = "BBBBBBBBBB";

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BatchJobExecutionContext createEntity() {

        BatchJobExecutionContext batchJobExecutionContext = new BatchJobExecutionContext()
                .shortContext(DEFAULT_SHORT_CONTEXT)
                .serializedContext(DEFAULT_SERIALIZED_CONTEXT);
        return batchJobExecutionContext;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BatchJobExecutionContext createUpdatedEntity(EntityManager em) {
        BatchJobExecutionContext batchJobExecutionContext = new BatchJobExecutionContext()
                .shortContext(UPDATED_SHORT_CONTEXT)
                .serializedContext(UPDATED_SERIALIZED_CONTEXT);
        return batchJobExecutionContext;
    }
}
