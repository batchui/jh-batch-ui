package com.ag04.batchui.domain;

import com.ag04.batchui.dbqueue.domain.StartJobCommand;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.ag04.batchui.web.rest.TestUtil;

public class StartJobCommandTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(StartJobCommand.class);
        StartJobCommand startJobCommand1 = new StartJobCommand();
        startJobCommand1.setId(1L);
        StartJobCommand startJobCommand2 = new StartJobCommand();
        startJobCommand2.setId(startJobCommand1.getId());
        assertThat(startJobCommand1).isEqualTo(startJobCommand2);
        startJobCommand2.setId(2L);
        assertThat(startJobCommand1).isNotEqualTo(startJobCommand2);
        startJobCommand1.setId(null);
        assertThat(startJobCommand1).isNotEqualTo(startJobCommand2);
    }
}
