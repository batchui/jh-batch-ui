package com.ag04.batchui.domain;

import com.ag04.batchui.web.rest.BatchJobExecutionResourceIT;

import javax.persistence.EntityManager;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

public class BatchJobExecutionParamsStubFactory {

    private static final String DEFAULT_TYPE_CD = "AAAAAA";
    private static final String UPDATED_TYPE_CD = "BBBBBB";

    private static final String DEFAULT_KEY_NAME = "AAAAAAAAAA";
    private static final String UPDATED_KEY_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_STRING_VAL = "AAAAAAAAAA";
    private static final String UPDATED_STRING_VAL = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_DATE_VAL = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_DATE_VAL = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Long DEFAULT_LONG_VAL = 1L;
    private static final Long UPDATED_LONG_VAL = 2L;

    private static final Double DEFAULT_DOUBLE_VAL = 1D;
    private static final Double UPDATED_DOUBLE_VAL = 2D;

    private static final Character DEFAULT_IDENTIFYING = Character.valueOf('A');
    private static final Character UPDATED_IDENTIFYING = Character.valueOf('B');

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BatchJobExecutionParams createEntity() {
        BatchJobExecutionParams batchJobExecutionParams = new BatchJobExecutionParams()
                .typeCd(DEFAULT_TYPE_CD)
                .keyName(DEFAULT_KEY_NAME)
                .stringVal(DEFAULT_STRING_VAL)
                .dateVal(DEFAULT_DATE_VAL)
                .longVal(DEFAULT_LONG_VAL)
                .doubleVal(DEFAULT_DOUBLE_VAL)
                .identifying(DEFAULT_IDENTIFYING);
        return batchJobExecutionParams;
    }
}
