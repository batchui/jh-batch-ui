package com.ag04.batchui.domain;

public class BatchStepExecutionContextStubFactory {
    private static final String DEFAULT_SHORT_CONTEXT = "AAAAAAAAAA";
    private static final String UPDATED_SHORT_CONTEXT = "BBBBBBBBBB";

    private static final String DEFAULT_SERIALIZED_CONTEXT = "AAAAAAAAAA";
    private static final String UPDATED_SERIALIZED_CONTEXT = "BBBBBBBBBB";

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BatchStepExecutionContext createEntity() {
        BatchStepExecutionContext batchStepExecutionContext = new BatchStepExecutionContext()
                .shortContext(DEFAULT_SHORT_CONTEXT)
                .serializedContext(DEFAULT_SERIALIZED_CONTEXT);
        return batchStepExecutionContext;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static BatchStepExecutionContext createUpdatedEntity() {
        BatchStepExecutionContext batchStepExecutionContext = new BatchStepExecutionContext()
                .shortContext(UPDATED_SHORT_CONTEXT)
                .serializedContext(UPDATED_SERIALIZED_CONTEXT);
        return batchStepExecutionContext;
    }
}
