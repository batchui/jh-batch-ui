import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { BatchJobInstanceService } from 'app/entities/batch-job-instance/batch-job-instance.service';
import { IBatchJobInstance, BatchJobInstance } from 'app/shared/model/batch-job-instance.model';

describe('Service Tests', () => {
  describe('BatchJobInstance Service', () => {
    let injector: TestBed;
    let service: BatchJobInstanceService;
    let httpMock: HttpTestingController;
    let elemDefault: IBatchJobInstance;
    let expectedResult: IBatchJobInstance | IBatchJobInstance[] | boolean | null;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(BatchJobInstanceService);
      httpMock = injector.get(HttpTestingController);

      elemDefault = new BatchJobInstance(0, 0, 'AAAAAAA', 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign({}, elemDefault);

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a BatchJobInstance', () => {
        const returnedFromService = Object.assign(
          {
            id: 0
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.create(new BatchJobInstance()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a BatchJobInstance', () => {
        const returnedFromService = Object.assign(
          {
            jobInstanceId: 1,
            version: 1,
            jobName: 'BBBBBB',
            jobKey: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of BatchJobInstance', () => {
        const returnedFromService = Object.assign(
          {
            jobInstanceId: 1,
            version: 1,
            jobName: 'BBBBBB',
            jobKey: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign({}, returnedFromService);

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a BatchJobInstance', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
