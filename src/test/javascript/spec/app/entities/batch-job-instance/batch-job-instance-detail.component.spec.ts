import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { BatchuiTestModule } from '../../../test.module';
import { BatchJobInstanceDetailComponent } from 'app/entities/batch-job-instance/batch-job-instance-detail.component';
import { BatchJobInstance } from 'app/shared/model/batch-job-instance.model';

describe('Component Tests', () => {
  describe('BatchJobInstance Management Detail Component', () => {
    let comp: BatchJobInstanceDetailComponent;
    let fixture: ComponentFixture<BatchJobInstanceDetailComponent>;
    const route = ({ data: of({ batchJobInstance: new BatchJobInstance(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BatchuiTestModule],
        declarations: [BatchJobInstanceDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(BatchJobInstanceDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BatchJobInstanceDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load batchJobInstance on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.batchJobInstance).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
