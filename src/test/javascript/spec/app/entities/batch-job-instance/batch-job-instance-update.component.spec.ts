import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { BatchuiTestModule } from '../../../test.module';
import { BatchJobInstanceUpdateComponent } from 'app/entities/batch-job-instance/batch-job-instance-update.component';
import { BatchJobInstanceService } from 'app/entities/batch-job-instance/batch-job-instance.service';
import { BatchJobInstance } from 'app/shared/model/batch-job-instance.model';

describe('Component Tests', () => {
  describe('BatchJobInstance Management Update Component', () => {
    let comp: BatchJobInstanceUpdateComponent;
    let fixture: ComponentFixture<BatchJobInstanceUpdateComponent>;
    let service: BatchJobInstanceService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BatchuiTestModule],
        declarations: [BatchJobInstanceUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(BatchJobInstanceUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BatchJobInstanceUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BatchJobInstanceService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BatchJobInstance(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BatchJobInstance();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
