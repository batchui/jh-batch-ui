import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { of } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';

import { BatchuiTestModule } from '../../../test.module';
import { MockEventManager } from '../../../helpers/mock-event-manager.service';
import { MockActiveModal } from '../../../helpers/mock-active-modal.service';
import { BatchJobInstanceDeleteDialogComponent } from 'app/entities/batch-job-instance/batch-job-instance-delete-dialog.component';
import { BatchJobInstanceService } from 'app/entities/batch-job-instance/batch-job-instance.service';
import { LoadingBarService } from '@ngx-loading-bar/core';

describe('Component Tests', () => {
  describe('BatchJobInstance Management Delete Component', () => {
    let comp: BatchJobInstanceDeleteDialogComponent;
    let fixture: ComponentFixture<BatchJobInstanceDeleteDialogComponent>;
    let service: BatchJobInstanceService;
    let mockEventManager: MockEventManager;
    let mockActiveModal: MockActiveModal;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BatchuiTestModule],
        declarations: [BatchJobInstanceDeleteDialogComponent]
      })
        .overrideTemplate(BatchJobInstanceDeleteDialogComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BatchJobInstanceDeleteDialogComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BatchJobInstanceService);
      mockEventManager = TestBed.get(JhiEventManager);
      mockActiveModal = TestBed.get(NgbActiveModal);
    });

    describe('confirmDelete', () => {
      /*
      TODO: fix the problem with the LoadingBarService and tests!
      
      it('Should call delete service on confirmDelete', inject(
        [],
        fakeAsync(() => {
          // GIVEN
          spyOn(service, 'delete').and.returnValue(of({}));

          // WHEN
          comp.confirmDelete(123);
          tick();

          // THEN
          expect(service.delete).toHaveBeenCalledWith(123);
          expect(mockActiveModal.closeSpy).toHaveBeenCalled();
          expect(mockEventManager.broadcastSpy).toHaveBeenCalled();
        })
      ));
      */

      it('Should not call delete service on clear', () => {
        // GIVEN
        spyOn(service, 'delete');

        // WHEN
        comp.cancel();

        // THEN
        expect(service.delete).not.toHaveBeenCalled();
        expect(mockActiveModal.dismissSpy).toHaveBeenCalled();
      });
    });
  });
});
