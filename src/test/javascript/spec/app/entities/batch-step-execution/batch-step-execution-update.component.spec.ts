import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { BatchuiTestModule } from '../../../test.module';
import { BatchStepExecutionUpdateComponent } from 'app/entities/batch-step-execution/batch-step-execution-update.component';
import { BatchStepExecutionService } from 'app/entities/batch-step-execution/batch-step-execution.service';
import { BatchStepExecution } from 'app/shared/model/batch-step-execution.model';

describe('Component Tests', () => {
  describe('BatchStepExecution Management Update Component', () => {
    let comp: BatchStepExecutionUpdateComponent;
    let fixture: ComponentFixture<BatchStepExecutionUpdateComponent>;
    let service: BatchStepExecutionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BatchuiTestModule],
        declarations: [BatchStepExecutionUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(BatchStepExecutionUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BatchStepExecutionUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BatchStepExecutionService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BatchStepExecution(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new BatchStepExecution();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
