import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { BatchuiTestModule } from '../../../test.module';
import { BatchStepExecutionDetailComponent } from 'app/entities/batch-step-execution/batch-step-execution-detail.component';
import { BatchStepExecution } from 'app/shared/model/batch-step-execution.model';

describe('Component Tests', () => {
  describe('BatchStepExecution Management Detail Component', () => {
    let comp: BatchStepExecutionDetailComponent;
    let fixture: ComponentFixture<BatchStepExecutionDetailComponent>;
    const route = ({ data: of({ batchStepExecution: new BatchStepExecution(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BatchuiTestModule],
        declarations: [BatchStepExecutionDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(BatchStepExecutionDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BatchStepExecutionDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load batchStepExecution on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.batchStepExecution).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
