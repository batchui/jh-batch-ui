import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { BatchStepExecutionService } from 'app/entities/batch-step-execution/batch-step-execution.service';
import { IBatchStepExecution, BatchStepExecution } from 'app/shared/model/batch-step-execution.model';

describe('Service Tests', () => {
  describe('BatchStepExecution Service', () => {
    let injector: TestBed;
    let service: BatchStepExecutionService;
    let httpMock: HttpTestingController;
    let elemDefault: IBatchStepExecution;
    let expectedResult: IBatchStepExecution | IBatchStepExecution[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(BatchStepExecutionService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new BatchStepExecution(
        0,
        0,
        'AAAAAAA',
        currentDate,
        currentDate,
        'AAAAAAA',
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        'AAAAAAA',
        'AAAAAAA',
        currentDate
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            startTime: currentDate.format(DATE_TIME_FORMAT),
            endTime: currentDate.format(DATE_TIME_FORMAT),
            lastUpdated: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a BatchStepExecution', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            startTime: currentDate.format(DATE_TIME_FORMAT),
            endTime: currentDate.format(DATE_TIME_FORMAT),
            lastUpdated: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            startTime: currentDate,
            endTime: currentDate,
            lastUpdated: currentDate
          },
          returnedFromService
        );

        service.create(new BatchStepExecution()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a BatchStepExecution', () => {
        const returnedFromService = Object.assign(
          {
            stepExecutionId: 1,
            version: 1,
            stepName: 'BBBBBB',
            startTime: currentDate.format(DATE_TIME_FORMAT),
            endTime: currentDate.format(DATE_TIME_FORMAT),
            status: 'BBBBBB',
            commitCount: 1,
            readCount: 1,
            filterCount: 1,
            writeCount: 1,
            readSkipCount: 1,
            writeSkipCount: 1,
            processSkipCount: 1,
            rollbackCount: 1,
            exitCode: 'BBBBBB',
            exitMessage: 'BBBBBB',
            lastUpdated: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            startTime: currentDate,
            endTime: currentDate,
            lastUpdated: currentDate
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of BatchStepExecution', () => {
        const returnedFromService = Object.assign(
          {
            stepExecutionId: 1,
            version: 1,
            stepName: 'BBBBBB',
            startTime: currentDate.format(DATE_TIME_FORMAT),
            endTime: currentDate.format(DATE_TIME_FORMAT),
            status: 'BBBBBB',
            commitCount: 1,
            readCount: 1,
            filterCount: 1,
            writeCount: 1,
            readSkipCount: 1,
            writeSkipCount: 1,
            processSkipCount: 1,
            rollbackCount: 1,
            exitCode: 'BBBBBB',
            exitMessage: 'BBBBBB',
            lastUpdated: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            startTime: currentDate,
            endTime: currentDate,
            lastUpdated: currentDate
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a BatchStepExecution', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
