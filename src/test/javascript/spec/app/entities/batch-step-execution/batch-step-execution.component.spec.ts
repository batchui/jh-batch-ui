import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { BatchuiTestModule } from '../../../test.module';
import { BatchStepExecutionComponent } from 'app/entities/batch-step-execution/batch-step-execution.component';
import { BatchStepExecutionService } from 'app/entities/batch-step-execution/batch-step-execution.service';
import { BatchStepExecution } from 'app/shared/model/batch-step-execution.model';

describe('Component Tests', () => {
  describe('BatchStepExecution Management Component', () => {
    let comp: BatchStepExecutionComponent;
    let fixture: ComponentFixture<BatchStepExecutionComponent>;
    let service: BatchStepExecutionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BatchuiTestModule],
        declarations: [BatchStepExecutionComponent]
      })
        .overrideTemplate(BatchStepExecutionComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BatchStepExecutionComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BatchStepExecutionService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new BatchStepExecution(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.batchStepExecutions && comp.batchStepExecutions[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
