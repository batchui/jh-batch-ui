import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { BatchJobExecutionService } from 'app/entities/batch-job-execution/batch-job-execution.service';
import { IBatchJobExecution, BatchJobExecution } from 'app/shared/model/batch-job-execution.model';

describe('Service Tests', () => {
  describe('BatchJobExecution Service', () => {
    let injector: TestBed;
    let service: BatchJobExecutionService;
    let httpMock: HttpTestingController;
    let elemDefault: IBatchJobExecution;
    let expectedResult: IBatchJobExecution | IBatchJobExecution[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(BatchJobExecutionService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new BatchJobExecution(
        0,
        0,
        currentDate,
        currentDate,
        currentDate,
        'AAAAAAA',
        'AAAAAAA',
        'AAAAAAA',
        currentDate,
        'AAAAAAA'
      );
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            createTime: currentDate.format(DATE_TIME_FORMAT),
            startTime: currentDate.format(DATE_TIME_FORMAT),
            endTime: currentDate.format(DATE_TIME_FORMAT),
            lastUpdated: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should update a BatchJobExecution', () => {
        const returnedFromService = Object.assign(
          {
            jobExecutionId: 1,
            version: 1,
            createTime: currentDate.format(DATE_TIME_FORMAT),
            startTime: currentDate.format(DATE_TIME_FORMAT),
            endTime: currentDate.format(DATE_TIME_FORMAT),
            status: 'BBBBBB',
            exitCode: 'BBBBBB',
            exitMessage: 'BBBBBB',
            lastUpdated: currentDate.format(DATE_TIME_FORMAT),
            jobConfigurationLocation: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createTime: currentDate,
            startTime: currentDate,
            endTime: currentDate,
            lastUpdated: currentDate
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of BatchJobExecution', () => {
        const returnedFromService = Object.assign(
          {
            jobExecutionId: 1,
            version: 1,
            createTime: currentDate.format(DATE_TIME_FORMAT),
            startTime: currentDate.format(DATE_TIME_FORMAT),
            endTime: currentDate.format(DATE_TIME_FORMAT),
            status: 'BBBBBB',
            exitCode: 'BBBBBB',
            exitMessage: 'BBBBBB',
            lastUpdated: currentDate.format(DATE_TIME_FORMAT),
            jobConfigurationLocation: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            createTime: currentDate,
            startTime: currentDate,
            endTime: currentDate,
            lastUpdated: currentDate
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a BatchJobExecution', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
