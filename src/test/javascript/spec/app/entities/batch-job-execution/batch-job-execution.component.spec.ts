import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Data } from '@angular/router';

import { BatchuiTestModule } from '../../../test.module';
import { BatchJobExecutionComponent } from 'app/entities/batch-job-execution/batch-job-execution.component';
import { BatchJobExecutionService } from 'app/entities/batch-job-execution/batch-job-execution.service';
import { BatchJobExecution } from 'app/shared/model/batch-job-execution.model';

describe('Component Tests', () => {
  describe('BatchJobExecution Management Component', () => {
    let comp: BatchJobExecutionComponent;
    let fixture: ComponentFixture<BatchJobExecutionComponent>;
    let service: BatchJobExecutionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BatchuiTestModule],
        declarations: [BatchJobExecutionComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              data: {
                subscribe: (fn: (value: Data) => void) =>
                  fn({
                    pagingParams: {
                      predicate: 'id',
                      reverse: false,
                      page: 0
                    }
                  })
              }
            }
          }
        ]
      })
        .overrideTemplate(BatchJobExecutionComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BatchJobExecutionComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BatchJobExecutionService);
    });

    it('should load a page', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new BatchJobExecution(123)],
            headers
          })
        )
      );

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.batchJobExecutions && comp.batchJobExecutions[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      comp.ngOnInit();
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,desc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // INIT
      comp.ngOnInit();

      // GIVEN
      comp.predicate = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,desc', 'id']);
    });
  });
});
