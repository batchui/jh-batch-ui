import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { BatchuiTestModule } from '../../../test.module';
import { BatchJobExecutionDetailComponent } from 'app/entities/batch-job-execution/batch-job-execution-detail.component';
import { BatchJobExecution } from 'app/shared/model/batch-job-execution.model';

describe('Component Tests', () => {
  describe('BatchJobExecution Management Detail Component', () => {
    let comp: BatchJobExecutionDetailComponent;
    let fixture: ComponentFixture<BatchJobExecutionDetailComponent>;
    const route = ({ data: of({ batchJobExecution: new BatchJobExecution(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BatchuiTestModule],
        declarations: [BatchJobExecutionDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(BatchJobExecutionDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(BatchJobExecutionDetailComponent);
      comp = fixture.componentInstance;
    });

    describe('OnInit', () => {
      it('Should load batchJobExecution on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.batchJobExecution).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });
  });
});
