import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { BatchuiTestModule } from '../../../test.module';
import { BatchJobExecutionUpdateComponent } from 'app/entities/batch-job-execution/batch-job-execution-update.component';
import { BatchJobExecutionService } from 'app/entities/batch-job-execution/batch-job-execution.service';
import { BatchJobExecution } from 'app/shared/model/batch-job-execution.model';

describe('Component Tests', () => {
  describe('BatchJobExecution Management Update Component', () => {
    let comp: BatchJobExecutionUpdateComponent;
    let fixture: ComponentFixture<BatchJobExecutionUpdateComponent>;
    let service: BatchJobExecutionService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BatchuiTestModule],
        declarations: [BatchJobExecutionUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(BatchJobExecutionUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(BatchJobExecutionUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(BatchJobExecutionService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new BatchJobExecution(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
