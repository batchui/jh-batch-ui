import { ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { of } from 'rxjs';

import { BatchuiTestModule } from '../../../test.module';
import { StartJobCommandUpdateComponent } from 'app/entities/start-job-command/start-job-command-update.component';
import { StartJobCommandService } from 'app/entities/start-job-command/start-job-command.service';
import { StartJobCommand } from 'app/shared/model/start-job-command.model';

describe('Component Tests', () => {
  describe('StartJobCommand Management Update Component', () => {
    let comp: StartJobCommandUpdateComponent;
    let fixture: ComponentFixture<StartJobCommandUpdateComponent>;
    let service: StartJobCommandService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BatchuiTestModule],
        declarations: [StartJobCommandUpdateComponent],
        providers: [FormBuilder]
      })
        .overrideTemplate(StartJobCommandUpdateComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(StartJobCommandUpdateComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(StartJobCommandService);
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', fakeAsync(() => {
        // GIVEN
        const entity = new StartJobCommand(123);
        spyOn(service, 'update').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.update).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));

      it('Should call create service on save for new entity', fakeAsync(() => {
        // GIVEN
        const entity = new StartJobCommand();
        spyOn(service, 'create').and.returnValue(of(new HttpResponse({ body: entity })));
        comp.updateForm(entity);
        // WHEN
        comp.save();
        tick(); // simulate async

        // THEN
        expect(service.create).toHaveBeenCalledWith(entity);
        expect(comp.isSaving).toEqual(false);
      }));
    });
  });
});
