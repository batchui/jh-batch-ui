import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Data } from '@angular/router';

import { BatchuiTestModule } from '../../../test.module';
import { StartJobCommandComponent } from 'app/entities/start-job-command/start-job-command.component';
import { StartJobCommandService } from 'app/entities/start-job-command/start-job-command.service';
import { StartJobCommand } from 'app/shared/model/start-job-command.model';

describe('Component Tests', () => {
  describe('StartJobCommand Management Component', () => {
    let comp: StartJobCommandComponent;
    let fixture: ComponentFixture<StartJobCommandComponent>;
    let service: StartJobCommandService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BatchuiTestModule],
        declarations: [StartJobCommandComponent],
        providers: [
          {
            provide: ActivatedRoute,
            useValue: {
              data: {
                subscribe: (fn: (value: Data) => void) =>
                  fn({
                    pagingParams: {
                      predicate: 'id',
                      reverse: false,
                      page: 0
                    }
                  })
              }
            }
          }
        ]
      })
        .overrideTemplate(StartJobCommandComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(StartJobCommandComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(StartJobCommandService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new StartJobCommand(123)],
            headers
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.startJobCommands && comp.startJobCommands[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should load a page', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new StartJobCommand(123)],
            headers
          })
        )
      );

      // WHEN
      comp.loadPage(1);

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.startJobCommands && comp.startJobCommands[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });

    it('should calculate the sort attribute for an id', () => {
      // WHEN
      comp.ngOnInit();
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['id,desc']);
    });

    it('should calculate the sort attribute for a non-id attribute', () => {
      // INIT
      comp.ngOnInit();

      // GIVEN
      comp.predicate = 'name';

      // WHEN
      const result = comp.sort();

      // THEN
      expect(result).toEqual(['name,desc', 'id']);
    });
  });
});
