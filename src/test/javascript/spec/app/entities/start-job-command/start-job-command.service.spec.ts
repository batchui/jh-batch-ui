import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { StartJobCommandService } from 'app/entities/start-job-command/start-job-command.service';
import { IStartJobCommand, StartJobCommand } from 'app/shared/model/start-job-command.model';
import { QueueStatus } from 'app/shared/model/enumerations/queue-status.model';

describe('Service Tests', () => {
  describe('StartJobCommand Service', () => {
    let injector: TestBed;
    let service: StartJobCommandService;
    let httpMock: HttpTestingController;
    let elemDefault: IStartJobCommand;
    let expectedResult: IStartJobCommand | IStartJobCommand[] | boolean | null;
    let currentDate: moment.Moment;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [HttpClientTestingModule]
      });
      expectedResult = null;
      injector = getTestBed();
      service = injector.get(StartJobCommandService);
      httpMock = injector.get(HttpTestingController);
      currentDate = moment();

      elemDefault = new StartJobCommand(0, 'AAAAAAA', 'AAAAAAA', QueueStatus.NOT_ATTEMPTED, currentDate, 0, currentDate, 'AAAAAAA');
    });

    describe('Service methods', () => {
      it('should find an element', () => {
        const returnedFromService = Object.assign(
          {
            nextAttemptTime: currentDate.format(DATE_TIME_FORMAT),
            lastAttemptTime: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        service.find(123).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(elemDefault);
      });

      it('should create a StartJobCommand', () => {
        const returnedFromService = Object.assign(
          {
            id: 0,
            nextAttemptTime: currentDate.format(DATE_TIME_FORMAT),
            lastAttemptTime: currentDate.format(DATE_TIME_FORMAT)
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            nextAttemptTime: currentDate,
            lastAttemptTime: currentDate
          },
          returnedFromService
        );

        service.create(new StartJobCommand()).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'POST' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should update a StartJobCommand', () => {
        const returnedFromService = Object.assign(
          {
            jobName: 'BBBBBB',
            jobParams: 'BBBBBB',
            status: 'BBBBBB',
            nextAttemptTime: currentDate.format(DATE_TIME_FORMAT),
            attemptCount: 1,
            lastAttemptTime: currentDate.format(DATE_TIME_FORMAT),
            lastAttemptErrorMessage: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            nextAttemptTime: currentDate,
            lastAttemptTime: currentDate
          },
          returnedFromService
        );

        service.update(expected).subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'PUT' });
        req.flush(returnedFromService);
        expect(expectedResult).toMatchObject(expected);
      });

      it('should return a list of StartJobCommand', () => {
        const returnedFromService = Object.assign(
          {
            jobName: 'BBBBBB',
            jobParams: 'BBBBBB',
            status: 'BBBBBB',
            nextAttemptTime: currentDate.format(DATE_TIME_FORMAT),
            attemptCount: 1,
            lastAttemptTime: currentDate.format(DATE_TIME_FORMAT),
            lastAttemptErrorMessage: 'BBBBBB'
          },
          elemDefault
        );

        const expected = Object.assign(
          {
            nextAttemptTime: currentDate,
            lastAttemptTime: currentDate
          },
          returnedFromService
        );

        service.query().subscribe(resp => (expectedResult = resp.body));

        const req = httpMock.expectOne({ method: 'GET' });
        req.flush([returnedFromService]);
        httpMock.verify();
        expect(expectedResult).toContainEqual(expected);
      });

      it('should delete a StartJobCommand', () => {
        service.delete(123).subscribe(resp => (expectedResult = resp.ok));

        const req = httpMock.expectOne({ method: 'DELETE' });
        req.flush({ status: 200 });
        expect(expectedResult);
      });
    });

    afterEach(() => {
      httpMock.verify();
    });
  });
});
