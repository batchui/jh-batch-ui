import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';
import { JhiDataUtils } from 'ng-jhipster';

import { BatchuiTestModule } from '../../../test.module';
import { StartJobCommandDetailComponent } from 'app/entities/start-job-command/start-job-command-detail.component';
import { StartJobCommand } from 'app/shared/model/start-job-command.model';

describe('Component Tests', () => {
  describe('StartJobCommand Management Detail Component', () => {
    let comp: StartJobCommandDetailComponent;
    let fixture: ComponentFixture<StartJobCommandDetailComponent>;
    let dataUtils: JhiDataUtils;
    const route = ({ data: of({ startJobCommand: new StartJobCommand(123) }) } as any) as ActivatedRoute;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [BatchuiTestModule],
        declarations: [StartJobCommandDetailComponent],
        providers: [{ provide: ActivatedRoute, useValue: route }]
      })
        .overrideTemplate(StartJobCommandDetailComponent, '')
        .compileComponents();
      fixture = TestBed.createComponent(StartJobCommandDetailComponent);
      comp = fixture.componentInstance;
      dataUtils = fixture.debugElement.injector.get(JhiDataUtils);
    });

    describe('OnInit', () => {
      it('Should load startJobCommand on init', () => {
        // WHEN
        comp.ngOnInit();

        // THEN
        expect(comp.startJobCommand).toEqual(jasmine.objectContaining({ id: 123 }));
      });
    });

    describe('byteSize', () => {
      it('Should call byteSize from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'byteSize');
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.byteSize(fakeBase64);

        // THEN
        expect(dataUtils.byteSize).toBeCalledWith(fakeBase64);
      });
    });

    describe('openFile', () => {
      it('Should call openFile from JhiDataUtils', () => {
        // GIVEN
        spyOn(dataUtils, 'openFile');
        const fakeContentType = 'fake content type';
        const fakeBase64 = 'fake base64';

        // WHEN
        comp.openFile(fakeContentType, fakeBase64);

        // THEN
        expect(dataUtils.openFile).toBeCalledWith(fakeContentType, fakeBase64);
      });
    });
  });
});
