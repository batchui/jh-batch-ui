import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import './vendor';
import { BatchuiSharedModule } from 'app/shared/shared.module';
import { BatchuiCoreModule } from 'app/core/core.module';
import { BatchuiAppRoutingModule } from './app-routing.module';
import { BatchuiHomeModule } from './home/home.module';
import { BatchuiEntityModule } from './entities/entity.module';
// jhipster-needle-angular-add-module-import JHipster will add new module here
import { MainComponent } from './layouts/main/main.component';
import { NavbarComponent } from './layouts/navbar/navbar.component';
import { FooterComponent } from './layouts/footer/footer.component';
import { PageRibbonComponent } from './layouts/profiles/page-ribbon.component';
import { ActiveMenuDirective } from './layouts/navbar/active-menu.directive';
import { ErrorComponent } from './layouts/error/error.component';
import { ChartsModule } from 'ng2-charts';
import { LoadingBarModule } from '@ngx-loading-bar/core';
import { LoadingBarRouterModule } from '@ngx-loading-bar/router';

@NgModule({
  imports: [
    BrowserModule,
    BatchuiSharedModule,
    BatchuiCoreModule,
    BatchuiHomeModule,
    // jhipster-needle-angular-add-module JHipster will add new module here
    BatchuiEntityModule,
    BatchuiAppRoutingModule,
    ChartsModule,
    LoadingBarModule,
    LoadingBarRouterModule
  ],
  declarations: [MainComponent, NavbarComponent, ErrorComponent, PageRibbonComponent, ActiveMenuDirective, FooterComponent],
  bootstrap: [MainComponent]
})
export class BatchuiAppModule {}
