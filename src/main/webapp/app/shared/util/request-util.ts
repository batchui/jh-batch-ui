import { HttpParams } from '@angular/common/http';
import { ParamMap } from '@angular/router';

export interface Pagination {
  page: number;
  size: number;
  sort: string[];
}

export interface Search {
  query: string;
}

export interface SearchWithPagination extends Search, Pagination {}

export const createRequestOption = (req?: any): HttpParams => {
  let options: HttpParams = new HttpParams();

  if (req) {
    Object.keys(req).forEach(key => {
      if (key !== 'sort') {
        options = options.set(key, req[key]);
      }
    });

    if (req.sort) {
      req.sort.forEach((val: string) => {
        options = options.append('sort', val);
      });
    }
  }

  return options;
};

export function parseBoolean(value: string | null, defaultVal: boolean | undefined = undefined): boolean | undefined {
  let result = defaultVal;
  if (value != null) {
    const valLowerCase = value.toLowerCase();
    if (valLowerCase === 'true' || valLowerCase === 'false') {
      result = valLowerCase === 'true' ? true : false;
    }
  }
  return result;
}

export function getParam(params: ParamMap, paramName: string): string | undefined {
  const param = params.get(paramName);
  return param ? param : undefined;
}
