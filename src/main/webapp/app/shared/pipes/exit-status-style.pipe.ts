import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'exitStatusStyle'
})
export class ExitStatusStylePipe implements PipeTransform {
  private defaultStyle = 'light';

  transform(exitStatus: string, prefix: string): string {
    let val: string = this.defaultStyle;
    if ('COMPLETED' === exitStatus) {
      val = prefix + 'success';
      return val;
    }

    if ('EXECUTING' === exitStatus) {
      val = prefix + 'warning';
      return val;
    }

    if ('FAILED' === exitStatus) {
      val = prefix + 'danger';
      return val;
    }

    if ('NOOP' === exitStatus) {
      val = prefix + 'info';
      return val;
    }

    if ('STOPPED' === exitStatus) {
      val = prefix + 'secondary';
      return val;
    }

    if ('UNKNOWN' === exitStatus) {
      val = prefix + 'dark';
      return val;
    }

    return val;
  }
}
