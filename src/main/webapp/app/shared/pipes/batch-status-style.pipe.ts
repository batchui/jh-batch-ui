import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'batchStatusStyle'
})
export class BatchStatusStylePipe implements PipeTransform {
  private defaultStyle = 'light';

  transform(batchStatus: string, prefix: string): string {
    let val: string = this.defaultStyle;

    if ('COMPLETED' === batchStatus) {
      val = prefix + 'success';
      return val;
    }

    if ('ABANDONED' === batchStatus) {
      val = prefix + 'warning';
      return val;
    }

    if ('FAILED' === batchStatus) {
      val = prefix + 'danger';
      return val;
    }

    if ('STARTED' === batchStatus) {
      val = prefix + 'primary';
      return val;
    }

    if ('STOPPED' === batchStatus) {
      val = prefix + 'info';
      return val;
    }

    if ('STARTING' === batchStatus || 'STOPPING' === batchStatus) {
      val = prefix + 'secondary';
      return val;
    }

    if ('UNKNOWN' === batchStatus) {
      val = prefix + 'dark';
      return val;
    }

    return val;
  }
}
