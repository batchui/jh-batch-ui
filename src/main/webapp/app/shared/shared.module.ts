import { NgModule } from '@angular/core';
import { BatchuiSharedLibsModule } from './shared-libs.module';
import { FindLanguageFromKeyPipe } from './language/find-language-from-key.pipe';
import { AlertComponent } from './alert/alert.component';
import { AlertErrorComponent } from './alert/alert-error.component';
import { LoginModalComponent } from './login/login.component';
import { HasAnyAuthorityDirective } from './auth/has-any-authority.directive';
import { ExitStatusStylePipe } from './pipes/exit-status-style.pipe';
import { BatchStatusStylePipe } from './pipes/batch-status-style.pipe';
import { PrettyjsonPipe } from './pipes/prettyjson.pipe';

@NgModule({
  imports: [BatchuiSharedLibsModule],
  declarations: [
    FindLanguageFromKeyPipe,
    AlertComponent,
    AlertErrorComponent,
    LoginModalComponent,
    HasAnyAuthorityDirective,
    ExitStatusStylePipe,
    BatchStatusStylePipe,
    PrettyjsonPipe
  ],
  entryComponents: [LoginModalComponent],
  exports: [
    BatchuiSharedLibsModule,
    FindLanguageFromKeyPipe,
    AlertComponent,
    AlertErrorComponent,
    LoginModalComponent,
    HasAnyAuthorityDirective,
    ExitStatusStylePipe,
    BatchStatusStylePipe,
    PrettyjsonPipe
  ]
})
export class BatchuiSharedModule {}
