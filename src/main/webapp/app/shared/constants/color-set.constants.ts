export const COLOR_SET_8 = ['#003f5c', '#2f4b7c', '#665191', '#a05195', '#d45087', '#f95d6a', '#ff7c43', '#ffa600'];

export const COLOR_SET_6 = ['#D21F75', '#9067A7', '#CCC210', '#D45087', '#FF7C43', '#FFA600'];

export const COLOR_SET_16 = [
  '#267278',
  '#D21F75',
  '#4770B3',
  '#3B3689',
  '#65338D',
  '#50AED3',
  '#48B24F',
  '#E57438',
  '#569DD2',
  '#569D79',
  '#58595B',
  '#E4B031',
  '#84DsF4',
  '#CAD93F',
  '#F5C8AF',
  '#9AC483',
  '#9E9EA2'
];
