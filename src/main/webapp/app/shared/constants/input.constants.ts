export const DATE_FORMAT = 'YYYY-MM-DD';
export const DATE_TIME_FORMAT = 'YYYY-MM-DDTHH:mm';
export const DATE_TIME_FORMAT_ISO = 'YYYY-MM-DDTHH:mm:ss.SSS';
