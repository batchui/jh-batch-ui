import { Moment } from 'moment';
import { QueueStatus } from 'app/shared/model/enumerations/queue-status.model';

export interface IStartJobCommand {
  id?: number;
  jobName?: string;
  jobParams?: any;
  status?: QueueStatus;
  nextAttemptTime?: Moment;
  attemptCount?: number;
  lastAttemptTime?: Moment;
  lastAttemptErrorMessage?: string;
}

export class StartJobCommand implements IStartJobCommand {
  constructor(
    public id?: number,
    public jobName?: string,
    public jobParams?: any,
    public status?: QueueStatus,
    public nextAttemptTime?: Moment,
    public attemptCount?: number,
    public lastAttemptTime?: Moment,
    public lastAttemptErrorMessage?: string
  ) {}
}
