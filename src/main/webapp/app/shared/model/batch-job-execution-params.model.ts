import { Moment } from 'moment';

export interface IBatchJobExecutionParams {
  typeCd?: string;
  keyName?: string;
  stringVal?: string;
  dateVal?: Moment;
  longVal?: number;
  doubleVal?: number;
  identifying?: string;
}

export class BatchJobExecutionParams implements IBatchJobExecutionParams {
  constructor(
    public typeCd?: string,
    public keyName?: string,
    public stringVal?: string,
    public dateVal?: Moment,
    public longVal?: number,
    public doubleVal?: number,
    public identifying?: string
  ) {}
}
