export interface IJobStatusCount {
  status?: string;
  count?: number;
}

export class JobStatusCount {
  constructor(public count?: number, public status?: string) {}
}

export interface IJobStatusReport {
  jobCount?: number;
  batchStatuses?: IJobStatusCount[];
  exitCodes?: IJobStatusCount[];
}

export class JobStatusReport implements IJobStatusReport {
  constructor(public jobCount?: number, public batchStatuses?: IJobStatusCount[], public exitCodes?: IJobStatusCount[]) {}
}
