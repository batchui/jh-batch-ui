import { Moment } from 'moment';
import { IBatchStepExecution } from 'app/shared/model/batch-step-execution.model';
import { IBatchJobExecutionParams } from './batch-job-execution-params.model';
import { IBatchJobExecutionContext } from './batch-job-execution-context.model';

export interface IBatchJobExecution {
  id?: number;
  version?: number;
  createTime?: Moment;
  startTime?: Moment;
  endTime?: Moment;
  status?: string;
  exitCode?: string;
  exitMessage?: string;
  lastUpdated?: Moment;
  jobConfigurationLocation?: string;
  batchStepExecutions?: IBatchStepExecution[];
  batchJobInstanceId?: number;
  jobName?: string;
  batchJobExecutionParams?: IBatchJobExecutionParams[];
  batchJobExecutionContexts?: IBatchJobExecutionContext[];
}

export class BatchJobExecution implements IBatchJobExecution {
  constructor(
    public id?: number,
    public version?: number,
    public createTime?: Moment,
    public startTime?: Moment,
    public endTime?: Moment,
    public status?: string,
    public exitCode?: string,
    public exitMessage?: string,
    public lastUpdated?: Moment,
    public jobConfigurationLocation?: string,
    public batchStepExecutions?: IBatchStepExecution[],
    public batchJobInstanceId?: number,
    public jobName?: string,
    public batchJobExecutionParams?: IBatchJobExecutionParams[],
    public batchJobExecutionContexts?: IBatchJobExecutionContext[]
  ) {}
}
