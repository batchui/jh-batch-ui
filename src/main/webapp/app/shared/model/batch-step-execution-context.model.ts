export interface IBatchStepExecutionContext {
  id?: number;
  stepExecutionId?: number;
  shortContext?: string;
  serializedContext?: any;
}

export class BatchStepExecutionContext implements IBatchStepExecutionContext {
  constructor(public id?: number, public stepExecutionId?: number, public shortContext?: string, public serializedContext?: any) {}
}
