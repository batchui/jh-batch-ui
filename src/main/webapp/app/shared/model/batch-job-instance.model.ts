import { IBatchJobExecution } from 'app/shared/model/batch-job-execution.model';

export interface IBatchJobInstance {
  id?: number;
  jobInstanceId?: number;
  version?: number;
  jobName?: string;
  jobKey?: string;
  batchJobExecutions?: IBatchJobExecution[];
}

export class BatchJobInstance implements IBatchJobInstance {
  constructor(
    public id?: number,
    public version?: number,
    public jobName?: string,
    public jobKey?: string,
    public batchJobExecutions?: IBatchJobExecution[]
  ) {}
}
