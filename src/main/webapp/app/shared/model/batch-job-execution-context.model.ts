export interface IBatchJobExecutionContext {
  shortContext?: string;
  serializedContext?: any;
}

export class BatchJobExecutionContext implements IBatchJobExecutionContext {
  constructor(public id?: number, public jobExecutionId?: number, public shortContext?: string, public serializedContext?: any) {}
}
