export const enum QueueStatus {
  NOT_ATTEMPTED = 'NOT_ATTEMPTED',
  ERROR = 'ERROR',
  SUCCESS = 'SUCCESS'
}
