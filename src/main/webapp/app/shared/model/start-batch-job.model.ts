import { IBatchJobExecutionParams } from './batch-job-execution-params.model';

export interface IStartBatchJob {
  jobName?: string;
  batchJobExecutionParams?: IBatchJobExecutionParams[];
}

export class StartBatchJob implements IStartBatchJob {
  constructor(public jobName?: string, public batchJobExecutionParams?: IBatchJobExecutionParams[]) {}
}
