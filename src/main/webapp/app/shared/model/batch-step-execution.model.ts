import { Moment } from 'moment';

export interface IBatchStepExecution {
  id?: number;
  version?: number;
  stepName?: string;
  startTime?: Moment;
  endTime?: Moment;
  status?: string;
  commitCount?: number;
  readCount?: number;
  filterCount?: number;
  writeCount?: number;
  readSkipCount?: number;
  writeSkipCount?: number;
  processSkipCount?: number;
  rollbackCount?: number;
  exitCode?: string;
  exitMessage?: string;
  lastUpdated?: Moment;
  batchJobExecutionId?: number;
}

export class BatchStepExecution implements IBatchStepExecution {
  constructor(
    public id?: number,
    public version?: number,
    public stepName?: string,
    public startTime?: Moment,
    public endTime?: Moment,
    public status?: string,
    public commitCount?: number,
    public readCount?: number,
    public filterCount?: number,
    public writeCount?: number,
    public readSkipCount?: number,
    public writeSkipCount?: number,
    public processSkipCount?: number,
    public rollbackCount?: number,
    public exitCode?: string,
    public exitMessage?: string,
    public lastUpdated?: Moment,
    public batchJobExecutionId?: number
  ) {}
}
