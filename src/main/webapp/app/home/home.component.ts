import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';

import { BatchJobExecutionService } from 'app/entities/batch-job-execution/batch-job-execution.service';
import { LoginModalService } from 'app/core/login/login-modal.service';
import { AccountService } from 'app/core/auth/account.service';
import { Account } from 'app/core/user/account.model';
import { IJobStatusReport } from 'app/shared/model/job-status-report.dto';

@Component({
  selector: 'jhi-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  account: Account | null = null;
  authSubscription?: Subscription;
  jobStatusReport?: IJobStatusReport | null;

  successJobCount = 0;
  failedJobCount = 0;
  totalJobCount = 0;

  constructor(
    private accountService: AccountService,
    private batchJobExecutionService: BatchJobExecutionService,
    private loginModalService: LoginModalService
  ) {}

  ngOnInit(): void {
    this.authSubscription = this.accountService.getAuthenticationState().subscribe(account => (this.account = account));
    this.batchJobExecutionService.jobStatusReport().subscribe(res => this.processJobStatusReport(res.body));
  }

  isAuthenticated(): boolean {
    return this.accountService.isAuthenticated();
  }

  login(): void {
    this.loginModalService.open();
  }

  ngOnDestroy(): void {
    if (this.authSubscription) {
      this.authSubscription.unsubscribe();
    }
  }

  private processJobStatusReport(jobStatusReport: IJobStatusReport | null): void {
    if (jobStatusReport) {
      this.jobStatusReport = jobStatusReport;

      this.totalJobCount = jobStatusReport.jobCount ? jobStatusReport.jobCount : 0;
      this.failedJobCount = 0;
      this.successJobCount = 0;

      if (jobStatusReport.batchStatuses) {
        for (const statusCount of jobStatusReport.batchStatuses) {
          if (statusCount.status) {
            if (statusCount.status === 'COMPLETED') {
              this.successJobCount = statusCount.count ? statusCount.count : 0;
            }
            if (statusCount.status === 'FAILED') {
              this.failedJobCount = statusCount.count ? statusCount.count : 0;
            }
          }
        }
      }
    }
  }
}
