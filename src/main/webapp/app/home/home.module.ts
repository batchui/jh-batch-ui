import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ChartsModule } from 'ng2-charts';
import { BatchuiSharedModule } from 'app/shared/shared.module';
import { HOME_ROUTE } from './home.route';
import { HomeComponent } from './home.component';
import { ExitCodesReportComponent } from './widgets/exit-codes-report.component';

@NgModule({
  imports: [BatchuiSharedModule, RouterModule.forChild([HOME_ROUTE]), ChartsModule],
  declarations: [HomeComponent, ExitCodesReportComponent],
  exports: [ExitCodesReportComponent]
})
export class BatchuiHomeModule {}
