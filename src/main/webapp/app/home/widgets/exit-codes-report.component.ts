import { Component, OnInit, Input } from '@angular/core';
import { BatchJobExecutionService } from 'app/entities/batch-job-execution/batch-job-execution.service';
import { IJobStatusReport, IJobStatusCount } from 'app/shared/model/job-status-report.dto';
import { Moment } from 'moment';
import { COLOR_SET_16 } from 'app/shared/constants/color-set.constants';
import { Label } from 'ng2-charts';
import { ChartType, ChartOptions } from 'chart.js';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { LoadingBarService } from '@ngx-loading-bar/core';

@Component({
  selector: 'jhi-exit-codes-report',
  templateUrl: './exit-codes-report.component.html'
})
export class ExitCodesReportComponent implements OnInit {
  exitCodesSearchEnabled = true;
  fromDateDp: any;
  toDateDp: any;

  _exitCodes?: IJobStatusCount[] | null;

  @Input()
  fromDate?: Moment;

  @Input()
  toDate?: Moment;

  @Input()
  public set exitCodes(exitCodes: IJobStatusCount[]) {
    this._exitCodes = exitCodes;
    this.processExitCodes();
  }

  // --- Chartsjs PieChart options ------------------------------------------

  public chartReady = false;
  public chartType: ChartType = 'doughnut';
  public chartLegend = true;
  public chartLabels: Label[] = [];
  public chartData: number[] = [];
  public chartPlugins = [pluginDataLabels];

  public chartColors: any[] = [];
  public chartOptions: ChartOptions = {
    legend: {
      position: 'right'
    },
    cutoutPercentage: 40,
    plugins: {
      datalabels: {
        color: 'white',
        font: {
          weight: 'bold',
          size: 16
        },
        align: 'center',
        anchor: 'center'
      }
    }
  };

  constructor(protected batchJobExecutionService: BatchJobExecutionService, protected loadingBarService: LoadingBarService) {
    //
  }

  ngOnInit(): void {
    this.populateColors();
  }

  getJobStatusReport(): void {
    this.exitCodesSearchEnabled = false;
    this.loadingBarService.start();
    this.batchJobExecutionService.jobStatusReport(this.fromDate, this.toDate).subscribe(
      res => {
        this.onSuccess(res.body);
      },
      () => this.onError()
    );
  }

  // --- util private methods -----------------------------------------------

  private onSuccess(jobStatusReport: IJobStatusReport | null): void {
    this.exitCodesSearchEnabled = true;
    this.loadingBarService.complete();
    if (jobStatusReport) {
      this._exitCodes = jobStatusReport.exitCodes;
      this.processExitCodes();
    }
  }

  private processExitCodes(): void {
    if (this._exitCodes) {
      this.chartLabels = [];
      this.chartData = [];
      for (const exitCode of this._exitCodes) {
        if (exitCode.status) {
          this.chartData.push(exitCode.count!);
          this.chartLabels.push(exitCode.status);
        }
      }
      this.chartReady = true;
    }
  }

  private onError(): void {
    this.exitCodesSearchEnabled = true;
  }

  private populateColors(): void {
    const backgroundColors = [];
    const borderColors = [];
    for (const color of COLOR_SET_16) {
      backgroundColors.push(color);
      borderColors.push('rgba(77,83,96,1)');
    }
    const c = {
      backgroundColor: backgroundColors,
      borderColor: borderColors
    };
    this.chartColors.push(c);
    this.chartReady = true;
  }
}
