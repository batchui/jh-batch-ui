import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBatchStepExecution } from 'app/shared/model/batch-step-execution.model';
import { BatchStepExecutionService } from './batch-step-execution.service';

@Component({
  templateUrl: './batch-step-execution-delete-dialog.component.html'
})
export class BatchStepExecutionDeleteDialogComponent {
  batchStepExecution?: IBatchStepExecution;

  constructor(
    protected batchStepExecutionService: BatchStepExecutionService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.batchStepExecutionService.delete(id).subscribe(() => {
      this.eventManager.broadcast('batchStepExecutionListModification');
      this.activeModal.close();
    });
  }
}
