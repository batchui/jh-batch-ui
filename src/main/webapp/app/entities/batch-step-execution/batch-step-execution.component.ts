import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IBatchStepExecution } from 'app/shared/model/batch-step-execution.model';
import { BatchStepExecutionService } from './batch-step-execution.service';
import { BatchStepExecutionDeleteDialogComponent } from './batch-step-execution-delete-dialog.component';

@Component({
  selector: 'jhi-batch-step-execution',
  templateUrl: './batch-step-execution.component.html'
})
export class BatchStepExecutionComponent implements OnInit, OnDestroy {
  batchStepExecutions?: IBatchStepExecution[];
  eventSubscriber?: Subscription;

  constructor(
    protected batchStepExecutionService: BatchStepExecutionService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.batchStepExecutionService
      .query()
      .subscribe((res: HttpResponse<IBatchStepExecution[]>) => (this.batchStepExecutions = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInBatchStepExecutions();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IBatchStepExecution): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInBatchStepExecutions(): void {
    this.eventSubscriber = this.eventManager.subscribe('batchStepExecutionListModification', () => this.loadAll());
  }

  delete(batchStepExecution: IBatchStepExecution): void {
    const modalRef = this.modalService.open(BatchStepExecutionDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.batchStepExecution = batchStepExecution;
  }
}
