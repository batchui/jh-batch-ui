import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IBatchStepExecution } from 'app/shared/model/batch-step-execution.model';

@Component({
  selector: 'jhi-batch-step-execution-detail',
  templateUrl: './batch-step-execution-detail.component.html'
})
export class BatchStepExecutionDetailComponent implements OnInit {
  batchStepExecution: IBatchStepExecution | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ batchStepExecution }) => (this.batchStepExecution = batchStepExecution));
  }

  previousState(): void {
    window.history.back();
  }
}
