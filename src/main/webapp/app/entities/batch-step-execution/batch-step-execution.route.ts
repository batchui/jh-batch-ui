import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBatchStepExecution, BatchStepExecution } from 'app/shared/model/batch-step-execution.model';
import { BatchStepExecutionService } from './batch-step-execution.service';
import { BatchStepExecutionComponent } from './batch-step-execution.component';
import { BatchStepExecutionDetailComponent } from './batch-step-execution-detail.component';
import { BatchStepExecutionUpdateComponent } from './batch-step-execution-update.component';

@Injectable({ providedIn: 'root' })
export class BatchStepExecutionResolve implements Resolve<IBatchStepExecution> {
  constructor(private service: BatchStepExecutionService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBatchStepExecution> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((batchStepExecution: HttpResponse<BatchStepExecution>) => {
          if (batchStepExecution.body) {
            return of(batchStepExecution.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BatchStepExecution());
  }
}

export const batchStepExecutionRoute: Routes = [
  {
    path: '',
    component: BatchStepExecutionComponent,
    data: {
      authorities: [Authority.USER],
      pageTitle: 'batchuiApp.batchStepExecution.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: BatchStepExecutionDetailComponent,
    resolve: {
      batchStepExecution: BatchStepExecutionResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'batchuiApp.batchStepExecution.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: BatchStepExecutionUpdateComponent,
    resolve: {
      batchStepExecution: BatchStepExecutionResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'batchuiApp.batchStepExecution.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: BatchStepExecutionUpdateComponent,
    resolve: {
      batchStepExecution: BatchStepExecutionResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'batchuiApp.batchStepExecution.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
