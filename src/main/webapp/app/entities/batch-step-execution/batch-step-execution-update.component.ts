import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IBatchStepExecution, BatchStepExecution } from 'app/shared/model/batch-step-execution.model';
import { BatchStepExecutionService } from './batch-step-execution.service';
import { IBatchJobExecution } from 'app/shared/model/batch-job-execution.model';
import { BatchJobExecutionService } from 'app/entities/batch-job-execution/batch-job-execution.service';

@Component({
  selector: 'jhi-batch-step-execution-update',
  templateUrl: './batch-step-execution-update.component.html'
})
export class BatchStepExecutionUpdateComponent implements OnInit {
  isSaving = false;
  batchjobexecutions: IBatchJobExecution[] = [];

  editForm = this.fb.group({
    id: [],
    stepExecutionId: [null, [Validators.required]],
    version: [null, [Validators.required]],
    stepName: [null, [Validators.required, Validators.maxLength(100)]],
    startTime: [null, [Validators.required]],
    endTime: [null, [Validators.required]],
    status: [null, [Validators.maxLength(10)]],
    commitCount: [],
    readCount: [],
    filterCount: [],
    writeCount: [],
    readSkipCount: [],
    writeSkipCount: [],
    processSkipCount: [],
    rollbackCount: [],
    exitCode: [null, [Validators.maxLength(2500)]],
    exitMessage: [null, [Validators.maxLength(2500)]],
    lastUpdated: [],
    batchJobExecutionId: []
  });

  constructor(
    protected batchStepExecutionService: BatchStepExecutionService,
    protected batchJobExecutionService: BatchJobExecutionService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ batchStepExecution }) => {
      if (!batchStepExecution.id) {
        const today = moment().startOf('day');
        batchStepExecution.startTime = today;
        batchStepExecution.endTime = today;
        batchStepExecution.lastUpdated = today;
      }

      this.updateForm(batchStepExecution);

      this.batchJobExecutionService
        .query()
        .subscribe((res: HttpResponse<IBatchJobExecution[]>) => (this.batchjobexecutions = res.body || []));
    });
  }

  updateForm(batchStepExecution: IBatchStepExecution): void {
    this.editForm.patchValue({
      id: batchStepExecution.id,
      version: batchStepExecution.version,
      stepName: batchStepExecution.stepName,
      startTime: batchStepExecution.startTime ? batchStepExecution.startTime.format(DATE_TIME_FORMAT) : null,
      endTime: batchStepExecution.endTime ? batchStepExecution.endTime.format(DATE_TIME_FORMAT) : null,
      status: batchStepExecution.status,
      commitCount: batchStepExecution.commitCount,
      readCount: batchStepExecution.readCount,
      filterCount: batchStepExecution.filterCount,
      writeCount: batchStepExecution.writeCount,
      readSkipCount: batchStepExecution.readSkipCount,
      writeSkipCount: batchStepExecution.writeSkipCount,
      processSkipCount: batchStepExecution.processSkipCount,
      rollbackCount: batchStepExecution.rollbackCount,
      exitCode: batchStepExecution.exitCode,
      exitMessage: batchStepExecution.exitMessage,
      lastUpdated: batchStepExecution.lastUpdated ? batchStepExecution.lastUpdated.format(DATE_TIME_FORMAT) : null,
      batchJobExecutionId: batchStepExecution.batchJobExecutionId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const batchStepExecution = this.createFromForm();
    if (batchStepExecution.id !== undefined) {
      this.subscribeToSaveResponse(this.batchStepExecutionService.update(batchStepExecution));
    } else {
      this.subscribeToSaveResponse(this.batchStepExecutionService.create(batchStepExecution));
    }
  }

  private createFromForm(): IBatchStepExecution {
    return {
      ...new BatchStepExecution(),
      id: this.editForm.get(['id'])!.value,
      version: this.editForm.get(['version'])!.value,
      stepName: this.editForm.get(['stepName'])!.value,
      startTime: this.editForm.get(['startTime'])!.value ? moment(this.editForm.get(['startTime'])!.value, DATE_TIME_FORMAT) : undefined,
      endTime: this.editForm.get(['endTime'])!.value ? moment(this.editForm.get(['endTime'])!.value, DATE_TIME_FORMAT) : undefined,
      status: this.editForm.get(['status'])!.value,
      commitCount: this.editForm.get(['commitCount'])!.value,
      readCount: this.editForm.get(['readCount'])!.value,
      filterCount: this.editForm.get(['filterCount'])!.value,
      writeCount: this.editForm.get(['writeCount'])!.value,
      readSkipCount: this.editForm.get(['readSkipCount'])!.value,
      writeSkipCount: this.editForm.get(['writeSkipCount'])!.value,
      processSkipCount: this.editForm.get(['processSkipCount'])!.value,
      rollbackCount: this.editForm.get(['rollbackCount'])!.value,
      exitCode: this.editForm.get(['exitCode'])!.value,
      exitMessage: this.editForm.get(['exitMessage'])!.value,
      lastUpdated: this.editForm.get(['lastUpdated'])!.value
        ? moment(this.editForm.get(['lastUpdated'])!.value, DATE_TIME_FORMAT)
        : undefined,
      batchJobExecutionId: this.editForm.get(['batchJobExecutionId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBatchStepExecution>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IBatchJobExecution): any {
    return item.id;
  }
}
