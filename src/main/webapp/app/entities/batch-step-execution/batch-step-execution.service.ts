import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IBatchStepExecution } from 'app/shared/model/batch-step-execution.model';

type EntityResponseType = HttpResponse<IBatchStepExecution>;
type EntityArrayResponseType = HttpResponse<IBatchStepExecution[]>;

@Injectable({ providedIn: 'root' })
export class BatchStepExecutionService {
  public resourceUrl = SERVER_API_URL + 'api/batch-step-executions';

  constructor(protected http: HttpClient) {}

  create(batchStepExecution: IBatchStepExecution): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(batchStepExecution);
    return this.http
      .post<IBatchStepExecution>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(batchStepExecution: IBatchStepExecution): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(batchStepExecution);
    return this.http
      .put<IBatchStepExecution>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IBatchStepExecution>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBatchStepExecution[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  protected convertDateFromClient(batchStepExecution: IBatchStepExecution): IBatchStepExecution {
    const copy: IBatchStepExecution = Object.assign({}, batchStepExecution, {
      startTime: batchStepExecution.startTime && batchStepExecution.startTime.isValid() ? batchStepExecution.startTime.toJSON() : undefined,
      endTime: batchStepExecution.endTime && batchStepExecution.endTime.isValid() ? batchStepExecution.endTime.toJSON() : undefined,
      lastUpdated:
        batchStepExecution.lastUpdated && batchStepExecution.lastUpdated.isValid() ? batchStepExecution.lastUpdated.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.startTime = res.body.startTime ? moment(res.body.startTime) : undefined;
      res.body.endTime = res.body.endTime ? moment(res.body.endTime) : undefined;
      res.body.lastUpdated = res.body.lastUpdated ? moment(res.body.lastUpdated) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((batchStepExecution: IBatchStepExecution) => {
        batchStepExecution.startTime = batchStepExecution.startTime ? moment(batchStepExecution.startTime) : undefined;
        batchStepExecution.endTime = batchStepExecution.endTime ? moment(batchStepExecution.endTime) : undefined;
        batchStepExecution.lastUpdated = batchStepExecution.lastUpdated ? moment(batchStepExecution.lastUpdated) : undefined;
      });
    }
    return res;
  }
}
