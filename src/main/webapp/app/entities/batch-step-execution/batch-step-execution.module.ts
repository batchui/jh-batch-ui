import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BatchuiSharedModule } from 'app/shared/shared.module';
import { BatchStepExecutionComponent } from './batch-step-execution.component';
import { BatchStepExecutionDetailComponent } from './batch-step-execution-detail.component';
import { BatchStepExecutionUpdateComponent } from './batch-step-execution-update.component';
import { BatchStepExecutionDeleteDialogComponent } from './batch-step-execution-delete-dialog.component';
import { batchStepExecutionRoute } from './batch-step-execution.route';

@NgModule({
  imports: [BatchuiSharedModule, RouterModule.forChild(batchStepExecutionRoute)],
  declarations: [
    BatchStepExecutionComponent,
    BatchStepExecutionDetailComponent,
    BatchStepExecutionUpdateComponent,
    BatchStepExecutionDeleteDialogComponent
  ],
  entryComponents: [BatchStepExecutionDeleteDialogComponent]
})
export class BatchuiBatchStepExecutionModule {}
