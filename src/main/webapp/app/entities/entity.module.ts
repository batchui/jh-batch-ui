import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'batch-job-instance',
        loadChildren: () => import('./batch-job-instance/batch-job-instance.module').then(m => m.BatchuiBatchJobInstanceModule)
      },
      {
        path: 'batch-job-execution',
        loadChildren: () => import('./batch-job-execution/batch-job-execution.module').then(m => m.BatchuiBatchJobExecutionModule)
      },
      {
        path: 'batch-step-execution',
        loadChildren: () => import('./batch-step-execution/batch-step-execution.module').then(m => m.BatchuiBatchStepExecutionModule)
      },
      {
        path: 'start-job-command',
        loadChildren: () => import('./start-job-command/start-job-command.module').then(m => m.BatchuiStartJobCommandModule)
      }
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ])
  ]
})
export class BatchuiEntityModule {}
