import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBatchJobExecution, BatchJobExecution } from 'app/shared/model/batch-job-execution.model';
import { BatchJobExecutionService } from './batch-job-execution.service';
import { BatchJobExecutionComponent } from './batch-job-execution.component';
import { BatchJobExecutionDetailComponent } from './batch-job-execution-detail.component';
import { BatchJobExecutionUpdateComponent } from './batch-job-execution-update.component';
import { StartBatchJobComponent } from './start-batch-job.component';

@Injectable({ providedIn: 'root' })
export class BatchJobExecutionResolve implements Resolve<IBatchJobExecution> {
  constructor(private service: BatchJobExecutionService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBatchJobExecution> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((batchJobExecution: HttpResponse<BatchJobExecution>) => {
          if (batchJobExecution.body) {
            return of(batchJobExecution.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BatchJobExecution());
  }
}

export const batchJobExecutionRoute: Routes = [
  {
    path: '',
    component: BatchJobExecutionComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,desc',
      pageTitle: 'batchuiApp.batchJobExecution.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: BatchJobExecutionDetailComponent,
    resolve: {
      batchJobExecution: BatchJobExecutionResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'batchuiApp.batchJobExecution.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: StartBatchJobComponent,
    resolve: {
      batchJobExecution: BatchJobExecutionResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'batchuiApp.batchJobExecution.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: BatchJobExecutionUpdateComponent,
    resolve: {
      batchJobExecution: BatchJobExecutionResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'batchuiApp.batchJobExecution.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
