import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { BatchJobExecutionService } from './batch-job-execution.service';
import { IBatchJobExecution } from 'app/shared/model/batch-job-execution.model';
import { BatchJobExecutionDeleteDialogComponent } from './batch-job-execution-delete-dialog.component';

@Component({
  selector: 'jhi-batch-job-execution-detail',
  templateUrl: './batch-job-execution-detail.component.html'
})
export class BatchJobExecutionDetailComponent implements OnInit, OnDestroy {
  batchJobExecution: IBatchJobExecution | null = null;
  eventSubscriber?: Subscription;

  constructor(
    protected activatedRoute: ActivatedRoute,
    protected service: BatchJobExecutionService,
    protected modalService: NgbModal,
    protected router: Router,
    protected eventManager: JhiEventManager
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ batchJobExecution }) => (this.batchJobExecution = batchJobExecution));
    this.registerChangeInBatchJobExecutions();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  refresh(): void {
    this.service.find(this.batchJobExecution!.id!).subscribe((res: HttpResponse<IBatchJobExecution>) => {
      this.batchJobExecution = res.body;
    });
  }

  previousState(): void {
    window.history.back();
  }

  delete(): void {
    const modalRef = this.modalService.open(BatchJobExecutionDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.batchJobExecution = this.batchJobExecution;
  }

  registerChangeInBatchJobExecutions(): void {
    this.eventSubscriber = this.eventManager.subscribe('batchJobExecutionListModification', () => this.previousState());
  }
}
