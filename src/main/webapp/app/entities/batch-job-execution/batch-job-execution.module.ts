import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BatchuiSharedModule } from 'app/shared/shared.module';
import { BatchJobExecutionComponent } from './batch-job-execution.component';
import { BatchJobExecutionDetailComponent } from './batch-job-execution-detail.component';
import { BatchJobExecutionUpdateComponent } from './batch-job-execution-update.component';
import { BatchJobExecutionDeleteDialogComponent } from './batch-job-execution-delete-dialog.component';
import { BatchJobExecutionListComponent } from './widgets/batch-job-execution-list.component';
import { BatchJobExecutionViewComponent } from './widgets/batch-job-execution-view.component';
import { BatchJobExecutionParamsWidgetComponent } from './widgets/batch-job-executionparams.component';
import { BatchJobStepExecutionListComponent } from './widgets/batch-job-step-execution-list.component';
import { BatchJobExecutionContextComponent } from './widgets/batch-job-execution-context.component';
import { BatchJobStepExecutionContextComponent } from './widgets/batch-step-execution-context.component';
import { JobExecutionSearchFormComponent } from './widgets/search-form.component';
import { StartBatchJobComponent } from './start-batch-job.component';
import { batchJobExecutionRoute } from './batch-job-execution.route';

@NgModule({
  imports: [BatchuiSharedModule, RouterModule.forChild(batchJobExecutionRoute)],
  declarations: [
    BatchJobExecutionComponent,
    BatchJobExecutionDetailComponent,
    BatchJobExecutionUpdateComponent,
    BatchJobExecutionDeleteDialogComponent,
    BatchJobExecutionListComponent,
    BatchJobExecutionParamsWidgetComponent,
    BatchJobExecutionViewComponent,
    BatchJobExecutionContextComponent,
    BatchJobStepExecutionListComponent,
    BatchJobStepExecutionContextComponent,
    JobExecutionSearchFormComponent,
    StartBatchJobComponent
  ],
  exports: [BatchJobExecutionListComponent, BatchJobExecutionViewComponent],
  entryComponents: [BatchJobExecutionDeleteDialogComponent]
})
export class BatchuiBatchJobExecutionModule {}
