import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { Moment } from 'moment';
import { SERVER_API_URL } from 'app/app.constants';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IBatchJobExecution } from 'app/shared/model/batch-job-execution.model';
import { IStartBatchJob } from 'app/shared/model/start-batch-job.model';
import { IJobStatusReport } from 'app/shared/model/job-status-report.dto';

type EntityResponseType = HttpResponse<IBatchJobExecution>;
type EntityArrayResponseType = HttpResponse<IBatchJobExecution[]>;
type StatusReportResponseType = HttpResponse<IJobStatusReport>;

@Injectable({ providedIn: 'root' })
export class BatchJobExecutionService {
  public resourceUrl = SERVER_API_URL + 'api/batch-job-executions';

  constructor(protected http: HttpClient) {}

  startNewJob(startBatchJob: IStartBatchJob): Observable<EntityResponseType> {
    return this.http
      .post<IBatchJobExecution>('/api/start-batch-job', startBatchJob, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  /*
  create(batchJobExecution: IBatchJobExecution): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(batchJobExecution);
    return this.http
      .post<IBatchJobExecution>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }*/

  update(batchJobExecution: IBatchJobExecution): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(batchJobExecution);
    return this.http
      .put<IBatchJobExecution>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IBatchJobExecution>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IBatchJobExecution[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  jobStatusReport(fromDate?: Moment, toDate?: Moment): Observable<StatusReportResponseType> {
    let options: HttpParams = new HttpParams();
    if (fromDate) {
      options = options.set('from', fromDate.format(DATE_FORMAT));
    }
    if (toDate) {
      options = options.set('to', toDate.format(DATE_FORMAT));
    }
    return this.http
      .get<IJobStatusReport>(this.resourceUrl + '/status-report', { params: options, observe: 'response' })
      .pipe(map((res: StatusReportResponseType) => this.convertDateFromServerReport(res)));
  }

  protected convertDateFromClient(batchJobExecution: IBatchJobExecution): IBatchJobExecution {
    const copy: IBatchJobExecution = Object.assign({}, batchJobExecution, {
      createTime:
        batchJobExecution.createTime && batchJobExecution.createTime.isValid() ? batchJobExecution.createTime.toJSON() : undefined,
      startTime: batchJobExecution.startTime && batchJobExecution.startTime.isValid() ? batchJobExecution.startTime.toJSON() : undefined,
      endTime: batchJobExecution.endTime && batchJobExecution.endTime.isValid() ? batchJobExecution.endTime.toJSON() : undefined,
      lastUpdated:
        batchJobExecution.lastUpdated && batchJobExecution.lastUpdated.isValid() ? batchJobExecution.lastUpdated.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.createTime = res.body.createTime ? moment(res.body.createTime) : undefined;
      res.body.startTime = res.body.startTime ? moment(res.body.startTime) : undefined;
      res.body.endTime = res.body.endTime ? moment(res.body.endTime) : undefined;
      res.body.lastUpdated = res.body.lastUpdated ? moment(res.body.lastUpdated) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((batchJobExecution: IBatchJobExecution) => {
        batchJobExecution.createTime = batchJobExecution.createTime ? moment(batchJobExecution.createTime) : undefined;
        batchJobExecution.startTime = batchJobExecution.startTime ? moment(batchJobExecution.startTime) : undefined;
        batchJobExecution.endTime = batchJobExecution.endTime ? moment(batchJobExecution.endTime) : undefined;
        batchJobExecution.lastUpdated = batchJobExecution.lastUpdated ? moment(batchJobExecution.lastUpdated) : undefined;
      });
    }
    return res;
  }

  protected convertDateFromServerReport(res: StatusReportResponseType): StatusReportResponseType {
    if (res.body) {
      /*
      res.body.fromDate = res.body.fromDate ? moment(res.body.fromDate) : undefined;
      res.body.toDate = res.body.toDate ? moment(res.body.toDate) : undefined;
      */
    }
    return res;
  }
}
