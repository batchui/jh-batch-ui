import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router, ParamMap } from '@angular/router';
import { Subscription } from 'rxjs';
import { combineLatest } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import * as ReqUtils from 'app/shared/util/request-util';
import * as moment from 'moment';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { IBatchJobExecution } from 'app/shared/model/batch-job-execution.model';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { DATE_FORMAT } from 'app/shared/constants/input.constants';
import { BatchJobExecutionService } from './batch-job-execution.service';
import { JobExecutionSearchCriteria } from './widgets/job-execution-search.criteria';

@Component({
  selector: 'jhi-batch-job-execution',
  templateUrl: './batch-job-execution.component.html'
})
export class BatchJobExecutionComponent implements OnInit, OnDestroy {
  batchJobExecutions?: IBatchJobExecution[];
  eventSubscriber?: Subscription;
  queryParams: any;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate = 'id';
  ascending!: boolean;
  ngbPaginationPage = 1;
  initialized = false;

  searchCriteria: JobExecutionSearchCriteria;
  sidebarActive = true;

  constructor(
    protected batchJobExecutionService: BatchJobExecutionService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected loadingBarService: LoadingBarService
  ) {
    this.searchCriteria = new JobExecutionSearchCriteria();
  }

  loadPage(page?: number): void {
    this.loadingBarService.start();
    const pageToLoad: number = page || this.page;
    const req = this.constructRequest(pageToLoad);

    this.batchJobExecutionService.query(req).subscribe(
      (res: HttpResponse<IBatchJobExecution[]>) => this.onSuccess(res.body, res.headers, pageToLoad),
      () => this.onError()
    );
  }

  ngOnInit(): void {
    // Combine them both into a single observable
    const obsComb = combineLatest(this.activatedRoute.data, this.activatedRoute.queryParamMap, (data, qparams) => ({ data, qparams }));
    obsComb.subscribe(ap => {
      if (!this.initialized) {
        this.page = ap.data.pagingParams.page;
        this.ascending = ap.data.pagingParams.ascending;
        this.predicate = ap.data.pagingParams.predicate;
        this.ngbPaginationPage = ap.data.pagingParams.page;
        this.mapFromRequestParams(ap.qparams);
        this.initialized = true;
        this.loadPage();
      }
    });
    this.registerChangeInBatchJobExecutions();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IBatchJobExecution): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInBatchJobExecutions(): void {
    this.eventSubscriber = this.eventManager.subscribe('batchJobExecutionListModification', () => this.loadPage());
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  search(): void {
    this.page = 1;
    this.ngbPaginationPage = 1;
    this.predicate = 'startTime';
    this.ascending = false;
    this.loadPage();
  }

  toggleSearchSidebar(): void {
    this.sidebarActive = !this.sidebarActive;
  }

  refresh(): void {
    this.loadPage(this.page);
  }

  protected onSuccess(data: IBatchJobExecution[] | null, headers: HttpHeaders, page: number): void {
    this.loadingBarService.complete();
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.batchJobExecutions = data || [];
    this.navigate();
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }

  // --- custom private methods -----------------------------------------------

  private navigate(): void {
    const queryParams = {
      page: this.page,
      size: this.itemsPerPage,
      sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
    };
    this.mapToRequestParams(queryParams);
    this.router.navigate(['/batch-job-execution'], {
      queryParams
    });
  }

  private mapToRequestParams(req: any): void {
    if (this.searchCriteria.jobName != null) {
      req['jobName'] = this.searchCriteria.jobName;
    }
    if (this.searchCriteria.fromDate != null) {
      req['fd'] = this.searchCriteria.fromDate.format(DATE_FORMAT);
    }
    if (this.searchCriteria.toDate != null) {
      req['td'] = this.searchCriteria.toDate.format(DATE_FORMAT);
    }
    if (this.searchCriteria.exitCode != null) {
      req['exitCode'] = this.searchCriteria.exitCode;
    }
    if (this.searchCriteria.status != null) {
      req['status'] = this.searchCriteria.status;
    }
    return req;
  }

  private constructRequest(pageToLoad: number): any {
    const req = {
      page: pageToLoad - 1,
      size: this.itemsPerPage,
      sort: this.sort()
    };
    return this.searchCriteria.toJhipsterSearchParams(req);
  }

  private mapFromRequestParams(params: ParamMap): void {
    const criteria = new JobExecutionSearchCriteria();

    if (params.get('fd') != null) {
      criteria.fromDate = moment(ReqUtils.getParam(params, 'fd'), DATE_FORMAT);
    }
    if (params.get('td') != null) {
      criteria.toDate = moment(ReqUtils.getParam(params, 'td'), DATE_FORMAT);
    }

    criteria.jobName = ReqUtils.getParam(params, 'jobName');
    criteria.exitCode = ReqUtils.getParam(params, 'exitCode');
    criteria.status = ReqUtils.getParam(params, 'status');

    this.searchCriteria = criteria;
  }
}
