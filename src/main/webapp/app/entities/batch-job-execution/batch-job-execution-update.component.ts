import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IBatchJobExecution, BatchJobExecution } from 'app/shared/model/batch-job-execution.model';
import { BatchJobExecutionService } from './batch-job-execution.service';
import { IBatchJobInstance } from 'app/shared/model/batch-job-instance.model';
import { BatchJobInstanceService } from 'app/entities/batch-job-instance/batch-job-instance.service';

@Component({
  selector: 'jhi-batch-job-execution-update',
  templateUrl: './batch-job-execution-update.component.html'
})
export class BatchJobExecutionUpdateComponent implements OnInit {
  isSaving = false;
  batchjobinstances: IBatchJobInstance[] = [];

  editForm = this.fb.group({
    id: [],
    jobExecutionId: [null, [Validators.required]],
    version: [],
    createTime: [null, [Validators.required]],
    startTime: [null, [Validators.required]],
    endTime: [null, [Validators.required]],
    status: [null, [Validators.maxLength(10)]],
    exitCode: [null, [Validators.maxLength(2500)]],
    exitMessage: [null, [Validators.maxLength(2500)]],
    lastUpdated: [],
    jobConfigurationLocation: [null, [Validators.maxLength(2500)]],
    batchJobInstanceId: []
  });

  constructor(
    protected batchJobExecutionService: BatchJobExecutionService,
    protected batchJobInstanceService: BatchJobInstanceService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ batchJobExecution }) => {
      if (!batchJobExecution.id) {
        const today = moment().startOf('day');
        batchJobExecution.createTime = today;
        batchJobExecution.startTime = today;
        batchJobExecution.endTime = today;
        batchJobExecution.lastUpdated = today;
      }

      this.updateForm(batchJobExecution);

      this.batchJobInstanceService.query().subscribe((res: HttpResponse<IBatchJobInstance[]>) => (this.batchjobinstances = res.body || []));
    });
  }

  updateForm(batchJobExecution: IBatchJobExecution): void {
    this.editForm.patchValue({
      id: batchJobExecution.id,
      version: batchJobExecution.version,
      createTime: batchJobExecution.createTime ? batchJobExecution.createTime.format(DATE_TIME_FORMAT) : null,
      startTime: batchJobExecution.startTime ? batchJobExecution.startTime.format(DATE_TIME_FORMAT) : null,
      endTime: batchJobExecution.endTime ? batchJobExecution.endTime.format(DATE_TIME_FORMAT) : null,
      status: batchJobExecution.status,
      exitCode: batchJobExecution.exitCode,
      exitMessage: batchJobExecution.exitMessage,
      lastUpdated: batchJobExecution.lastUpdated ? batchJobExecution.lastUpdated.format(DATE_TIME_FORMAT) : null,
      jobConfigurationLocation: batchJobExecution.jobConfigurationLocation,
      batchJobInstanceId: batchJobExecution.batchJobInstanceId
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const batchJobExecution = this.createFromForm();
    if (batchJobExecution.id !== undefined) {
      // Maybe we will need this in case of Job rerun ?!
      this.subscribeToSaveResponse(this.batchJobExecutionService.update(batchJobExecution));
    } else {
      // this.subscribeToSaveResponse(this.batchJobExecutionService.create(batchJobExecution));
    }
  }

  private createFromForm(): IBatchJobExecution {
    return {
      ...new BatchJobExecution(),
      id: this.editForm.get(['id'])!.value,
      version: this.editForm.get(['version'])!.value,
      createTime: this.editForm.get(['createTime'])!.value ? moment(this.editForm.get(['createTime'])!.value, DATE_TIME_FORMAT) : undefined,
      startTime: this.editForm.get(['startTime'])!.value ? moment(this.editForm.get(['startTime'])!.value, DATE_TIME_FORMAT) : undefined,
      endTime: this.editForm.get(['endTime'])!.value ? moment(this.editForm.get(['endTime'])!.value, DATE_TIME_FORMAT) : undefined,
      status: this.editForm.get(['status'])!.value,
      exitCode: this.editForm.get(['exitCode'])!.value,
      exitMessage: this.editForm.get(['exitMessage'])!.value,
      lastUpdated: this.editForm.get(['lastUpdated'])!.value
        ? moment(this.editForm.get(['lastUpdated'])!.value, DATE_TIME_FORMAT)
        : undefined,
      jobConfigurationLocation: this.editForm.get(['jobConfigurationLocation'])!.value,
      batchJobInstanceId: this.editForm.get(['batchJobInstanceId'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBatchJobExecution>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }

  trackById(index: number, item: IBatchJobInstance): any {
    return item.id;
  }
}
