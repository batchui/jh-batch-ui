import { FormGroup } from '@angular/forms';

export function oneValueRequierdValidator(formGroup: FormGroup): { [key: string]: boolean } | null {
  if (formGroup.value.longVal || formGroup.value.stringVal || formGroup.value.doubleVal || formGroup.value.dateVal) {
    return null;
  }
  return { oneValueFieldRequiered: true };
}

export function onlyOneValueAllowedValidator(formGroup: FormGroup): { [key: string]: boolean } | null {
  let count = 0;
  if (formGroup.value.longVal) {
    count++;
  }
  if (formGroup.value.stringVal) {
    count++;
  }
  if (formGroup.value.doubleVal) {
    count++;
  }
  if (formGroup.value.dateVal) {
    count++;
  }
  if (count === 0 || count === 1) {
    return null;
  }
  return { onlyOneValueFieldAllowed: true };
}
