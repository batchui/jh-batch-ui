import { Component, OnInit, Input } from '@angular/core';
import { IBatchJobExecutionContext } from 'app/shared/model/batch-job-execution-context.model';

@Component({
  selector: 'jhi-batch-job-execution-context',
  templateUrl: './batch-job-execution-context.component.html'
})
export class BatchJobExecutionContextComponent implements OnInit {
  @Input()
  batchJobExecutionContexts: IBatchJobExecutionContext[] = [];

  ngOnInit(): void {
    // Implement me ...
  }
}
