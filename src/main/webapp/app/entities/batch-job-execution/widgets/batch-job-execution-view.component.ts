import { Component, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IBatchJobExecution } from 'app/shared/model/batch-job-execution.model';

@Component({
  selector: 'jhi-batch-job-execution-view',
  templateUrl: './batch-job-execution-view.component.html'
})
export class BatchJobExecutionViewComponent {
  @Input()
  batchJobExecution!: IBatchJobExecution;

  constructor(protected activatedRoute: ActivatedRoute) {}
}
