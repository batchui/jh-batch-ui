import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { IBatchJobExecution } from 'app/shared/model/batch-job-execution.model';
import { BatchJobExecutionDeleteDialogComponent } from '../batch-job-execution-delete-dialog.component';

@Component({
  selector: 'jhi-batch-job-execution-list',
  templateUrl: './batch-job-execution-list.component.html'
})
export class BatchJobExecutionListComponent implements OnInit {
  @Input()
  batchJobExecutions: IBatchJobExecution[] = [];

  @Input()
  predicate = 'endTime';

  @Input()
  ascending = false;

  @Input()
  sortCallback!: Function;

  @Output()
  predicateChange: EventEmitter<string> = new EventEmitter();

  @Output()
  ascendingChange: EventEmitter<boolean> = new EventEmitter();

  constructor(protected modalService: NgbModal) {
    //
  }

  ngOnInit(): void {
    this.predicate = 'endTime';
    this.ascending = false;
    // Implement me ...
  }

  sortEnabled(): boolean {
    if (this.sortCallback) {
      return true;
    }
    return false;
  }

  trackId(index: number, item: IBatchJobExecution): number {
    return item.id!;
  }

  onSort(): void {
    this.predicateChange.emit(this.predicate);
    this.ascendingChange.emit(this.ascending);
    this.sortCallback();
  }

  delete(batchJobExecution: IBatchJobExecution): void {
    const modalRef = this.modalService.open(BatchJobExecutionDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.batchJobExecution = batchJobExecution;
  }
}
