import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { JhiAlertService } from 'ng-jhipster';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { BatchJobInstanceService } from '../../batch-job-instance/batch-job-instance.service';
import { JobExecutionSearchCriteria } from './job-execution-search.criteria';
import { ExitCode, StatusCode } from 'app/shared/constants/entities.constants';

@Component({
  selector: 'jhi-job-execution-search-form',
  templateUrl: './search-form.component.html'
})
export class JobExecutionSearchFormComponent implements OnInit {
  fromDateDp: any;
  toDateDp: any;
  listJobNames: string[] = [];
  exitCodes: string[] = Object.keys(ExitCode).filter(type => isNaN(type as any));
  statuses: string[] = Object.keys(StatusCode).filter(type => isNaN(type as any));
  criteria: JobExecutionSearchCriteria;

  @Output()
  onSerarch = new EventEmitter();

  constructor(private batchJobInstanceService: BatchJobInstanceService, private jhiAlertService: JhiAlertService) {
    this.criteria = new JobExecutionSearchCriteria();
  }

  ngOnInit(): void {
    // get list of Job names
    this.batchJobInstanceService.getJobNames().subscribe(
      (res: HttpResponse<string[]>) => (this.listJobNames = res.body ? res.body : []),
      (res: HttpErrorResponse) => this.onError(res.message)
    );
  }

  // --- public methods -----------------------------------------------------

  search(): void {
    this.onSerarch.emit();
  }

  clearSearch(): void {
    this.criteria.reset();
  }

  // --- set/get methods ------------------------------------------------------

  @Input()
  public set searchCriteria(scriteria: JobExecutionSearchCriteria) {
    this.criteria = scriteria;
  }

  public get searchCriteria(): JobExecutionSearchCriteria {
    return this.criteria;
  }

  // --- private methods ----------------------------------------------------

  private onError(errorMessage: string): void {
    this.jhiAlertService.error(errorMessage, null);
  }
}
