import { Component, OnInit, Input } from '@angular/core';
import { IBatchStepExecutionContext } from 'app/shared/model/batch-step-execution-context.model';

@Component({
  selector: 'jhi-batch-job-step-execution-context',
  templateUrl: './batch-step-execution-context.component.html'
})
export class BatchJobStepExecutionContextComponent implements OnInit {
  @Input()
  batchStepExecutionContexts: IBatchStepExecutionContext[] = [];

  ngOnInit(): void {
    // Implement me ...
  }
}
