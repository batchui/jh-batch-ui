import { Moment } from 'moment';

export class JobExecutionSearchCriteria {
  constructor(public fromDate?: Moment, public toDate?: Moment, public jobName?: string, public exitCode?: string, public status?: string) {
    // ...
  }

  public reset(): void {
    this.fromDate = undefined;
    this.toDate = undefined;
    this.jobName = undefined;
    this.exitCode = undefined;
    this.status = undefined;
  }

  public toJhipsterSearchParams(req: any): any {
    if (this.fromDate != null) {
      const fromDate = this.fromDate.startOf('day');
      req['startTime.greaterOrEqualThan'] = fromDate.toISOString();
    }
    if (this.toDate != null) {
      const toDate = this.toDate.endOf('day');
      req['startTime.lessOrEqualThan'] = toDate.toISOString();
    }
    if (this.jobName != null) {
      req['jobName.equals'] = this.jobName;
    }
    if (this.exitCode != null) {
      req['exitCode.equals'] = this.exitCode;
    }
    if (this.status != null) {
      req['status.equals'] = this.status;
    }
    return req;
  }
}
