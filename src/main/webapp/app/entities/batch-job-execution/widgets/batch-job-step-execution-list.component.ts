import { Component, Input } from '@angular/core';
import { IBatchStepExecution } from 'app/shared/model/batch-step-execution.model';

@Component({
  selector: 'jhi-batch-job-step-execution-list',
  templateUrl: './batch-job-step-execution-list.component.html'
})
export class BatchJobStepExecutionListComponent {
  @Input()
  batchStepExecutions: IBatchStepExecution[] = [];
}
