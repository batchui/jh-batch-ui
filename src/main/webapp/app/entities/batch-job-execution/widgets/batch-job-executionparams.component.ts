import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IBatchJobExecutionParams } from 'app/shared/model/batch-job-execution-params.model';

@Component({
  selector: 'jhi-batch-job-executionparams-widget',
  templateUrl: './batch-job-executionparams.component.html'
})
export class BatchJobExecutionParamsWidgetComponent {
  @Input()
  batchJobExecutionParams: IBatchJobExecutionParams[] = [];

  @Output()
  onRemove = new EventEmitter();

  @Input()
  showRemoveControl = false;

  remove(ndx: number): void {
    this.onRemove.emit(ndx);
  }
}
