import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { StartBatchJob } from 'app/shared/model/start-batch-job.model';
import { BatchJobExecutionParams } from 'app/shared/model/batch-job-execution-params.model';
import { BatchJobExecutionService } from './batch-job-execution.service';
import { StartJobCommandService } from '../start-job-command/start-job-command.service';
import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { oneValueRequierdValidator, onlyOneValueAllowedValidator } from './job-params.validator';

@Component({
  selector: 'jhi-start-batch-job',
  templateUrl: './start-batch-job.component.html'
})
export class StartBatchJobComponent implements OnInit {
  isSaving = false;
  identifyingOptions = [
    { key: 'Yes', value: 'Y' },
    { key: 'No', value: 'N' }
  ];

  jobParamForm = this.fb.group(
    {
      keyName: [null, [Validators.required, Validators.maxLength(100)]],
      stringVal: [null, [Validators.maxLength(250)]],
      dateVal: [],
      longVal: [],
      doubleVal: [],
      identifying: [null, [Validators.required]]
    },
    {
      validators: [oneValueRequierdValidator, onlyOneValueAllowedValidator]
    }
  );

  startJobForm = this.fb.group({
    jobName: [null, [Validators.required]]
  });

  startBatchJob = new StartBatchJob();

  constructor(
    protected startJobCommandService: StartJobCommandService,
    protected batchJobExecutionService: BatchJobExecutionService,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.startBatchJob = new StartBatchJob();
    this.startBatchJob.batchJobExecutionParams = [];
  }

  previousState(): void {
    window.history.back();
  }

  resetParam(): void {
    this.jobParamForm.get(['keyName'])!.patchValue(undefined);
    this.jobParamForm.get(['stringVal'])!.patchValue(undefined);
    this.jobParamForm.get(['dateVal'])!.patchValue(undefined);
    this.jobParamForm.get(['longVal'])!.patchValue(undefined);
    this.jobParamForm.get(['doubleVal'])!.patchValue(undefined);
    this.jobParamForm.get(['identifying'])!.patchValue(undefined);
    this.jobParamForm.reset();
  }

  addParam(): void {
    const param = {
      ...new BatchJobExecutionParams(),
      keyName: this.jobParamForm.get(['keyName'])!.value,
      stringVal: this.jobParamForm.get(['stringVal'])!.value,
      dateVal: this.jobParamForm.get(['dateVal'])!.value ? moment(this.jobParamForm.get(['dateVal'])!.value, DATE_TIME_FORMAT) : undefined,
      longVal: this.jobParamForm.get(['longVal'])!.value,
      doubleVal: this.jobParamForm.get(['doubleVal'])!.value,
      identifying: this.jobParamForm.get(['identifying'])!.value
    };

    // determine typeCd from values ...
    if (param.stringVal) {
      param.typeCd = 'STRING';
    } else if (param.longVal) {
      param.typeCd = 'LONG';
    } else if (param.doubleVal) {
      param.typeCd = 'DOUBLE';
    } else {
      param.typeCd = 'DATE';
    }

    this.startBatchJob.batchJobExecutionParams!.push(param);
    this.resetParam();
  }

  save(): void {
    this.isSaving = true;
    this.startBatchJob.jobName = this.startJobForm.get(['jobName'])!.value;
    // eslint-disable-next-line no-console
    console.warn('Saving startBatchJob: %o', this.startBatchJob);
    this.subscribeToSaveResponse(this.startJobCommandService.startNewJob(this.startBatchJob));
    // this.subscribeToSaveResponse(this.batchJobExecutionService.startNewJob(this.startBatchJob));
  }

  removeBjep(ndx: number): void {
    this.startBatchJob.batchJobExecutionParams!.splice(ndx, 1);
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<any>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
