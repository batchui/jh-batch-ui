import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IBatchJobExecution } from 'app/shared/model/batch-job-execution.model';
import { BatchJobExecutionService } from './batch-job-execution.service';

@Component({
  templateUrl: './batch-job-execution-delete-dialog.component.html'
})
export class BatchJobExecutionDeleteDialogComponent {
  batchJobExecution?: IBatchJobExecution;

  constructor(
    protected batchJobExecutionService: BatchJobExecutionService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.batchJobExecutionService.delete(id).subscribe(() => {
      this.eventManager.broadcast('batchJobExecutionListModification');
      this.activeModal.close();
    });
  }
}
