import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';
import { JhiDataUtils, JhiFileLoadError, JhiEventManager, JhiEventWithContent } from 'ng-jhipster';

import { IStartJobCommand, StartJobCommand } from 'app/shared/model/start-job-command.model';
import { StartJobCommandService } from './start-job-command.service';
import { AlertError } from 'app/shared/alert/alert-error.model';

@Component({
  selector: 'jhi-start-job-command-update',
  templateUrl: './start-job-command-update.component.html'
})
export class StartJobCommandUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    jobName: [null, [Validators.required, Validators.maxLength(80)]],
    jobParams: [],
    status: [],
    nextAttemptTime: [],
    attemptCount: [],
    lastAttemptTime: [],
    lastAttemptErrorMessage: [null, [Validators.maxLength(500)]]
  });

  constructor(
    protected dataUtils: JhiDataUtils,
    protected eventManager: JhiEventManager,
    protected startJobCommandService: StartJobCommandService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ startJobCommand }) => {
      if (!startJobCommand.id) {
        const today = moment().startOf('day');
        startJobCommand.nextAttemptTime = today;
        startJobCommand.lastAttemptTime = today;
      }

      this.updateForm(startJobCommand);
    });
  }

  updateForm(startJobCommand: IStartJobCommand): void {
    this.editForm.patchValue({
      id: startJobCommand.id,
      jobName: startJobCommand.jobName,
      jobParams: startJobCommand.jobParams,
      status: startJobCommand.status,
      nextAttemptTime: startJobCommand.nextAttemptTime ? startJobCommand.nextAttemptTime.format(DATE_TIME_FORMAT) : null,
      attemptCount: startJobCommand.attemptCount,
      lastAttemptTime: startJobCommand.lastAttemptTime ? startJobCommand.lastAttemptTime.format(DATE_TIME_FORMAT) : null,
      lastAttemptErrorMessage: startJobCommand.lastAttemptErrorMessage
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe(null, (err: JhiFileLoadError) => {
      this.eventManager.broadcast(
        new JhiEventWithContent<AlertError>('batchuiApp.error', { ...err, key: 'error.file.' + err.key })
      );
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const startJobCommand = this.createFromForm();
    if (startJobCommand.id !== undefined) {
      this.subscribeToSaveResponse(this.startJobCommandService.update(startJobCommand));
    } else {
      this.subscribeToSaveResponse(this.startJobCommandService.create(startJobCommand));
    }
  }

  private createFromForm(): IStartJobCommand {
    return {
      ...new StartJobCommand(),
      id: this.editForm.get(['id'])!.value,
      jobName: this.editForm.get(['jobName'])!.value,
      jobParams: this.editForm.get(['jobParams'])!.value,
      status: this.editForm.get(['status'])!.value,
      nextAttemptTime: this.editForm.get(['nextAttemptTime'])!.value
        ? moment(this.editForm.get(['nextAttemptTime'])!.value, DATE_TIME_FORMAT)
        : undefined,
      attemptCount: this.editForm.get(['attemptCount'])!.value,
      lastAttemptTime: this.editForm.get(['lastAttemptTime'])!.value
        ? moment(this.editForm.get(['lastAttemptTime'])!.value, DATE_TIME_FORMAT)
        : undefined,
      lastAttemptErrorMessage: this.editForm.get(['lastAttemptErrorMessage'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IStartJobCommand>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
