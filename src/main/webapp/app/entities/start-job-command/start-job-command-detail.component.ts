import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { JhiDataUtils } from 'ng-jhipster';

import { IStartJobCommand } from 'app/shared/model/start-job-command.model';

@Component({
  selector: 'jhi-start-job-command-detail',
  templateUrl: './start-job-command-detail.component.html'
})
export class StartJobCommandDetailComponent implements OnInit {
  startJobCommand: IStartJobCommand | null = null;

  constructor(protected dataUtils: JhiDataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ startJobCommand }) => (this.startJobCommand = startJobCommand));
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    this.dataUtils.openFile(contentType, base64String);
  }

  previousState(): void {
    window.history.back();
  }
}
