import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IStartJobCommand, StartJobCommand } from 'app/shared/model/start-job-command.model';
import { StartJobCommandService } from './start-job-command.service';
import { StartJobCommandComponent } from './start-job-command.component';
import { StartJobCommandDetailComponent } from './start-job-command-detail.component';
import { StartJobCommandUpdateComponent } from './start-job-command-update.component';

@Injectable({ providedIn: 'root' })
export class StartJobCommandResolve implements Resolve<IStartJobCommand> {
  constructor(private service: StartJobCommandService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IStartJobCommand> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((startJobCommand: HttpResponse<StartJobCommand>) => {
          if (startJobCommand.body) {
            return of(startJobCommand.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new StartJobCommand());
  }
}

export const startJobCommandRoute: Routes = [
  {
    path: '',
    component: StartJobCommandComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,desc',
      pageTitle: 'batchuiApp.startJobCommand.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: StartJobCommandDetailComponent,
    resolve: {
      startJobCommand: StartJobCommandResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'batchuiApp.startJobCommand.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: StartJobCommandUpdateComponent,
    resolve: {
      startJobCommand: StartJobCommandResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'batchuiApp.startJobCommand.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: StartJobCommandUpdateComponent,
    resolve: {
      startJobCommand: StartJobCommandResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'batchuiApp.startJobCommand.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];
