import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { BatchuiSharedModule } from 'app/shared/shared.module';
import { StartJobCommandComponent } from './start-job-command.component';
import { StartJobCommandDetailComponent } from './start-job-command-detail.component';
import { StartJobCommandUpdateComponent } from './start-job-command-update.component';
import { StartJobCommandDeleteDialogComponent } from './start-job-command-delete-dialog.component';
import { startJobCommandRoute } from './start-job-command.route';

@NgModule({
  imports: [BatchuiSharedModule, RouterModule.forChild(startJobCommandRoute)],
  declarations: [
    StartJobCommandComponent,
    StartJobCommandDetailComponent,
    StartJobCommandUpdateComponent,
    StartJobCommandDeleteDialogComponent
  ],
  entryComponents: [StartJobCommandDeleteDialogComponent]
})
export class BatchuiStartJobCommandModule {}
