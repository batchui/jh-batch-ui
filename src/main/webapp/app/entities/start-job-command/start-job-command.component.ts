import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiDataUtils } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IStartJobCommand } from 'app/shared/model/start-job-command.model';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { StartJobCommandService } from './start-job-command.service';
import { StartJobCommandDeleteDialogComponent } from './start-job-command-delete-dialog.component';

@Component({
  selector: 'jhi-start-job-command',
  templateUrl: './start-job-command.component.html'
})
export class StartJobCommandComponent implements OnInit, OnDestroy {
  startJobCommands?: IStartJobCommand[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending!: boolean;
  ngbPaginationPage = 1;

  constructor(
    protected startJobCommandService: StartJobCommandService,
    protected activatedRoute: ActivatedRoute,
    protected dataUtils: JhiDataUtils,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected loadingBarService: LoadingBarService
  ) {}

  loadPage(page?: number): void {
    this.loadingBarService.start();
    const pageToLoad: number = page || this.page;

    this.startJobCommandService
      .query({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IStartJobCommand[]>) => this.onSuccess(res.body, res.headers, pageToLoad),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.ascending = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
      this.ngbPaginationPage = data.pagingParams.page;
      this.loadPage();
    });
    this.registerChangeInStartJobCommands();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IStartJobCommand): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(contentType: string, base64String: string): void {
    return this.dataUtils.openFile(contentType, base64String);
  }

  registerChangeInStartJobCommands(): void {
    this.eventSubscriber = this.eventManager.subscribe('startJobCommandListModification', () => this.loadPage());
  }

  delete(startJobCommand: IStartJobCommand): void {
    const modalRef = this.modalService.open(StartJobCommandDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.startJobCommand = startJobCommand;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  refresh(): void {
    this.loadPage(this.page);
  }

  protected onSuccess(data: IStartJobCommand[] | null, headers: HttpHeaders, page: number): void {
    this.loadingBarService.complete();
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/start-job-command'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.startJobCommands = data || [];
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }
}
