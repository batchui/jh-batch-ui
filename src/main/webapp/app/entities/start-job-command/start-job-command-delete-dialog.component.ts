import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';

import { IStartJobCommand } from 'app/shared/model/start-job-command.model';
import { StartJobCommandService } from './start-job-command.service';

@Component({
  templateUrl: './start-job-command-delete-dialog.component.html'
})
export class StartJobCommandDeleteDialogComponent {
  startJobCommand?: IStartJobCommand;

  constructor(
    protected startJobCommandService: StartJobCommandService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.startJobCommandService.delete(id).subscribe(() => {
      this.eventManager.broadcast('startJobCommandListModification');
      this.activeModal.close();
    });
  }
}
