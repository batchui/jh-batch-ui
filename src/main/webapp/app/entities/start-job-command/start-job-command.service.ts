import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IStartJobCommand } from 'app/shared/model/start-job-command.model';
import { IStartBatchJob } from 'app/shared/model/start-batch-job.model';

type EntityResponseType = HttpResponse<IStartJobCommand>;
type EntityArrayResponseType = HttpResponse<IStartJobCommand[]>;

@Injectable({ providedIn: 'root' })
export class StartJobCommandService {
  public resourceUrl = SERVER_API_URL + 'api/start-job-commands';

  constructor(protected http: HttpClient) {}

  create(startJobCommand: IStartJobCommand): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(startJobCommand);
    return this.http
      .post<IStartJobCommand>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(startJobCommand: IStartJobCommand): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(startJobCommand);
    return this.http
      .put<IStartJobCommand>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IStartJobCommand>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IStartJobCommand[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  startNewJob(startBatchJob: IStartBatchJob): Observable<EntityResponseType> {
    return this.http
      .post<IStartJobCommand>('/api/start-batch-job', startBatchJob, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  protected convertDateFromClient(startJobCommand: IStartJobCommand): IStartJobCommand {
    const copy: IStartJobCommand = Object.assign({}, startJobCommand, {
      nextAttemptTime:
        startJobCommand.nextAttemptTime && startJobCommand.nextAttemptTime.isValid() ? startJobCommand.nextAttemptTime.toJSON() : undefined,
      lastAttemptTime:
        startJobCommand.lastAttemptTime && startJobCommand.lastAttemptTime.isValid() ? startJobCommand.lastAttemptTime.toJSON() : undefined
    });
    return copy;
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.nextAttemptTime = res.body.nextAttemptTime ? moment(res.body.nextAttemptTime) : undefined;
      res.body.lastAttemptTime = res.body.lastAttemptTime ? moment(res.body.lastAttemptTime) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((startJobCommand: IStartJobCommand) => {
        startJobCommand.nextAttemptTime = startJobCommand.nextAttemptTime ? moment(startJobCommand.nextAttemptTime) : undefined;
        startJobCommand.lastAttemptTime = startJobCommand.lastAttemptTime ? moment(startJobCommand.lastAttemptTime) : undefined;
      });
    }
    return res;
  }
}
