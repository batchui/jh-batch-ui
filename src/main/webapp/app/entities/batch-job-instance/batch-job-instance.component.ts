import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { IBatchJobInstance } from 'app/shared/model/batch-job-instance.model';
import { ITEMS_PER_PAGE } from 'app/shared/constants/pagination.constants';
import { BatchJobInstanceService } from './batch-job-instance.service';
import { BatchJobInstanceDeleteDialogComponent } from './batch-job-instance-delete-dialog.component';
import { BatchJobInstanceBulkDeleteDialogComponent } from './batch-job-instance-bulkdelete-dialog.component';
import { FormGroup, FormBuilder, FormControl, FormArray } from '@angular/forms';

@Component({
  selector: 'jhi-batch-job-instance',
  templateUrl: './batch-job-instance.component.html'
})
export class BatchJobInstanceComponent implements OnInit, OnDestroy {
  batchJobInstances?: IBatchJobInstance[];
  eventSubscriber?: Subscription;
  totalItems = 0;
  itemsPerPage = ITEMS_PER_PAGE;
  page!: number;
  predicate!: string;
  ascending = false;
  ngbPaginationPage = 1;

  selectRowForm: FormGroup;
  selectedIds: number[] = [];
  afterDeletePage = undefined;

  constructor(
    protected batchJobInstanceService: BatchJobInstanceService,
    protected activatedRoute: ActivatedRoute,
    protected router: Router,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal,
    protected fb: FormBuilder,
    protected loadingBarService: LoadingBarService
  ) {
    this.selectRowForm = this.fb.group({
      rowSelector: [],
      elements: new FormArray([])
    });
  }

  loadPage(page?: number): void {
    this.loadingBarService.start();
    const pageToLoad: number = page || this.page;

    this.batchJobInstanceService
      .query({
        page: pageToLoad - 1,
        size: this.itemsPerPage,
        sort: this.sort()
      })
      .subscribe(
        (res: HttpResponse<IBatchJobInstance[]>) => this.onSuccess(res.body, res.headers, pageToLoad),
        () => this.onError()
      );
  }

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(data => {
      this.page = data.pagingParams.page;
      this.ascending = data.pagingParams.ascending;
      this.predicate = data.pagingParams.predicate;
      this.ngbPaginationPage = data.pagingParams.page;
      this.loadPage();
    });
    this.registerChangeInBatchJobInstances();
    this.registerBulkDeleteBatchJobInstances();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IBatchJobInstance): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInBatchJobInstances(): void {
    this.eventSubscriber = this.eventManager.subscribe('batchJobInstanceListModification', () => this.loadPage());
  }

  registerBulkDeleteBatchJobInstances(): void {
    this.eventSubscriber = this.eventManager.subscribe('batchJobInstanceBulkDelete', () => this.loadPageAfterDelete());
  }

  delete(batchJobInstance: IBatchJobInstance): void {
    const modalRef = this.modalService.open(BatchJobInstanceDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.batchJobInstance = batchJobInstance;
  }

  sort(): string[] {
    const result = [this.predicate + ',' + (this.ascending ? 'asc' : 'desc')];
    if (this.predicate !== 'id') {
      result.push('id');
    }
    return result;
  }

  toggle(): void {
    let targetValue = true;
    if (this.selectRowForm.value.rowSelector) {
      targetValue = false;
    }
    (this.selectRowForm.controls.elements as FormArray).controls.forEach(control => {
      control.setValue(targetValue);
    });
  }

  deleteSelected(): void {
    this.selectedIds = this.selectRowForm.value.elements
      .map((v: any, i: number) => (v ? this.batchJobInstances![i].id : null))
      .filter((v: null) => v !== null);

    if (this.selectedIds.length > 0) {
      const modalRef = this.modalService.open(BatchJobInstanceBulkDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
      modalRef.componentInstance.ids = this.selectedIds;
    } else {
      // eslint-disable-next-line no-console
      console.log('Please select some items');
    }
  }

  protected onSuccess(data: IBatchJobInstance[] | null, headers: HttpHeaders, page: number): void {
    this.loadingBarService.complete();
    this.totalItems = Number(headers.get('X-Total-Count'));
    this.page = page;
    this.router.navigate(['/batch-job-instance'], {
      queryParams: {
        page: this.page,
        size: this.itemsPerPage,
        sort: this.predicate + ',' + (this.ascending ? 'asc' : 'desc')
      }
    });
    this.selectRowForm.controls.rowSelector.reset();
    this.addCheckboxes(data);
    this.batchJobInstances = data || [];
  }

  protected loadPageAfterDelete(): void {
    let targetPage = 1;
    if (this.page > 1) {
      const itemsAfterDelete = this.totalItems - this.selectedIds.length;
      if (this.page * ITEMS_PER_PAGE > itemsAfterDelete) {
        targetPage = this.page - 1;
      }
    }
    this.loadPage(targetPage);
  }

  // eslint-disable-next-line
  protected onSuccessDelete(body: {} | null): void {
    this.loadPage();
  }

  protected onError(): void {
    this.ngbPaginationPage = this.page;
  }

  protected addCheckboxes(data: IBatchJobInstance[] | null): void {
    (this.selectRowForm.controls.elements as FormArray).clear();
    for (let i = 0; i < data!.length; i++) {
      const control = new FormControl(false);
      (this.selectRowForm.controls.elements as FormArray).push(control);
    }
  }
}
