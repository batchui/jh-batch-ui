import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { BatchJobInstanceService } from './batch-job-instance.service';
import { LoadingBarService } from '@ngx-loading-bar/core';

@Component({
  templateUrl: './batch-job-instance-bulkdelete-dialog.component.html'
})
export class BatchJobInstanceBulkDeleteDialogComponent {
  ids?: number[];

  constructor(
    protected batchJobInstanceService: BatchJobInstanceService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager,
    protected loadingBarService: LoadingBarService
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(): void {
    this.loadingBarService.start();
    this.batchJobInstanceService.deleteItems(this.ids!).subscribe(() => {
      this.loadingBarService.complete();
      this.eventManager.broadcast('batchJobInstanceBulkDelete');
      this.activeModal.close();
    });
  }
}
