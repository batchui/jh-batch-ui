import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { JhiEventManager } from 'ng-jhipster';
import { LoadingBarService } from '@ngx-loading-bar/core';
import { IBatchJobInstance } from 'app/shared/model/batch-job-instance.model';
import { BatchJobInstanceService } from './batch-job-instance.service';

@Component({
  templateUrl: './batch-job-instance-delete-dialog.component.html'
})
export class BatchJobInstanceDeleteDialogComponent {
  batchJobInstance?: IBatchJobInstance;

  constructor(
    protected batchJobInstanceService: BatchJobInstanceService,
    public activeModal: NgbActiveModal,
    protected eventManager: JhiEventManager,
    protected loadingBarService: LoadingBarService
  ) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.loadingBarService.start();
    this.batchJobInstanceService.delete(id).subscribe(() => {
      this.loadingBarService.complete();
      this.eventManager.broadcast('batchJobInstanceListModification');
      this.activeModal.close();
    });
  }
}
