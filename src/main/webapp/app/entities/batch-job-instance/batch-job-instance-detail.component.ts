import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { IBatchJobInstance } from 'app/shared/model/batch-job-instance.model';
import { IBatchJobExecution } from 'app/shared/model/batch-job-execution.model';
import { BatchJobInstanceDeleteDialogComponent } from './batch-job-instance-delete-dialog.component';

@Component({
  selector: 'jhi-batch-job-instance-detail',
  templateUrl: './batch-job-instance-detail.component.html'
})
export class BatchJobInstanceDetailComponent implements OnInit, OnDestroy {
  batchJobInstance: IBatchJobInstance | null = null;
  lastBatchJobExecution: IBatchJobExecution | null = null;
  eventSubscriber?: Subscription;

  constructor(protected activatedRoute: ActivatedRoute, protected modalService: NgbModal, protected eventManager: JhiEventManager) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ batchJobInstance }) => this.processBatchJobInstance(batchJobInstance));
    this.registerChangeInBatchJobInstances();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  previousState(): void {
    window.history.back();
  }

  delete(): void {
    const modalRef = this.modalService.open(BatchJobInstanceDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.batchJobInstance = this.batchJobInstance;
  }

  registerChangeInBatchJobInstances(): void {
    this.eventSubscriber = this.eventManager.subscribe('batchJobInstanceListModification', () => this.previousState());
  }

  private processBatchJobInstance(batchJobInstance: IBatchJobInstance): void {
    this.batchJobInstance = batchJobInstance;

    // sort desc batchJobExecutions based on endTime ...
    if (batchJobInstance.batchJobExecutions && batchJobInstance.batchJobExecutions.length > 0) {
      if (batchJobInstance.batchJobExecutions.length > 1) {
        this.batchJobInstance.batchJobExecutions = batchJobInstance.batchJobExecutions.sort((je1, je2) => {
          if (je1.endTime!.isAfter(je2.endTime)) {
            return 1;
          }
          if (je1.endTime!.isBefore(je2.endTime)) {
            return -1;
          }
          return 0;
        });
      }
      this.lastBatchJobExecution = batchJobInstance.batchJobExecutions[0];
    }
  }
}
