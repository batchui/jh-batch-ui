import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { SERVER_API_URL } from 'app/app.constants';
import { createRequestOption } from 'app/shared/util/request-util';
import { IBatchJobInstance } from 'app/shared/model/batch-job-instance.model';

type EntityResponseType = HttpResponse<IBatchJobInstance>;
type EntityArrayResponseType = HttpResponse<IBatchJobInstance[]>;

@Injectable({ providedIn: 'root' })
export class BatchJobInstanceService {
  public resourceUrl = SERVER_API_URL + 'api/batch-job-instances';
  public rescouceJobNamesUrl = SERVER_API_URL + 'api/batch-job-names';

  constructor(protected http: HttpClient) {}

  create(batchJobInstance: IBatchJobInstance): Observable<EntityResponseType> {
    return this.http.post<IBatchJobInstance>(this.resourceUrl, batchJobInstance, { observe: 'response' });
  }

  update(batchJobInstance: IBatchJobInstance): Observable<EntityResponseType> {
    return this.http.put<IBatchJobInstance>(this.resourceUrl, batchJobInstance, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IBatchJobInstance>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IBatchJobInstance[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  // --- custom methods -------------------------------------------------------

  deleteItems(ids: number[]): Observable<HttpResponse<{}>> {
    // eslint-disable-next-line no-console
    console.log(ids);
    return this.http.post(`${this.resourceUrl}/deletes`, ids, { observe: 'response' });
  }

  getJobNames(): Observable<HttpResponse<string[]>> {
    return this.http.get<string[]>(this.rescouceJobNamesUrl, { observe: 'response' });
  }
}
