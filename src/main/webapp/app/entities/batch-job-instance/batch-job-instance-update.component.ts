import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { IBatchJobInstance, BatchJobInstance } from 'app/shared/model/batch-job-instance.model';
import { BatchJobInstanceService } from './batch-job-instance.service';

@Component({
  selector: 'jhi-batch-job-instance-update',
  templateUrl: './batch-job-instance-update.component.html'
})
export class BatchJobInstanceUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    jobInstanceId: [null, [Validators.required]],
    version: [],
    jobName: [null, [Validators.required, Validators.maxLength(100)]],
    jobKey: [null, [Validators.required, Validators.maxLength(32)]]
  });

  constructor(
    protected batchJobInstanceService: BatchJobInstanceService,
    protected activatedRoute: ActivatedRoute,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ batchJobInstance }) => {
      this.updateForm(batchJobInstance);
    });
  }

  updateForm(batchJobInstance: IBatchJobInstance): void {
    this.editForm.patchValue({
      id: batchJobInstance.id,
      jobInstanceId: batchJobInstance.jobInstanceId,
      version: batchJobInstance.version,
      jobName: batchJobInstance.jobName,
      jobKey: batchJobInstance.jobKey
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const batchJobInstance = this.createFromForm();
    if (batchJobInstance.id !== undefined) {
      this.subscribeToSaveResponse(this.batchJobInstanceService.update(batchJobInstance));
    } else {
      this.subscribeToSaveResponse(this.batchJobInstanceService.create(batchJobInstance));
    }
  }

  private createFromForm(): IBatchJobInstance {
    return {
      ...new BatchJobInstance(),
      id: this.editForm.get(['id'])!.value,
      jobInstanceId: this.editForm.get(['jobInstanceId'])!.value,
      version: this.editForm.get(['version'])!.value,
      jobName: this.editForm.get(['jobName'])!.value,
      jobKey: this.editForm.get(['jobKey'])!.value
    };
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IBatchJobInstance>>): void {
    result.subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.isSaving = false;
    this.previousState();
  }

  protected onSaveError(): void {
    this.isSaving = false;
  }
}
