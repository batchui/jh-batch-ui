import { NgModule, Injectable } from '@angular/core';
import { RouterModule } from '@angular/router';

import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Routes, Router } from '@angular/router';
import { JhiResolvePagingParams } from 'ng-jhipster';
import { Observable, of, EMPTY } from 'rxjs';
import { flatMap } from 'rxjs/operators';

import { Authority } from 'app/shared/constants/authority.constants';
import { UserRouteAccessService } from 'app/core/auth/user-route-access-service';
import { IBatchJobInstance, BatchJobInstance } from 'app/shared/model/batch-job-instance.model';
import { BatchJobInstanceService } from './batch-job-instance.service';
import { BatchJobInstanceComponent } from './batch-job-instance.component';
import { BatchJobInstanceDetailComponent } from './batch-job-instance-detail.component';
import { BatchJobInstanceUpdateComponent } from './batch-job-instance-update.component';

@Injectable({ providedIn: 'root' })
export class BatchJobInstanceResolve implements Resolve<IBatchJobInstance> {
  constructor(private service: BatchJobInstanceService, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IBatchJobInstance> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        flatMap((batchJobInstance: HttpResponse<BatchJobInstance>) => {
          if (batchJobInstance.body) {
            return of(batchJobInstance.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new BatchJobInstance());
  }
}

const batchJobInstanceRoute: Routes = [
  {
    path: '',
    component: BatchJobInstanceComponent,
    resolve: {
      pagingParams: JhiResolvePagingParams
    },
    data: {
      authorities: [Authority.USER],
      defaultSort: 'id,desc',
      pageTitle: 'batchuiApp.batchJobInstance.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/view',
    component: BatchJobInstanceDetailComponent,
    resolve: {
      batchJobInstance: BatchJobInstanceResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'batchuiApp.batchJobInstance.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: 'new',
    component: BatchJobInstanceUpdateComponent,
    resolve: {
      batchJobInstance: BatchJobInstanceResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'batchuiApp.batchJobInstance.home.title'
    },
    canActivate: [UserRouteAccessService]
  },
  {
    path: ':id/edit',
    component: BatchJobInstanceUpdateComponent,
    resolve: {
      batchJobInstance: BatchJobInstanceResolve
    },
    data: {
      authorities: [Authority.USER],
      pageTitle: 'batchuiApp.batchJobInstance.home.title'
    },
    canActivate: [UserRouteAccessService]
  }
];

@NgModule({
  imports: [RouterModule.forChild(batchJobInstanceRoute)],
  exports: [RouterModule]
})
export class BatchJobInstanceRoute {}
