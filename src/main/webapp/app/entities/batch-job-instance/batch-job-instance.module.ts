import { NgModule } from '@angular/core';
// import { RouterModule } from '@angular/router';

import { BatchuiSharedModule } from 'app/shared/shared.module';
import { BatchJobInstanceComponent } from './batch-job-instance.component';
import { BatchJobInstanceDetailComponent } from './batch-job-instance-detail.component';
import { BatchJobInstanceUpdateComponent } from './batch-job-instance-update.component';
import { BatchJobInstanceDeleteDialogComponent } from './batch-job-instance-delete-dialog.component';
import { BatchJobInstanceBulkDeleteDialogComponent } from './batch-job-instance-bulkdelete-dialog.component';
import { BatchuiBatchJobExecutionModule } from '../batch-job-execution/batch-job-execution.module';
import { BatchJobInstanceRoute } from './batch-job-instance.route';

@NgModule({
  imports: [BatchJobInstanceRoute, BatchuiSharedModule, BatchuiBatchJobExecutionModule],
  declarations: [
    BatchJobInstanceComponent,
    BatchJobInstanceDetailComponent,
    BatchJobInstanceUpdateComponent,
    BatchJobInstanceDeleteDialogComponent,
    BatchJobInstanceBulkDeleteDialogComponent
  ],
  entryComponents: [BatchJobInstanceDeleteDialogComponent, BatchJobInstanceBulkDeleteDialogComponent]
})
export class BatchuiBatchJobInstanceModule {}
