package com.ag04.batchui.web.rest;

import com.ag04.batchui.service.StartJobCommandService;
import com.ag04.batchui.service.dto.StartBatchJobDTO;
import com.ag04.batchui.web.rest.errors.BadRequestAlertException;
import com.ag04.batchui.service.dto.StartJobCommandDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.ag04.batchui.dbqueue.domain.StartJobCommand}.
 */
@RestController
@RequestMapping("/api")
public class StartJobCommandResource {
    private final Logger log = LoggerFactory.getLogger(StartJobCommandResource.class);

    private static final String ENTITY_NAME = "startJobCommand";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final StartJobCommandService startJobCommandService;

    public StartJobCommandResource(StartJobCommandService startJobCommandService) {
        this.startJobCommandService = startJobCommandService;
    }

    /**
     * {@code POST  /start-job-commands} : Create a new startJobCommand.
     *
     * @param startJobCommandDTO the startJobCommandDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new startJobCommandDTO, or with status {@code 400 (Bad Request)} if the startJobCommand has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/start-job-commands")
    public ResponseEntity<StartJobCommandDTO> createStartJobCommand(@Valid @RequestBody StartJobCommandDTO startJobCommandDTO) throws URISyntaxException {
        log.debug("REST request to save StartJobCommand : {}", startJobCommandDTO);
        if (startJobCommandDTO.getId() != null) {
            throw new BadRequestAlertException("A new startJobCommand cannot already have an ID", ENTITY_NAME, "idexists");
        }
        StartJobCommandDTO result = startJobCommandService.save(startJobCommandDTO);
        return ResponseEntity.created(new URI("/api/start-job-commands/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /start-job-commands} : Updates an existing startJobCommand.
     *
     * @param startJobCommandDTO the startJobCommandDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated startJobCommandDTO,
     * or with status {@code 400 (Bad Request)} if the startJobCommandDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the startJobCommandDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/start-job-commands")
    public ResponseEntity<StartJobCommandDTO> updateStartJobCommand(@Valid @RequestBody StartJobCommandDTO startJobCommandDTO) throws URISyntaxException {
        log.debug("REST request to update StartJobCommand : {}", startJobCommandDTO);
        if (startJobCommandDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        StartJobCommandDTO result = startJobCommandService.save(startJobCommandDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, startJobCommandDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /start-job-commands} : get all the startJobCommands.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of startJobCommands in body.
     */
    @GetMapping("/start-job-commands")
    public ResponseEntity<List<StartJobCommandDTO>> getAllStartJobCommands(Pageable pageable) {
        log.debug("REST request to get a page of StartJobCommands");
        Page<StartJobCommandDTO> page = startJobCommandService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /start-job-commands/:id} : get the "id" startJobCommand.
     *
     * @param id the id of the startJobCommandDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the startJobCommandDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/start-job-commands/{id}")
    public ResponseEntity<StartJobCommandDTO> getStartJobCommand(@PathVariable Long id) {
        log.debug("REST request to get StartJobCommand : {}", id);
        Optional<StartJobCommandDTO> startJobCommandDTO = startJobCommandService.findOne(id);
        return ResponseUtil.wrapOrNotFound(startJobCommandDTO);
    }

    /**
     * {@code DELETE  /start-job-commands/:id} : delete the "id" startJobCommand.
     *
     * @param id the id of the startJobCommandDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/start-job-commands/{id}")
    public ResponseEntity<Void> deleteStartJobCommand(@PathVariable Long id) {
        log.debug("REST request to delete StartJobCommand : {}", id);
        startJobCommandService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

}
