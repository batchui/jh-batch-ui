package com.ag04.batchui.web.rest;

import io.github.jhipster.web.util.HeaderUtil;
import org.springframework.http.HttpHeaders;

public class BatchUiHeaderUtil {

    private BatchUiHeaderUtil() {
        //
    }

    /**
     * <p>createEntityDeletionAlert.</p>
     *
     * @param applicationName a {@link java.lang.String} object.
     * @param enableTranslation a boolean.
     * @param entityName a {@link java.lang.String} object.
     * @param param a {@link java.lang.String} object.
     * @return a {@link org.springframework.http.HttpHeaders} object.
     */
    public static HttpHeaders createEntityBulkDeletionAlert(String applicationName, boolean enableTranslation, String entityName, String param) {
        String message = enableTranslation ? applicationName + "." + entityName + ".bulk-deleted"
                : "Bulk deleted total of " + param + " " + entityName + "(s)" ;
        return HeaderUtil.createAlert(applicationName, message, param);
    }
}
