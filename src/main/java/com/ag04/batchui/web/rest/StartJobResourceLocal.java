package com.ag04.batchui.web.rest;

import com.ag04.batchui.service.*;
import com.ag04.batchui.service.dto.BatchJobExecutionDTO;
import com.ag04.batchui.service.dto.StartBatchJobDTO;
import io.github.jhipster.web.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobParameters;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Optional;

/**
 * REST controller for managing {@link com.ag04.batchui.domain.BatchJobExecution}.
 */
@ConditionalOnProperty(value = "batchui.dbqueue.enabled", havingValue = "false")
@RestController
@RequestMapping("/api")
public class StartJobResourceLocal {
    private final Logger log = LoggerFactory.getLogger(StartJobResourceLocal.class);

    private static final String ENTITY_NAME = "batchJobExecution";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BatchJobExecutionService batchJobExecutionService;

    private final JobManagementService jobManagementService;

    public StartJobResourceLocal(
            BatchJobExecutionService batchJobExecutionService,
            JobManagementService jobManagementService
    ) {
        this.batchJobExecutionService = batchJobExecutionService;
        this.jobManagementService = jobManagementService;
    }

    /**
     * {@code POST  /batch-job-executions} : Creates a new batchJobExecution using {@link JobManagementService}.
     *
     * @param startBatchJobDTO the StartBatchJobDTO to start.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new batchJobExecutionDTO, or with status {@code 400 (Bad Request)} if the batchJobExecution has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/start-batch-job")
    public ResponseEntity<BatchJobExecutionDTO> startBatchJob(
            @Valid @RequestBody StartBatchJobDTO startBatchJobDTO
    ) throws Exception {
        log.debug("REST request to start BatchJob : {}", startBatchJobDTO);

        JobParameters jobParameters = JobParamsUtil.convert(startBatchJobDTO.getBatchJobExecutionParams());
        Long id = jobManagementService.startNewJob(startBatchJobDTO.getJobName(), jobParameters);
        Optional<BatchJobExecutionDTO> result = batchJobExecutionService.findOne(id);
        BatchJobExecutionDTO dto = result.get();

        return ResponseEntity.created(new URI("/api/batch-job-executions/" + dto.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, dto.getId().toString()))
                .body(dto);
    }
}
