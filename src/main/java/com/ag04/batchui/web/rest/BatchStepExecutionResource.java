package com.ag04.batchui.web.rest;

import com.ag04.batchui.domain.BatchStepExecution;
import com.ag04.batchui.repository.BatchStepExecutionRepository;
import com.ag04.batchui.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.ag04.batchui.domain.BatchStepExecution}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class BatchStepExecutionResource {

    private final Logger log = LoggerFactory.getLogger(BatchStepExecutionResource.class);

    private static final String ENTITY_NAME = "batchStepExecution";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BatchStepExecutionRepository batchStepExecutionRepository;

    public BatchStepExecutionResource(BatchStepExecutionRepository batchStepExecutionRepository) {
        this.batchStepExecutionRepository = batchStepExecutionRepository;
    }

    /**
     * {@code GET  /batch-step-executions} : get all the batchStepExecutions.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of batchStepExecutions in body.
     */
    @GetMapping("/batch-step-executions")
    public List<BatchStepExecution> getAllBatchStepExecutions() {
        log.debug("REST request to get all BatchStepExecutions");
        return batchStepExecutionRepository.findAll();
    }

    /**
     * {@code GET  /batch-step-executions/:id} : get the "id" batchStepExecution.
     *
     * @param id the id of the batchStepExecution to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the batchStepExecution, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/batch-step-executions/{id}")
    public ResponseEntity<BatchStepExecution> getBatchStepExecution(@PathVariable Long id) {
        log.debug("REST request to get BatchStepExecution : {}", id);
        Optional<BatchStepExecution> batchStepExecution = batchStepExecutionRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(batchStepExecution);
    }

}
