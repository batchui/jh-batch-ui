package com.ag04.batchui.web.rest;

import com.ag04.batchui.service.BatchJobInstanceService;
import com.ag04.batchui.web.rest.errors.BadRequestAlertException;
import com.ag04.batchui.service.dto.BatchJobInstanceDTO;
import com.ag04.batchui.service.dto.BatchJobInstanceCriteria;
import com.ag04.batchui.service.BatchJobInstanceQueryService;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.ag04.batchui.domain.BatchJobInstance}.
 */
@RestController
@RequestMapping("/api")
public class BatchJobInstanceResource {

    private final Logger log = LoggerFactory.getLogger(BatchJobInstanceResource.class);

    private static final String ENTITY_NAME = "batchJobInstance";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BatchJobInstanceService batchJobInstanceService;

    private final BatchJobInstanceQueryService batchJobInstanceQueryService;

    public BatchJobInstanceResource(BatchJobInstanceService batchJobInstanceService, BatchJobInstanceQueryService batchJobInstanceQueryService) {
        this.batchJobInstanceService = batchJobInstanceService;
        this.batchJobInstanceQueryService = batchJobInstanceQueryService;
    }

    /**
     * {@code POST  /batch-job-instances} : Create a new batchJobInstance.
     *
     * @param batchJobInstanceDTO the batchJobInstanceDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new batchJobInstanceDTO, or with status {@code 400 (Bad Request)} if the batchJobInstance has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/batch-job-instances")
    public ResponseEntity<BatchJobInstanceDTO> createBatchJobInstance(@Valid @RequestBody BatchJobInstanceDTO batchJobInstanceDTO) throws URISyntaxException {
        log.debug("REST request to save BatchJobInstance : {}", batchJobInstanceDTO);
        if (batchJobInstanceDTO.getId() != null) {
            throw new BadRequestAlertException("A new batchJobInstance cannot already have an ID", ENTITY_NAME, "idexists");
        }
        BatchJobInstanceDTO result = batchJobInstanceService.save(batchJobInstanceDTO);
        return ResponseEntity.created(new URI("/api/batch-job-instances/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /batch-job-instances} : Updates an existing batchJobInstance.
     *
     * @param batchJobInstanceDTO the batchJobInstanceDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated batchJobInstanceDTO,
     * or with status {@code 400 (Bad Request)} if the batchJobInstanceDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the batchJobInstanceDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/batch-job-instances")
    public ResponseEntity<BatchJobInstanceDTO> updateBatchJobInstance(@Valid @RequestBody BatchJobInstanceDTO batchJobInstanceDTO) throws URISyntaxException {
        log.debug("REST request to update BatchJobInstance : {}", batchJobInstanceDTO);
        if (batchJobInstanceDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BatchJobInstanceDTO result = batchJobInstanceService.save(batchJobInstanceDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, batchJobInstanceDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /batch-job-instances} : get all the batchJobInstances.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of batchJobInstances in body.
     */
    @GetMapping("/batch-job-instances")
    public ResponseEntity<List<BatchJobInstanceDTO>> getAllBatchJobInstances(BatchJobInstanceCriteria criteria, Pageable pageable) {
        log.debug("REST request to get BatchJobInstances by criteria: {}", criteria);
        Page<BatchJobInstanceDTO> page = batchJobInstanceQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /batch-job-instances/count} : count all the batchJobInstances.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/batch-job-instances/count")
    public ResponseEntity<Long> countBatchJobInstances(BatchJobInstanceCriteria criteria) {
        log.debug("REST request to count BatchJobInstances by criteria: {}", criteria);
        return ResponseEntity.ok().body(batchJobInstanceQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /batch-job-instances/:id} : get the "id" batchJobInstance.
     *
     * @param id the id of the batchJobInstanceDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the batchJobInstanceDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/batch-job-instances/{id}")
    public ResponseEntity<BatchJobInstanceDTO> getBatchJobInstance(@PathVariable Long id) {
        log.debug("REST request to get BatchJobInstance : {}", id);
        Optional<BatchJobInstanceDTO> batchJobInstanceDTO = batchJobInstanceService.findOne(id);
        return ResponseUtil.wrapOrNotFound(batchJobInstanceDTO);
    }

    /**
     * {@code DELETE  /batch-job-instances/:id} : delete the "id" batchJobInstance.
     *
     * @param id the id of the batchJobInstanceDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/batch-job-instances/{id}")
    public ResponseEntity<Void> deleteBatchJobInstance(@PathVariable Long id) {
        log.debug("REST request to delete BatchJobInstance : {}", id);
        batchJobInstanceService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    @GetMapping("/batch-job-names")
    public ResponseEntity<List<String>> getBatchJobNames() {
        log.debug("REST request to get Job Names");
        List<String> result = batchJobInstanceService.getAllJobNames();
        return ResponseEntity.ok().body(result);
    }

    @PostMapping("/batch-job-instances/deletes")
    public ResponseEntity<Void> deleteInstances(@RequestBody Long[] ids) throws URISyntaxException {
        log.debug("REST request to delete BatchJobInstances : {}", Arrays.toString(ids));
        if (ids == null || ids.length == 0) {
            throw new BadRequestAlertException("No IDs supplied for Bulk delete", ENTITY_NAME, "idnull");
        }
        batchJobInstanceService.deleteBulk(ids);
        return ResponseEntity.noContent().headers(BatchUiHeaderUtil.createEntityBulkDeletionAlert(applicationName, true, ENTITY_NAME, String.valueOf(ids.length))).build();
    }

}
