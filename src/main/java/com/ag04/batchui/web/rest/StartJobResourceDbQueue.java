package com.ag04.batchui.web.rest;

import com.ag04.batchui.service.StartJobCommandService;
import com.ag04.batchui.service.dto.StartBatchJobDTO;
import com.ag04.batchui.service.dto.StartJobCommandDTO;
import io.github.jhipster.web.util.HeaderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.net.URI;
import java.util.Optional;

@ConditionalOnProperty(value = "batchui.dbqueue.enabled", havingValue = "true", matchIfMissing = true)
@RestController
@RequestMapping("/api")
public class StartJobResourceDbQueue {
    private final Logger log = LoggerFactory.getLogger(StartJobResourceDbQueue.class);

    private static final String ENTITY_NAME = "startJobCommand";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final StartJobCommandService startJobCommandService;

    public StartJobResourceDbQueue(StartJobCommandService startJobCommandService) {
        this.startJobCommandService = startJobCommandService;
    }

    @PostMapping("/start-batch-job")
    public ResponseEntity<StartJobCommandDTO> scheduleJob(@Valid @RequestBody StartBatchJobDTO startBatchJobDTO
    ) throws Exception {
        log.debug("REST request to schedule BatchJobExecution : {}", startBatchJobDTO);

        Long id = startJobCommandService.scheduleNew(startBatchJobDTO.getJobName(), startBatchJobDTO.getBatchJobExecutionParams());

        Optional<StartJobCommandDTO> result = startJobCommandService.findOne(id);
        StartJobCommandDTO dto = result.get();
        return ResponseEntity.created(new URI("/api/start-job-commands/" + dto.getId()))
                .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, dto.getId().toString()))
                .body(dto);
    }
}
