package com.ag04.batchui.web.rest;

import com.ag04.batchui.service.*;
import com.ag04.batchui.service.dto.JobExecutionStatusReport;
import com.ag04.batchui.web.rest.errors.BadRequestAlertException;
import com.ag04.batchui.service.dto.BatchJobExecutionDTO;
import com.ag04.batchui.service.dto.BatchJobExecutionCriteria;
import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.PaginationUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.ag04.batchui.domain.BatchJobExecution}.
 */
@RestController
@RequestMapping("/api")
public class BatchJobExecutionResource {

    private final Logger log = LoggerFactory.getLogger(BatchJobExecutionResource.class);

    private static final String ENTITY_NAME = "batchJobExecution";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final BatchJobExecutionService batchJobExecutionService;

    private final BatchJobExecutionQueryService batchJobExecutionQueryService;

    public BatchJobExecutionResource(
            BatchJobExecutionService batchJobExecutionService,
            BatchJobExecutionQueryService batchJobExecutionQueryService
    ) {
        this.batchJobExecutionService = batchJobExecutionService;
        this.batchJobExecutionQueryService = batchJobExecutionQueryService;
    }

    /**
     * {@code PUT  /batch-job-executions} : Updates an existing batchJobExecution.
     *
     * @param batchJobExecutionDTO the batchJobExecutionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated batchJobExecutionDTO,
     * or with status {@code 400 (Bad Request)} if the batchJobExecutionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the batchJobExecutionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/batch-job-executions")
    public ResponseEntity<BatchJobExecutionDTO> updateBatchJobExecution(@Valid @RequestBody BatchJobExecutionDTO batchJobExecutionDTO) throws URISyntaxException {
        log.debug("REST request to update BatchJobExecution : {}", batchJobExecutionDTO);
        if (batchJobExecutionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        BatchJobExecutionDTO result = batchJobExecutionService.save(batchJobExecutionDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, batchJobExecutionDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /batch-job-executions} : get all the batchJobExecutions.
     *
     * @param pageable the pagination information.
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of batchJobExecutions in body.
     */
    @GetMapping("/batch-job-executions")
    public ResponseEntity<List<BatchJobExecutionDTO>> getAllBatchJobExecutions(BatchJobExecutionCriteria criteria, Pageable pageable) {
        log.debug("REST request to get BatchJobExecutions by criteria: {}", criteria);
        Page<BatchJobExecutionDTO> page = batchJobExecutionQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /batch-job-executions/count} : count all the batchJobExecutions.
     *
     * @param criteria the criteria which the requested entities should match.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the count in body.
     */
    @GetMapping("/batch-job-executions/count")
    public ResponseEntity<Long> countBatchJobExecutions(BatchJobExecutionCriteria criteria) {
        log.debug("REST request to count BatchJobExecutions by criteria: {}", criteria);
        return ResponseEntity.ok().body(batchJobExecutionQueryService.countByCriteria(criteria));
    }

    /**
     * {@code GET  /batch-job-executions/:id} : get the "id" batchJobExecution.
     *
     * @param id the id of the batchJobExecutionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the batchJobExecutionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/batch-job-executions/{id}")
    public ResponseEntity<BatchJobExecutionDTO> getBatchJobExecution(@PathVariable Long id) {
        log.debug("REST request to get BatchJobExecution : {}", id);
        Optional<BatchJobExecutionDTO> batchJobExecutionDTO = batchJobExecutionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(batchJobExecutionDTO);
    }

    /**
     * {@code DELETE  /batch-job-executions/:id} : delete the "id" batchJobExecution.
     *
     * @param id the id of the batchJobExecutionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/batch-job-executions/{id}")
    public ResponseEntity<Void> deleteBatchJobExecution(@PathVariable Long id) {
        log.debug("REST request to delete BatchJobExecution : {}", id);
        batchJobExecutionService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    /**
     * {@code GET  /batch-job-executions/status-report} : count all the batchJobExecutions by BatchStatus and ExitCode.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the JobExecutionStatusReport in body.
     */
    @GetMapping("/batch-job-executions/status-report")
    public ResponseEntity<JobExecutionStatusReport> statusReport(
            @RequestParam(name="from", required = false) LocalDate from,
            @RequestParam(name="to", required = false) LocalDate to
    ) {
        log.debug("REST request to get Job Executions daily status for from='{}' : to='{}'", from, to);
        JobExecutionStatusReport statusReport;
        if (from == null && to == null) {
            statusReport = batchJobExecutionService.statusReport();
        } else if (from != null) {
            LocalDate toDate = to;
            if (to == null) {
                toDate = LocalDate.now();
            }
            statusReport = batchJobExecutionService.statusReport(from, toDate);
        } else {
            statusReport = batchJobExecutionService.statusReport(to);
        }

        return ResponseEntity.ok().body(statusReport);
    }
}
