/**
 * View Models used by Spring MVC REST controllers.
 */
package com.ag04.batchui.web.rest.vm;
