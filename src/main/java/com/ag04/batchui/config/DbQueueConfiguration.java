package com.ag04.batchui.config;

import com.ag04.batchui.dbqueue.annotation.EnableJobDbQueue;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;

/**
 * Enables configuration of Job DbQueue.
 * This will configure the following:
 * - StartJobCommand entity - added to the list of managed JPA entities
 * - StartJobCommandRepository - configured and registered.
 * - JobManagementServiceAsync - configured and added as Bean to ApplicationContext
 *
 */
@ConditionalOnProperty(value = "batchui.dbqueue.enabled", havingValue = "true", matchIfMissing = true)
@EnableJobDbQueue
@Configuration
public class DbQueueConfiguration {

}
