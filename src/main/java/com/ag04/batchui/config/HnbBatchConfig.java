package com.ag04.batchui.config;

import com.ag04.batchui.batch.ExitStatusJobExecutionListener;
import com.ag04.batchui.batch.hnb.HnbCheckTasklet;
import com.ag04.batchui.batch.hnb.HnbItemReader;
import org.springframework.batch.core.*;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemWriterBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.io.FileSystemResource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Profile("batch-demo")
@Configuration
public class HnbBatchConfig {
    private static final String STRING_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    @Autowired
    JobBuilderFactory jobBuilderFactory;

    @Autowired
    StepBuilderFactory stepBuilderFactory;

    @Autowired
    JobLauncher jobLauncher;

    @Autowired
    RestTemplate restTemplate;

    @Value("${exchange-rate-service.hnb-url}")
    private String hnbApiUrl;

    @Autowired
    private HnbCheckTasklet hnbCheckTasklet;

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(STRING_DATE_FORMAT);

    /**
     * Run every 30 minutes.
     * new JobParametersBuilder().addString("launchDate", formatter.format(date)).toJobParameters()
     * @throws Exception
     */
    @Scheduled(cron = "0 0/15 * * * ?")
    public void launchJob() throws Exception {
        JobExecution jobExecution = jobLauncher.run(
                hnbSyncJob(hnbSyncStep()),
                new JobParametersBuilder()
                    .addDate("launchDate", Date.from(ZonedDateTime.now().toInstant()), true)
                    .addString("actor", "scheduler", false)
                    .toJobParameters()
        );
    }

    @Bean
    public Job hnbSyncJob(Step hnbSyncStep) {
        ExitStatusJobExecutionListener stopExistStatusJobExecutionListener = new ExitStatusJobExecutionListener(ExitStatus.STOPPED);
        Job hnbSyncJob = jobBuilderFactory.get("hnbSyncJob")
            .start(hnbCheckStep())
            .on("STOPPED").end()
            .from(hnbCheckStep()).on("*").to(hnbSyncStep())
            .end()
            .listener(stopExistStatusJobExecutionListener)
            .build();
        return hnbSyncJob;
    }

    @Bean
    public Step hnbSyncStep() {
        return stepBuilderFactory
                .get("hnbSyncStep").chunk(10)
                .reader(hnbItemReader(null))
                .writer(hnbFileWriter(null))
                .build();
    }

    @Bean
    public Step hnbCheckStep() {
        return stepBuilderFactory.get("hnbCheckStep").tasklet(hnbCheckTasklet).build();
    }

    @Bean
    @StepScope
    public HnbItemReader hnbItemReader(@Value("#{jobParameters['launchDate']}") Date launchDate) {
        LocalDate targetDate = launchDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return new HnbItemReader(restTemplate, hnbApiUrl, targetDate);
    }

    @Bean
    @StepScope
    public FlatFileItemWriter hnbFileWriter(@Value("#{jobParameters['launchDate']}") Date launchDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd-hhmm");
        String fileName = "out/hnb_" + sdf.format(launchDate) + ".csv";
        return new FlatFileItemWriterBuilder()
                .name("hnbFileWriter")
                .resource(new FileSystemResource(fileName))
                .delimited()
                .delimiter(",")
                .names(new String[]{"date", "currencyCode", "rate"})
                .build();
    }
}
