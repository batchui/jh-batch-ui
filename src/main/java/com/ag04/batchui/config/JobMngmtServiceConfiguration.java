package com.ag04.batchui.config;

import com.ag04.batchui.service.JobManagementService;
import com.ag04.batchui.service.impl.JobManagementServiceImpl;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Used in scenario when monitored Jobs are available in local ApplicationContext.
 *
 */
@ConditionalOnProperty(value = "batchui.dbqueue.enabled", havingValue = "false")
@Configuration
public class JobMngmtServiceConfiguration {

    @Bean
    public JobManagementService jobManagementService(JobLauncher jobLauncher) {
        return new JobManagementServiceImpl(jobLauncher);
    }
    
}
