package com.ag04.batchui.config;

import java.time.Duration;

import org.ehcache.config.builders.*;
import org.ehcache.jsr107.Eh107Configuration;

import org.hibernate.cache.jcache.ConfigSettings;
import io.github.jhipster.config.JHipsterProperties;

import org.springframework.boot.autoconfigure.cache.JCacheManagerCustomizer;
import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.*;

@Configuration
@EnableCaching
public class CacheConfiguration {

    private final javax.cache.configuration.Configuration<Object, Object> jcacheConfiguration;

    public CacheConfiguration(JHipsterProperties jHipsterProperties) {
        JHipsterProperties.Cache.Ehcache ehcache = jHipsterProperties.getCache().getEhcache();

        jcacheConfiguration = Eh107Configuration.fromEhcacheCacheConfiguration(
            CacheConfigurationBuilder.newCacheConfigurationBuilder(Object.class, Object.class,
                ResourcePoolsBuilder.heap(ehcache.getMaxEntries()))
                .withExpiry(ExpiryPolicyBuilder.timeToLiveExpiration(Duration.ofSeconds(ehcache.getTimeToLiveSeconds())))
                .build());
    }

    @Bean
    public HibernatePropertiesCustomizer hibernatePropertiesCustomizer(javax.cache.CacheManager cacheManager) {
        return hibernateProperties -> hibernateProperties.put(ConfigSettings.CACHE_MANAGER, cacheManager);
    }

    @Bean
    public JCacheManagerCustomizer cacheManagerCustomizer() {
        return cm -> {
            createCache(cm, com.ag04.batchui.repository.UserRepository.USERS_BY_LOGIN_CACHE);
            createCache(cm, com.ag04.batchui.repository.UserRepository.USERS_BY_EMAIL_CACHE);
            createCache(cm, com.ag04.batchui.domain.User.class.getName());
            createCache(cm, com.ag04.batchui.domain.Authority.class.getName());
            createCache(cm, com.ag04.batchui.domain.User.class.getName() + ".authorities");
            createCache(cm, com.ag04.batchui.domain.BatchJobInstance.class.getName());
            createCache(cm, com.ag04.batchui.domain.BatchJobExecution.class.getName());
            createCache(cm, com.ag04.batchui.domain.BatchJobExecutionParams.class.getName());
            createCache(cm, com.ag04.batchui.domain.BatchStepExecution.class.getName());
            createCache(cm, com.ag04.batchui.domain.BatchStepExecutionContext.class.getName());
            createCache(cm, com.ag04.batchui.domain.BatchJobExecutionContext.class.getName());
            createCache(cm, com.ag04.batchui.domain.BatchJobInstance.class.getName() + ".batchJobExecutions");
            createCache(cm, com.ag04.batchui.domain.BatchJobExecution.class.getName() + ".batchStepExecutions");
            createCache(cm, com.ag04.batchui.domain.BatchStepExecution.class.getName());
            createCache(cm, com.ag04.batchui.dbqueue.domain.StartJobCommand.class.getName());
            // jhipster-needle-ehcache-add-entry
        };
    }

    private void createCache(javax.cache.CacheManager cm, String cacheName) {
        javax.cache.Cache<Object, Object> cache = cm.getCache(cacheName);
        if (cache == null) {
            cm.createCache(cacheName, jcacheConfiguration);
        }
    }

}
