package com.ag04.batchui.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;

/**
 * This Listener iterates through all StepExecutions and check if any has finished with the configured target ExitStatus.<br/>
 * If this is the case it sets the overall JobExecution ExitStatus to the configured targetStatus.
 *
 */
public class ExitStatusJobExecutionListener extends JobExecutionListenerSupport {
    private final Logger log = LoggerFactory.getLogger(ExitStatusJobExecutionListener.class);

    private ExitStatus targetStatus;

    public ExitStatusJobExecutionListener(ExitStatus targetStatus) {
        this.targetStatus = targetStatus;
    }

    @Override
    public void afterJob(JobExecution jobExecution) {
        for (StepExecution stepExecution : jobExecution.getStepExecutions()) {
            log.debug("Step '{}' completed with exitCode={}", stepExecution.getStepName(), stepExecution.getExitStatus().getExitCode());
            if (targetStatus.equals(stepExecution.getExitStatus())) {
                jobExecution.setExitStatus(targetStatus);
                log.info("--> Changed Job Exit status to '{}' based on the ExitStatus of the step='{}'", targetStatus.getExitCode(), stepExecution.getStepName());
                break;
            }
        }
    }
}
