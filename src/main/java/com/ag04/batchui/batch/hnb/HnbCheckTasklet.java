package com.ag04.batchui.batch.hnb;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepContribution;
import org.springframework.batch.core.scope.context.ChunkContext;
import org.springframework.batch.core.step.tasklet.Tasklet;
import org.springframework.batch.repeat.RepeatStatus;
import org.springframework.stereotype.Component;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Random;

@Component
public class HnbCheckTasklet implements Tasklet {
    private final Logger log = LoggerFactory.getLogger(com.ag04.batchui.batch.hnb.HnbCheckTasklet.class);
    private static final String STRING_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss";

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern(STRING_DATE_FORMAT);

    @Override
    public RepeatStatus execute(StepContribution contribution, ChunkContext chunkContext) throws Exception {
        LocalDateTime targetDate = ((Date) chunkContext.getStepContext().getJobParameters().get("launchDate")).toInstant()
                .atZone(ZoneId.systemDefault())
                .toLocalDateTime();
        //String launchDate = (String) chunkContext.getStepContext().getJobParameters().get("launchDate");
        log.info("INVOKED --> Hnb synchronization check for launchDate={}", formatter.format(targetDate));
        int result = new Random().nextInt(2);
        if (result > 0) {
            contribution.setExitStatus(ExitStatus.COMPLETED);
        } else {
            contribution.setExitStatus(ExitStatus.STOPPED);
        }
        return RepeatStatus.FINISHED;
    }

}
