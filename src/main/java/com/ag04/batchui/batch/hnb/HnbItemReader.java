package com.ag04.batchui.batch.hnb;

import com.ag04.batchui.service.dto.ExchangeRateDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class HnbItemReader implements ItemReader<ExchangeRateDTO> {
    private final Logger log = LoggerFactory.getLogger(com.ag04.batchui.batch.hnb.HnbItemReader.class);

    SimpleDateFormat jobLaunchSdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

    private RestTemplate restTemplate;
    private String hnbUrl;
    private LocalDate launchDate;

    List<ExchangeRateDTO> rates = null;
    int ndx = 0;

    public HnbItemReader(RestTemplate restTemplate, String hnbUrl, LocalDate launchDate) {
        this.restTemplate = restTemplate;
        this.hnbUrl = hnbUrl;
        this.launchDate = launchDate;
    }

    @Override
    public ExchangeRateDTO read() throws UnexpectedInputException, ParseException, NonTransientResourceException {
        if (rates == null) {
            readExchangeRates();
        }
        if (ndx >= rates.size()) {
            return null;
        }
        ExchangeRateDTO exRate = rates.get(ndx);
        ndx++;
        return exRate;
    }

    private void readExchangeRates() {
        ndx = 0;
        String formattedDate = launchDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        UriComponentsBuilder builder = UriComponentsBuilder
                .fromUriString(hnbUrl)
                .queryParam("datum", formattedDate);

        String url = builder.toUriString();
        log.debug("--> Invoking HNB exchangeRate Rest API at url={}", url);
        ResponseEntity<List<ExchangeRateDTO>> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<ExchangeRateDTO>>(){}
        );
        rates = response.getBody();
        for (ExchangeRateDTO dto : rates) {
            dto.setDate(launchDate);
        }
        log.info("Got list of rates from HNB API : size = {}", rates.size());
    }
}
