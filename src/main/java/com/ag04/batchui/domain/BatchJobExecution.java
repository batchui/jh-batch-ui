package com.ag04.batchui.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A BatchJobExecution.
 */
@Entity
@Table(name = "batch_job_execution")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class BatchJobExecution implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "jobExecutionSequenceGenerator")
    @SequenceGenerator(name = "jobExecutionSequenceGenerator", sequenceName = "BATCH_JOB_EXECUTION_SEQ", allocationSize = 1)
    @Column(name = "job_execution_id")
    private Long id;

    @Column(name = "version")
    private Long version;

    @NotNull
    @Column(name = "create_time", nullable = false)
    private Instant createTime;

    @NotNull
    @Column(name = "start_time", nullable = false)
    private Instant startTime;

    @NotNull
    @Column(name = "end_time", nullable = false)
    private Instant endTime;

    @Size(max = 15)
    @Column(name = "status", length = 15)
    private String status;

    @Size(max = 15)
    @Column(name = "exit_code", length = 15)
    private String exitCode;

    @Size(max = 2500)
    @Column(name = "exit_message", length = 2500)
    private String exitMessage;

    @Column(name = "last_updated")
    private Instant lastUpdated;

    @Size(max = 2500)
    @Column(name = "job_configuration_location", length = 2500)
    private String jobConfigurationLocation;

    @OneToMany(mappedBy = "batchJobExecution", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<BatchStepExecution> batchStepExecutions = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "job_instance_id")
    @JsonIgnoreProperties("batchJobExecutions")
    private BatchJobInstance batchJobInstance;

    @ElementCollection
    @CollectionTable(
            name="batch_job_execution_params",
            joinColumns=@JoinColumn(name="job_execution_id")
    )
    private List<BatchJobExecutionParams> batchJobExecutionParams = new ArrayList<>();

    @ElementCollection
    @CollectionTable(
            name="batch_job_execution_context",
            joinColumns=@JoinColumn(name="job_execution_id")
    )
    private List<BatchJobExecutionContext> batchJobExecutionContexts = new ArrayList<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public BatchJobExecution version(Long version) {
        this.version = version;
        return this;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Instant getCreateTime() {
        return createTime;
    }

    public BatchJobExecution createTime(Instant createTime) {
        this.createTime = createTime;
        return this;
    }

    public void setCreateTime(Instant createTime) {
        this.createTime = createTime;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public BatchJobExecution startTime(Instant startTime) {
        this.startTime = startTime;
        return this;
    }

    public void setStartTime(Instant startTime) {
        this.startTime = startTime;
    }

    public Instant getEndTime() {
        return endTime;
    }

    public BatchJobExecution endTime(Instant endTime) {
        this.endTime = endTime;
        return this;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }

    public String getStatus() {
        return status;
    }

    public BatchJobExecution status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getExitCode() {
        return exitCode;
    }

    public BatchJobExecution exitCode(String exitCode) {
        this.exitCode = exitCode;
        return this;
    }

    public void setExitCode(String exitCode) {
        this.exitCode = exitCode;
    }

    public String getExitMessage() {
        return exitMessage;
    }

    public BatchJobExecution exitMessage(String exitMessage) {
        this.exitMessage = exitMessage;
        return this;
    }

    public void setExitMessage(String exitMessage) {
        this.exitMessage = exitMessage;
    }

    public Instant getLastUpdated() {
        return lastUpdated;
    }

    public BatchJobExecution lastUpdated(Instant lastUpdated) {
        this.lastUpdated = lastUpdated;
        return this;
    }

    public void setLastUpdated(Instant lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getJobConfigurationLocation() {
        return jobConfigurationLocation;
    }

    public BatchJobExecution jobConfigurationLocation(String jobConfigurationLocation) {
        this.jobConfigurationLocation = jobConfigurationLocation;
        return this;
    }

    public void setJobConfigurationLocation(String jobConfigurationLocation) {
        this.jobConfigurationLocation = jobConfigurationLocation;
    }

    public Set<BatchStepExecution> getBatchStepExecutions() {
        return batchStepExecutions;
    }

    public BatchJobExecution batchStepExecutions(Set<BatchStepExecution> batchStepExecutions) {
        this.batchStepExecutions = batchStepExecutions;
        return this;
    }

    public BatchJobExecution addBatchStepExecution(BatchStepExecution batchStepExecution) {
        this.batchStepExecutions.add(batchStepExecution);
        batchStepExecution.setBatchJobExecution(this);
        return this;
    }

    public BatchJobExecution removeBatchStepExecution(BatchStepExecution batchStepExecution) {
        this.batchStepExecutions.remove(batchStepExecution);
        batchStepExecution.setBatchJobExecution(null);
        return this;
    }

    public void setBatchStepExecutions(Set<BatchStepExecution> batchStepExecutions) {
        this.batchStepExecutions = batchStepExecutions;
    }

    public BatchJobInstance getBatchJobInstance() {
        return batchJobInstance;
    }

    public BatchJobExecution batchJobInstance(BatchJobInstance batchJobInstance) {
        this.batchJobInstance = batchJobInstance;
        return this;
    }

    public void setBatchJobInstance(BatchJobInstance batchJobInstance) {
        this.batchJobInstance = batchJobInstance;
    }

    public BatchJobExecution batchJobExecutionParams(BatchJobExecutionParams params) {
        this.batchJobExecutionParams.add(params);
        return this;
    }

    public List<BatchJobExecutionParams> getBatchJobExecutionParams() {
        return batchJobExecutionParams;
    }

    public void setBatchJobExecutionParams(List<BatchJobExecutionParams> batchJobExecutionParams) {
        this.batchJobExecutionParams = batchJobExecutionParams;
    }

    public BatchJobExecution batchJobExecutionContexts(BatchJobExecutionContext context) {
        this.batchJobExecutionContexts.add(context);
        return this;
    }

    public List<BatchJobExecutionContext> getBatchJobExecutionContexts() {
        return batchJobExecutionContexts;
    }

    public void setBatchJobExecutionContexts(List<BatchJobExecutionContext> batchJobExecutionContexts) {
        this.batchJobExecutionContexts = batchJobExecutionContexts;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BatchJobExecution)) {
            return false;
        }
        return id != null && id.equals(((BatchJobExecution) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "BatchJobExecution{" +
            "id=" + getId() +
            ", version=" + getVersion() +
            ", createTime='" + getCreateTime() + "'" +
            ", startTime='" + getStartTime() + "'" +
            ", endTime='" + getEndTime() + "'" +
            ", status='" + getStatus() + "'" +
            ", exitCode='" + getExitCode() + "'" +
            ", exitMessage='" + getExitMessage() + "'" +
            ", lastUpdated='" + getLastUpdated() + "'" +
            ", jobConfigurationLocation='" + getJobConfigurationLocation() + "'" +
            "}";
    }
}
