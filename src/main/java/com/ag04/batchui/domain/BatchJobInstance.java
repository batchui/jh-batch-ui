package com.ag04.batchui.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;
import java.util.HashSet;
import java.util.Set;

/**
 * A BatchJobInstance.
 */
@Entity
@Table(name = "batch_job_instance")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class BatchJobInstance implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "jobInstanceSequenceGenerator")
    @SequenceGenerator(name = "jobInstanceSequenceGenerator", sequenceName = "BATCH_JOB_SEQ", allocationSize = 1)
    @Column(name = "job_instance_id", nullable = false)
    private Long id;

    @Column(name = "version")
    private Long version;

    @NotNull
    @Size(max = 100)
    @Column(name = "job_name", length = 100, nullable = false)
    private String jobName;

    @NotNull
    @Size(max = 32)
    @Column(name = "job_key", length = 32, nullable = false)
    private String jobKey;

    @OneToMany(mappedBy = "batchJobInstance", fetch = FetchType.LAZY, cascade = CascadeType.ALL, orphanRemoval = true)
    @Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
    private Set<BatchJobExecution> batchJobExecutions = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public BatchJobInstance version(Long version) {
        this.version = version;
        return this;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getJobName() {
        return jobName;
    }

    public BatchJobInstance jobName(String jobName) {
        this.jobName = jobName;
        return this;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobKey() {
        return jobKey;
    }

    public BatchJobInstance jobKey(String jobKey) {
        this.jobKey = jobKey;
        return this;
    }

    public void setJobKey(String jobKey) {
        this.jobKey = jobKey;
    }

    public Set<BatchJobExecution> getBatchJobExecutions() {
        return batchJobExecutions;
    }

    public BatchJobInstance batchJobExecutions(Set<BatchJobExecution> batchJobExecutions) {
        this.batchJobExecutions = batchJobExecutions;
        return this;
    }

    public BatchJobInstance addBatchJobExecution(BatchJobExecution batchJobExecution) {
        this.batchJobExecutions.add(batchJobExecution);
        batchJobExecution.setBatchJobInstance(this);
        return this;
    }

    public BatchJobInstance removeBatchJobExecution(BatchJobExecution batchJobExecution) {
        this.batchJobExecutions.remove(batchJobExecution);
        batchJobExecution.setBatchJobInstance(null);
        return this;
    }

    public void setBatchJobExecutions(Set<BatchJobExecution> batchJobExecutions) {
        this.batchJobExecutions = batchJobExecutions;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BatchJobInstance)) {
            return false;
        }
        return id != null && id.equals(((BatchJobInstance) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "BatchJobInstance{" +
            "id=" + getId() +
            ", version=" + getVersion() +
            ", jobName='" + getJobName() + "'" +
            ", jobKey='" + getJobKey() + "'" +
            "}";
    }
}
