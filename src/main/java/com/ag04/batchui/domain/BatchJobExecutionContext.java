package com.ag04.batchui.domain;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A BatchJobExecutionContext.
 */
@Embeddable
public class BatchJobExecutionContext implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotNull
    @Size(max = 2500)
    @Column(name = "short_context", length = 2500, nullable = false)
    private String shortContext;

    @Lob
    @Type(type = "org.hibernate.type.TextType")
    @Column(name = "serialized_context")
    private String serializedContext;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public String getShortContext() {
        return shortContext;
    }

    public BatchJobExecutionContext shortContext(String shortContext) {
        this.shortContext = shortContext;
        return this;
    }

    public void setShortContext(String shortContext) {
        this.shortContext = shortContext;
    }

    public String getSerializedContext() {
        return serializedContext;
    }

    public BatchJobExecutionContext serializedContext(String serializedContext) {
        this.serializedContext = serializedContext;
        return this;
    }

    public void setSerializedContext(String serializedContext) {
        this.serializedContext = serializedContext;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "BatchJobExecutionContext{" +
            "shortContext='" + getShortContext() + "'" +
            ", serializedContext='" + getSerializedContext() + "'" +
            "}";
    }
}
