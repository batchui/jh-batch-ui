package com.ag04.batchui.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.time.ZonedDateTime;

/**
 * A BatchStepExecution.
 */
@Entity
@Table(name = "batch_step_execution")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class BatchStepExecution implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "batchStepExecutionSequenceGenerator")
    @SequenceGenerator(name = "batchStepExecutionSequenceGenerator", sequenceName = "BATCH_STEP_EXECUTION_SEQ", allocationSize = 1)
    @Column(name = "step_execution_id", nullable = false)
    private Long id;

    @NotNull
    @Column(name = "version", nullable = false)
    private Long version;

    @NotNull
    @Size(max = 100)
    @Column(name = "step_name", length = 100, nullable = false)
    private String stepName;

    @NotNull
    @Column(name = "start_time", nullable = false)
    private ZonedDateTime startTime;

    @NotNull
    @Column(name = "end_time", nullable = false)
    private ZonedDateTime endTime;

    @Size(max = 10)
    @Column(name = "status", length = 10)
    private String status;

    @Column(name = "commit_count")
    private Long commitCount;

    @Column(name = "read_count")
    private Long readCount;

    @Column(name = "filter_count")
    private Long filterCount;

    @Column(name = "write_count")
    private Long writeCount;

    @Column(name = "read_skip_count")
    private Long readSkipCount;

    @Column(name = "write_skip_count")
    private Long writeSkipCount;

    @Column(name = "process_skip_count")
    private Long processSkipCount;

    @Column(name = "rollback_count")
    private Long rollbackCount;

    @Size(max = 2500)
    @Column(name = "exit_code", length = 2500)
    private String exitCode;

    @Size(max = 2500)
    @Column(name = "exit_message", length = 2500)
    private String exitMessage;

    @Column(name = "last_updated")
    private ZonedDateTime lastUpdated;

    @ManyToOne
    @JoinColumn(name = "job_execution_id")
    @JsonIgnoreProperties("batchStepExecutions")
    private BatchJobExecution batchJobExecution;

    @ElementCollection
    @CollectionTable(
            name="batch_step_execution_context",
            joinColumns=@JoinColumn(name="step_execution_id")
    )
    private List<BatchStepExecutionContext> batchStepExecutionContexts = new ArrayList<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public BatchStepExecution version(Long version) {
        this.version = version;
        return this;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getStepName() {
        return stepName;
    }

    public BatchStepExecution stepName(String stepName) {
        this.stepName = stepName;
        return this;
    }

    public void setStepName(String stepName) {
        this.stepName = stepName;
    }

    public ZonedDateTime getStartTime() {
        return startTime;
    }

    public BatchStepExecution startTime(ZonedDateTime startTime) {
        this.startTime = startTime;
        return this;
    }

    public void setStartTime(ZonedDateTime startTime) {
        this.startTime = startTime;
    }

    public ZonedDateTime getEndTime() {
        return endTime;
    }

    public BatchStepExecution endTime(ZonedDateTime endTime) {
        this.endTime = endTime;
        return this;
    }

    public void setEndTime(ZonedDateTime endTime) {
        this.endTime = endTime;
    }

    public String getStatus() {
        return status;
    }

    public BatchStepExecution status(String status) {
        this.status = status;
        return this;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getCommitCount() {
        return commitCount;
    }

    public BatchStepExecution commitCount(Long commitCount) {
        this.commitCount = commitCount;
        return this;
    }

    public void setCommitCount(Long commitCount) {
        this.commitCount = commitCount;
    }

    public Long getReadCount() {
        return readCount;
    }

    public BatchStepExecution readCount(Long readCount) {
        this.readCount = readCount;
        return this;
    }

    public void setReadCount(Long readCount) {
        this.readCount = readCount;
    }

    public Long getFilterCount() {
        return filterCount;
    }

    public BatchStepExecution filterCount(Long filterCount) {
        this.filterCount = filterCount;
        return this;
    }

    public void setFilterCount(Long filterCount) {
        this.filterCount = filterCount;
    }

    public Long getWriteCount() {
        return writeCount;
    }

    public BatchStepExecution writeCount(Long writeCount) {
        this.writeCount = writeCount;
        return this;
    }

    public void setWriteCount(Long writeCount) {
        this.writeCount = writeCount;
    }

    public Long getReadSkipCount() {
        return readSkipCount;
    }

    public BatchStepExecution readSkipCount(Long readSkipCount) {
        this.readSkipCount = readSkipCount;
        return this;
    }

    public void setReadSkipCount(Long readSkipCount) {
        this.readSkipCount = readSkipCount;
    }

    public Long getWriteSkipCount() {
        return writeSkipCount;
    }

    public BatchStepExecution writeSkipCount(Long writeSkipCount) {
        this.writeSkipCount = writeSkipCount;
        return this;
    }

    public void setWriteSkipCount(Long writeSkipCount) {
        this.writeSkipCount = writeSkipCount;
    }

    public Long getProcessSkipCount() {
        return processSkipCount;
    }

    public BatchStepExecution processSkipCount(Long processSkipCount) {
        this.processSkipCount = processSkipCount;
        return this;
    }

    public void setProcessSkipCount(Long processSkipCount) {
        this.processSkipCount = processSkipCount;
    }

    public Long getRollbackCount() {
        return rollbackCount;
    }

    public BatchStepExecution rollbackCount(Long rollbackCount) {
        this.rollbackCount = rollbackCount;
        return this;
    }

    public void setRollbackCount(Long rollbackCount) {
        this.rollbackCount = rollbackCount;
    }

    public String getExitCode() {
        return exitCode;
    }

    public BatchStepExecution exitCode(String exitCode) {
        this.exitCode = exitCode;
        return this;
    }

    public void setExitCode(String exitCode) {
        this.exitCode = exitCode;
    }

    public String getExitMessage() {
        return exitMessage;
    }

    public BatchStepExecution exitMessage(String exitMessage) {
        this.exitMessage = exitMessage;
        return this;
    }

    public void setExitMessage(String exitMessage) {
        this.exitMessage = exitMessage;
    }

    public ZonedDateTime getLastUpdated() {
        return lastUpdated;
    }

    public BatchStepExecution lastUpdated(ZonedDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
        return this;
    }

    public void setLastUpdated(ZonedDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public BatchJobExecution getBatchJobExecution() {
        return batchJobExecution;
    }

    public BatchStepExecution batchJobExecution(BatchJobExecution batchJobExecution) {
        this.batchJobExecution = batchJobExecution;
        return this;
    }

    public void setBatchJobExecution(BatchJobExecution batchJobExecution) {
        this.batchJobExecution = batchJobExecution;
    }

    public List<BatchStepExecutionContext> getBatchStepExecutionContexts() {
        return batchStepExecutionContexts;
    }

    public void setBatchStepExecutionContexts(List<BatchStepExecutionContext> batchStepExecutionContexts) {
        this.batchStepExecutionContexts = batchStepExecutionContexts;
    }

    public BatchStepExecution batchStepExecutionContexts(BatchStepExecutionContext context) {
        this.batchStepExecutionContexts.add(context);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof BatchStepExecution)) {
            return false;
        }
        return id != null && id.equals(((BatchStepExecution) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "BatchStepExecution{" +
            "id=" + getId() +
            ", version=" + getVersion() +
            ", stepName='" + getStepName() + "'" +
            ", startTime='" + getStartTime() + "'" +
            ", endTime='" + getEndTime() + "'" +
            ", status='" + getStatus() + "'" +
            ", commitCount=" + getCommitCount() +
            ", readCount=" + getReadCount() +
            ", filterCount=" + getFilterCount() +
            ", writeCount=" + getWriteCount() +
            ", readSkipCount=" + getReadSkipCount() +
            ", writeSkipCount=" + getWriteSkipCount() +
            ", processSkipCount=" + getProcessSkipCount() +
            ", rollbackCount=" + getRollbackCount() +
            ", exitCode='" + getExitCode() + "'" +
            ", exitMessage='" + getExitMessage() + "'" +
            ", lastUpdated='" + getLastUpdated() + "'" +
            "}";
    }
}
