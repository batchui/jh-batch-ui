package com.ag04.batchui.domain;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.ZonedDateTime;

/**
 * A BatchJobExecutionParams.
 */
@Embeddable
public class BatchJobExecutionParams implements Serializable {
    private static final long serialVersionUID = 1L;

    @NotNull
    @Size(max = 6)
    @Column(name = "type_cd", length = 6, nullable = false)
    private String typeCd;

    @NotNull
    @Size(max = 100)
    @Column(name = "key_name", length = 100, nullable = false)
    private String keyName;

    @Size(max = 250)
    @Column(name = "string_val", length = 250)
    private String stringVal;

    @Column(name = "date_val")
    private ZonedDateTime dateVal;

    @Column(name = "long_val")
    private Long longVal;

    @Column(name = "double_val")
    private Double doubleVal;

    @NotNull
    @Column(name = "identifying", length = 1, nullable = false)
    private Character identifying;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove

    public String getTypeCd() {
        return typeCd;
    }

    public BatchJobExecutionParams typeCd(String typeCd) {
        this.typeCd = typeCd;
        return this;
    }

    public void setTypeCd(String typeCd) {
        this.typeCd = typeCd;
    }

    public String getKeyName() {
        return keyName;
    }

    public BatchJobExecutionParams keyName(String keyName) {
        this.keyName = keyName;
        return this;
    }

    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }

    public String getStringVal() {
        return stringVal;
    }

    public BatchJobExecutionParams stringVal(String stringVal) {
        this.stringVal = stringVal;
        return this;
    }

    public void setStringVal(String stringVal) {
        this.stringVal = stringVal;
    }

    public ZonedDateTime getDateVal() {
        return dateVal;
    }

    public BatchJobExecutionParams dateVal(ZonedDateTime dateVal) {
        this.dateVal = dateVal;
        return this;
    }

    public void setDateVal(ZonedDateTime dateVal) {
        this.dateVal = dateVal;
    }

    public Long getLongVal() {
        return longVal;
    }

    public BatchJobExecutionParams longVal(Long longVal) {
        this.longVal = longVal;
        return this;
    }

    public void setLongVal(Long longVal) {
        this.longVal = longVal;
    }

    public Double getDoubleVal() {
        return doubleVal;
    }

    public BatchJobExecutionParams doubleVal(Double doubleVal) {
        this.doubleVal = doubleVal;
        return this;
    }

    public void setDoubleVal(Double doubleVal) {
        this.doubleVal = doubleVal;
    }

    public Character getIdentifying() {
        return identifying;
    }

    public BatchJobExecutionParams identifying(Character identifying) {
        this.identifying = identifying;
        return this;
    }

    public void setIdentifying(Character identifying) {
        this.identifying = identifying;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "BatchJobExecutionParams{" +
            "typeCd='" + getTypeCd() + "'" +
            ", keyName='" + getKeyName() + "'" +
            ", stringVal='" + getStringVal() + "'" +
            ", dateVal='" + getDateVal() + "'" +
            ", longVal=" + getLongVal() +
            ", doubleVal=" + getDoubleVal() +
            ", identifying='" + getIdentifying() + "'" +
            "}";
    }
}
