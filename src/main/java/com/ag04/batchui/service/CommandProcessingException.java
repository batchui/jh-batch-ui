package com.ag04.batchui.service;

public class CommandProcessingException extends RuntimeException {

    public CommandProcessingException(String message) {
        super(message);
    }

}
