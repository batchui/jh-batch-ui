package com.ag04.batchui.service;

import com.ag04.batchui.service.dto.BatchStepExecutionContextDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.ag04.batchui.domain.BatchStepExecutionContext}.
 */
public interface BatchStepExecutionContextService {

    /**
     * Save a batchStepExecutionContext.
     *
     * @param batchStepExecutionContextDTO the entity to save.
     * @return the persisted entity.
     */
    BatchStepExecutionContextDTO save(BatchStepExecutionContextDTO batchStepExecutionContextDTO);

    /**
     * Get all the batchStepExecutionContexts.
     *
     * @return the list of entities.
     */
    List<BatchStepExecutionContextDTO> findAll();

    /**
     * Get the "id" batchStepExecutionContext.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BatchStepExecutionContextDTO> findOne(Long id);

    /**
     * Delete the "id" batchStepExecutionContext.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
