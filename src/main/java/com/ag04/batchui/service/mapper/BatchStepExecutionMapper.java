package com.ag04.batchui.service.mapper;


import com.ag04.batchui.domain.*;
import com.ag04.batchui.service.dto.BatchStepExecutionDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link BatchStepExecution} and its DTO {@link BatchStepExecutionDTO}.
 */
@Mapper(componentModel = "spring", uses = {BatchStepExecutionContextMapper.class})
public interface BatchStepExecutionMapper extends EntityMapper<BatchStepExecutionDTO, BatchStepExecution> {

    @Mapping(source = "batchJobExecution.id", target = "batchJobExecutionId")
    BatchStepExecutionDTO toDto(BatchStepExecution batchStepExecution);

    @Mapping(source = "batchJobExecutionId", target = "batchJobExecution.id")
    BatchStepExecution toEntity(BatchStepExecutionDTO batchStepExecutionDTO);

    default BatchStepExecution fromId(Long id) {
        if (id == null) {
            return null;
        }
        BatchStepExecution batchStepExecution = new BatchStepExecution();
        batchStepExecution.setId(id);
        return batchStepExecution;
    }
}
