package com.ag04.batchui.service.mapper;

import com.ag04.batchui.domain.*;
import com.ag04.batchui.service.dto.BatchJobExecutionDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link BatchJobExecution} and its DTO {@link BatchJobExecutionDTO}.
 */
@Mapper(componentModel = "spring", uses = { BatchStepExecutionMapper.class, BatchJobExecutionParamsMapper.class, BatchJobExecutionContextMapper.class })
public interface BatchJobExecutionMapper extends EntityMapper<BatchJobExecutionDTO, BatchJobExecution> {

    @Mapping(source = "batchJobInstance.id", target = "batchJobInstanceId")
    @Mapping(source = "batchJobInstance.jobName", target = "jobName")
    BatchJobExecutionDTO toDto(BatchJobExecution batchJobExecution);

    @Mapping(target = "batchStepExecutions", ignore = true)
    @Mapping(target = "removeBatchStepExecution", ignore = true)
    @Mapping(target = "batchJobExecutionContexts", ignore = true)
    @Mapping(source="batchJobInstanceId" , target = "batchJobInstance.id")
    BatchJobExecution toEntity(BatchJobExecutionDTO batchJobExecutionDTO);

    default BatchJobExecution fromId(Long id) {
        if (id == null) {
            return null;
        }
        BatchJobExecution batchJobExecution = new BatchJobExecution();
        batchJobExecution.setId(id);
        return batchJobExecution;
    }
}
