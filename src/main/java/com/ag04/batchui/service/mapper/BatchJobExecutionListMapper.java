package com.ag04.batchui.service.mapper;

import com.ag04.batchui.domain.BatchJobExecution;
import com.ag04.batchui.service.dto.BatchJobExecutionDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface BatchJobExecutionListMapper extends EntityMapper<BatchJobExecutionDTO, BatchJobExecution> {

    @Override
    @Mapping(source = "batchJobInstance.id", target = "batchJobInstanceId")
    @Mapping(source = "batchJobInstance.jobName", target = "jobName")
    @Mapping(target = "batchStepExecutions", ignore = true)
    @Mapping(target = "batchJobExecutionParams", ignore = true)
    @Mapping(target = "batchJobExecutionContexts", ignore = true)
    BatchJobExecutionDTO toDto(BatchJobExecution batchJobExecution);

}
