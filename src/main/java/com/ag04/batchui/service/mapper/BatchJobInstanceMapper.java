package com.ag04.batchui.service.mapper;


import com.ag04.batchui.domain.*;
import com.ag04.batchui.service.dto.BatchJobInstanceDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link BatchJobInstance} and its DTO {@link BatchJobInstanceDTO}.
 */
@Mapper(componentModel = "spring", uses = {BatchJobExecutionMapper.class})
public interface BatchJobInstanceMapper extends EntityMapper<BatchJobInstanceDTO, BatchJobInstance> {

    @Mapping(target = "batchJobExecutions", ignore = true)
    @Mapping(target = "removeBatchJobExecution", ignore = true)
    BatchJobInstance toEntity(BatchJobInstanceDTO batchJobInstanceDTO);

    default BatchJobInstance fromId(Long id) {
        if (id == null) {
            return null;
        }
        BatchJobInstance batchJobInstance = new BatchJobInstance();
        batchJobInstance.setId(id);
        return batchJobInstance;
    }
}
