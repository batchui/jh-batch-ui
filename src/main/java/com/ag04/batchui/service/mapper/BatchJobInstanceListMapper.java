package com.ag04.batchui.service.mapper;

import com.ag04.batchui.domain.BatchJobInstance;
import com.ag04.batchui.service.dto.BatchJobInstanceDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

/**
 * Mapper for lists of BatchJobInstance entities.
 *
 */
@Mapper(componentModel = "spring")
public interface BatchJobInstanceListMapper extends EntityMapper<BatchJobInstanceDTO, BatchJobInstance> {

    @Override
    @Mapping(target = "batchJobExecutions", ignore = true)
    BatchJobInstanceDTO toDto(BatchJobInstance batchJobInstance);

}
