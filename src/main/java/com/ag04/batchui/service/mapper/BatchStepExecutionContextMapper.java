package com.ag04.batchui.service.mapper;


import com.ag04.batchui.domain.*;
import com.ag04.batchui.service.dto.BatchStepExecutionContextDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link BatchStepExecutionContext} and its DTO {@link BatchStepExecutionContextDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BatchStepExecutionContextMapper extends EntityMapper<BatchStepExecutionContextDTO, BatchStepExecutionContext> {

}
