package com.ag04.batchui.service.mapper;


import com.ag04.batchui.domain.*;
import com.ag04.batchui.service.dto.BatchJobExecutionParamsDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link BatchJobExecutionParams} and its DTO {@link BatchJobExecutionParamsDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BatchJobExecutionParamsMapper extends EntityMapper<BatchJobExecutionParamsDTO, BatchJobExecutionParams> {

}
