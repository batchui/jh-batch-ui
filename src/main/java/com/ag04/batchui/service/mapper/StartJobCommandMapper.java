package com.ag04.batchui.service.mapper;

import com.ag04.batchui.dbqueue.domain.StartJobCommand;
import com.ag04.batchui.service.dto.StartJobCommandDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link StartJobCommand} and its DTO {@link StartJobCommandDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface StartJobCommandMapper extends EntityMapper<StartJobCommandDTO, StartJobCommand> {

    //StartJobCommand toEntity(StartJobCommandDTO dto);

    default StartJobCommand fromId(Long id) {
        if (id == null) {
            return null;
        }
        StartJobCommand startJobCommand = new StartJobCommand();
        startJobCommand.setId(id);
        return startJobCommand;
    }
}
