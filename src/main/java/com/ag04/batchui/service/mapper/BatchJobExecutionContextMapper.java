package com.ag04.batchui.service.mapper;


import com.ag04.batchui.domain.*;
import com.ag04.batchui.service.dto.BatchJobExecutionContextDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link BatchJobExecutionContext} and its DTO {@link BatchJobExecutionContextDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface BatchJobExecutionContextMapper extends EntityMapper<BatchJobExecutionContextDTO, BatchJobExecutionContext> {

}
