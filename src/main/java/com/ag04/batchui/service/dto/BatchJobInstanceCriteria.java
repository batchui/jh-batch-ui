package com.ag04.batchui.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;

/**
 * Criteria class for the {@link com.ag04.batchui.domain.BatchJobInstance} entity. This class is used
 * in {@link com.ag04.batchui.web.rest.BatchJobInstanceResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /batch-job-instances?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class BatchJobInstanceCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter jobInstanceId;

    private LongFilter version;

    private StringFilter jobName;

    private StringFilter jobKey;

    private LongFilter batchJobExecutionId;

    public BatchJobInstanceCriteria() {
    }

    public BatchJobInstanceCriteria(BatchJobInstanceCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.jobInstanceId = other.jobInstanceId == null ? null : other.jobInstanceId.copy();
        this.version = other.version == null ? null : other.version.copy();
        this.jobName = other.jobName == null ? null : other.jobName.copy();
        this.jobKey = other.jobKey == null ? null : other.jobKey.copy();
        this.batchJobExecutionId = other.batchJobExecutionId == null ? null : other.batchJobExecutionId.copy();
    }

    @Override
    public BatchJobInstanceCriteria copy() {
        return new BatchJobInstanceCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getJobInstanceId() {
        return jobInstanceId;
    }

    public void setJobInstanceId(LongFilter jobInstanceId) {
        this.jobInstanceId = jobInstanceId;
    }

    public LongFilter getVersion() {
        return version;
    }

    public void setVersion(LongFilter version) {
        this.version = version;
    }

    public StringFilter getJobName() {
        return jobName;
    }

    public void setJobName(StringFilter jobName) {
        this.jobName = jobName;
    }

    public StringFilter getJobKey() {
        return jobKey;
    }

    public void setJobKey(StringFilter jobKey) {
        this.jobKey = jobKey;
    }

    public LongFilter getBatchJobExecutionId() {
        return batchJobExecutionId;
    }

    public void setBatchJobExecutionId(LongFilter batchJobExecutionId) {
        this.batchJobExecutionId = batchJobExecutionId;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final BatchJobInstanceCriteria that = (BatchJobInstanceCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(jobInstanceId, that.jobInstanceId) &&
            Objects.equals(version, that.version) &&
            Objects.equals(jobName, that.jobName) &&
            Objects.equals(jobKey, that.jobKey) &&
            Objects.equals(batchJobExecutionId, that.batchJobExecutionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        jobInstanceId,
        version,
        jobName,
        jobKey,
        batchJobExecutionId
        );
    }

    @Override
    public String toString() {
        return "BatchJobInstanceCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (jobInstanceId != null ? "jobInstanceId=" + jobInstanceId + ", " : "") +
                (version != null ? "version=" + version + ", " : "") +
                (jobName != null ? "jobName=" + jobName + ", " : "") +
                (jobKey != null ? "jobKey=" + jobKey + ", " : "") +
                (batchJobExecutionId != null ? "batchJobExecutionId=" + batchJobExecutionId + ", " : "") +
            "}";
    }

}
