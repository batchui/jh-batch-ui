package com.ag04.batchui.service.dto;

import java.time.LocalDateTime;
import java.util.List;

public class JobExecutionStatusReport {
    List<JobStatusCount> batchStatuses;
    List<JobStatusCount> exitCodes;

    int jobCount;
    LocalDateTime from;
    LocalDateTime to;

    //--- set / get methods ---------------------------------------------------

    public int getJobCount() {
        return jobCount;
    }

    public void setJobCount(int jobCount) {
        this.jobCount = jobCount;
    }

    public List<JobStatusCount> getBatchStatuses() {
        return batchStatuses;
    }

    public void setBatchStatuses(List<JobStatusCount> batchStatuses) {
        this.batchStatuses = batchStatuses;
    }

    public List<JobStatusCount> getExitCodes() {
        return exitCodes;
    }

    public void setExitCodes(List<JobStatusCount> exitCodes) {
        this.exitCodes = exitCodes;
    }

    public LocalDateTime getFrom() {
        return from;
    }

    public void setFrom(LocalDateTime from) {
        this.from = from;
    }

    public LocalDateTime getTo() {
        return to;
    }

    public void setTo(LocalDateTime to) {
        this.to = to;
    }
}
