package com.ag04.batchui.service.dto;

import com.ag04.batchui.dbqueue.domain.QueueStatus;

import java.time.LocalDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the {@link com.ag04.batchui.dbqueue.domain.StartJobCommand} entity.
 */
public class StartJobCommandDTO implements Serializable {
    
    private Long id;

    @NotNull
    @Size(max = 80)
    private String jobName;

    @Lob
    private String jobParams;

    private QueueStatus status;

    private LocalDateTime nextAttemptTime;

    private Integer attemptCount;

    private LocalDateTime lastAttemptTime;

    @Size(max = 500)
    private String lastAttemptErrorMessage;

    //--- set / get methods ---------------------------------------------------

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobParams() {
        return jobParams;
    }

    public void setJobParams(String jobParams) {
        this.jobParams = jobParams;
    }

    public QueueStatus getStatus() {
        return status;
    }

    public void setStatus(QueueStatus status) {
        this.status = status;
    }

    public LocalDateTime getNextAttemptTime() {
        return nextAttemptTime;
    }

    public void setNextAttemptTime(LocalDateTime nextAttemptTime) {
        this.nextAttemptTime = nextAttemptTime;
    }

    public Integer getAttemptCount() {
        return attemptCount;
    }

    public void setAttemptCount(Integer attemptCount) {
        this.attemptCount = attemptCount;
    }

    public LocalDateTime getLastAttemptTime() {
        return lastAttemptTime;
    }

    public void setLastAttemptTime(LocalDateTime lastAttemptTime) {
        this.lastAttemptTime = lastAttemptTime;
    }

    public String getLastAttemptErrorMessage() {
        return lastAttemptErrorMessage;
    }

    public void setLastAttemptErrorMessage(String lastAttemptErrorMessage) {
        this.lastAttemptErrorMessage = lastAttemptErrorMessage;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        StartJobCommandDTO startJobCommandDTO = (StartJobCommandDTO) o;
        if (startJobCommandDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), startJobCommandDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "StartJobCommandDTO{" +
            "id=" + getId() +
            ", jobName='" + getJobName() + "'" +
            ", status='" + getStatus() + "'" +
            ", nextAttemptTime='" + getNextAttemptTime() + "'" +
            ", attemptCount=" + getAttemptCount() +
            ", lastAttemptTime='" + getLastAttemptTime() + "'" +
            ", lastAttemptErrorMessage='" + getLastAttemptErrorMessage() + "'" +
            "}";
    }
}
