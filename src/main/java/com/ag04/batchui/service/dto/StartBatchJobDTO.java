package com.ag04.batchui.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;

public class StartBatchJobDTO implements Serializable {

    @NotNull
    @Size(max = 100)
    private String jobName;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<BatchJobExecutionParamsDTO> batchJobExecutionParams;

    // --- set / get methods --------------------------------------------------

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public List<BatchJobExecutionParamsDTO> getBatchJobExecutionParams() {
        return batchJobExecutionParams;
    }

    public void setBatchJobExecutionParams(List<BatchJobExecutionParamsDTO> batchJobExecutionParams) {
        this.batchJobExecutionParams = batchJobExecutionParams;
    }
}
