package com.ag04.batchui.service.dto;

public class JobStatusCount {
    String status;
    Long count;

    public JobStatusCount(Long count, String status) {
        this.status = status;
        this.count = count;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }
}
