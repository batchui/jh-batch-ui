package com.ag04.batchui.service.dto;

import com.ag04.batchui.domain.BatchJobExecution;
import com.fasterxml.jackson.annotation.JsonInclude;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the {@link com.ag04.batchui.domain.BatchJobInstance} entity.
 */
public class BatchJobInstanceDTO implements Serializable {

    private Long id;

    private Long version;

    @NotNull
    @Size(max = 100)
    private String jobName;

    @NotNull
    @Size(max = 32)
    private String jobKey;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<BatchJobExecutionDTO> batchJobExecutions;

    //--- set / get methods ---------------------------------------------------
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    public String getJobKey() {
        return jobKey;
    }

    public void setJobKey(String jobKey) {
        this.jobKey = jobKey;
    }

    public List<BatchJobExecutionDTO> getBatchJobExecutions() {
        return batchJobExecutions;
    }

    public void setBatchJobExecutions(List<BatchJobExecutionDTO> batchJobExecutions) {
        this.batchJobExecutions = batchJobExecutions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BatchJobInstanceDTO batchJobInstanceDTO = (BatchJobInstanceDTO) o;
        if (batchJobInstanceDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), batchJobInstanceDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BatchJobInstanceDTO{" +
            "id=" + getId() +
            ", version=" + getVersion() +
            ", jobName='" + getJobName() + "'" +
            ", jobKey='" + getJobKey() + "'" +
            "}";
    }
}
