package com.ag04.batchui.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.time.Instant;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the {@link com.ag04.batchui.domain.BatchJobExecution} entity.
 */
public class BatchJobExecutionDTO implements Serializable {

    private Long id;

    private Long version;

    @NotNull
    private Instant createTime;

    @NotNull
    private Instant startTime;

    @NotNull
    private Instant endTime;

    @Size(max = 10)
    private String status;

    @Size(max = 2500)
    private String exitCode;

    @Size(max = 2500)
    private String exitMessage;

    private Instant lastUpdated;

    @Size(max = 2500)
    private String jobConfigurationLocation;

    private Long batchJobInstanceId;

    private String jobName;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<BatchStepExecutionDTO> batchStepExecutions;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<BatchJobExecutionParamsDTO> batchJobExecutionParams;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<BatchJobExecutionContextDTO> batchJobExecutionContexts;

    //--- set / get methods ---------------------------------------------------

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Instant getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Instant createTime) {
        this.createTime = createTime;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public void setStartTime(Instant startTime) {
        this.startTime = startTime;
    }

    public Instant getEndTime() {
        return endTime;
    }

    public void setEndTime(Instant endTime) {
        this.endTime = endTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getExitCode() {
        return exitCode;
    }

    public void setExitCode(String exitCode) {
        this.exitCode = exitCode;
    }

    public String getExitMessage() {
        return exitMessage;
    }

    public void setExitMessage(String exitMessage) {
        this.exitMessage = exitMessage;
    }

    public Instant getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(Instant lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getJobConfigurationLocation() {
        return jobConfigurationLocation;
    }

    public void setJobConfigurationLocation(String jobConfigurationLocation) {
        this.jobConfigurationLocation = jobConfigurationLocation;
    }

    public Long getBatchJobInstanceId() {
        return batchJobInstanceId;
    }

    public void setBatchJobInstanceId(Long batchJobInstanceId) {
        this.batchJobInstanceId = batchJobInstanceId;
    }

    public List<BatchStepExecutionDTO> getBatchStepExecutions() {
        return batchStepExecutions;
    }

    public void setBatchStepExecutions(List<BatchStepExecutionDTO> batchStepExecutions) {
        this.batchStepExecutions = batchStepExecutions;
    }

    public List<BatchJobExecutionParamsDTO> getBatchJobExecutionParams() {
        return batchJobExecutionParams;
    }

    public void setBatchJobExecutionParams(List<BatchJobExecutionParamsDTO> batchJobExecutionParams) {
        this.batchJobExecutionParams = batchJobExecutionParams;
    }

    public List<BatchJobExecutionContextDTO> getBatchJobExecutionContexts() {
        return batchJobExecutionContexts;
    }

    public void setBatchJobExecutionContexts(List<BatchJobExecutionContextDTO> batchJobExecutionContexts) {
        this.batchJobExecutionContexts = batchJobExecutionContexts;
    }

    public String getJobName() {
        return jobName;
    }

    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BatchJobExecutionDTO batchJobExecutionDTO = (BatchJobExecutionDTO) o;
        if (batchJobExecutionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), batchJobExecutionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BatchJobExecutionDTO{" +
            "id=" + getId() +
            ", version=" + getVersion() +
            ", createTime='" + getCreateTime() + "'" +
            ", startTime='" + getStartTime() + "'" +
            ", endTime='" + getEndTime() + "'" +
            ", status='" + getStatus() + "'" +
            ", exitCode='" + getExitCode() + "'" +
            ", exitMessage='" + getExitMessage() + "'" +
            ", lastUpdated='" + getLastUpdated() + "'" +
            ", jobConfigurationLocation='" + getJobConfigurationLocation() + "'" +
            ", batchJobInstanceId=" + getBatchJobInstanceId() +
            "}";
    }
}
