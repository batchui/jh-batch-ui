package com.ag04.batchui.service.dto;

import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;
import javax.persistence.Lob;

/**
 * A DTO for the {@link com.ag04.batchui.domain.BatchJobExecutionContext} entity.
 */
public class BatchJobExecutionContextDTO implements Serializable {

    @NotNull
    @Size(max = 2500)
    private String shortContext;

    @Lob
    private String serializedContext;

    public String getShortContext() {
        return shortContext;
    }

    public void setShortContext(String shortContext) {
        this.shortContext = shortContext;
    }

    public String getSerializedContext() {
        return serializedContext;
    }

    public void setSerializedContext(String serializedContext) {
        this.serializedContext = serializedContext;
    }


    @Override
    public String toString() {
        return "BatchJobExecutionContextDTO{" +
            "shortContext='" + getShortContext() + "'" +
            ", serializedContext='" + getSerializedContext() + "'" +
            "}";
    }
}
