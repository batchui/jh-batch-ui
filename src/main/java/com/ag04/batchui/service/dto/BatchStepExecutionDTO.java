package com.ag04.batchui.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * A DTO for the {@link com.ag04.batchui.domain.BatchStepExecution} entity.
 */
public class BatchStepExecutionDTO implements Serializable {

    @NotNull
    private Long id;

    @NotNull
    private Long version;

    @NotNull
    @Size(max = 100)
    private String stepName;

    @NotNull
    private ZonedDateTime startTime;

    @NotNull
    private ZonedDateTime endTime;

    @Size(max = 10)
    private String status;

    private Long commitCount;

    private Long readCount;

    private Long filterCount;

    private Long writeCount;

    private Long readSkipCount;

    private Long writeSkipCount;

    private Long processSkipCount;

    private Long rollbackCount;

    @Size(max = 2500)
    private String exitCode;

    @Size(max = 2500)
    private String exitMessage;

    private ZonedDateTime lastUpdated;

    private Long batchJobExecutionId;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private List<BatchStepExecutionContextDTO> BatchStepExecutionContexts;

    //--- set / get methods ---------------------------------------------------

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public String getStepName() {
        return stepName;
    }

    public void setStepName(String stepName) {
        this.stepName = stepName;
    }

    public ZonedDateTime getStartTime() {
        return startTime;
    }

    public void setStartTime(ZonedDateTime startTime) {
        this.startTime = startTime;
    }

    public ZonedDateTime getEndTime() {
        return endTime;
    }

    public void setEndTime(ZonedDateTime endTime) {
        this.endTime = endTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getCommitCount() {
        return commitCount;
    }

    public void setCommitCount(Long commitCount) {
        this.commitCount = commitCount;
    }

    public Long getReadCount() {
        return readCount;
    }

    public void setReadCount(Long readCount) {
        this.readCount = readCount;
    }

    public Long getFilterCount() {
        return filterCount;
    }

    public void setFilterCount(Long filterCount) {
        this.filterCount = filterCount;
    }

    public Long getWriteCount() {
        return writeCount;
    }

    public void setWriteCount(Long writeCount) {
        this.writeCount = writeCount;
    }

    public Long getReadSkipCount() {
        return readSkipCount;
    }

    public void setReadSkipCount(Long readSkipCount) {
        this.readSkipCount = readSkipCount;
    }

    public Long getWriteSkipCount() {
        return writeSkipCount;
    }

    public void setWriteSkipCount(Long writeSkipCount) {
        this.writeSkipCount = writeSkipCount;
    }

    public Long getProcessSkipCount() {
        return processSkipCount;
    }

    public void setProcessSkipCount(Long processSkipCount) {
        this.processSkipCount = processSkipCount;
    }

    public Long getRollbackCount() {
        return rollbackCount;
    }

    public void setRollbackCount(Long rollbackCount) {
        this.rollbackCount = rollbackCount;
    }

    public String getExitCode() {
        return exitCode;
    }

    public void setExitCode(String exitCode) {
        this.exitCode = exitCode;
    }

    public String getExitMessage() {
        return exitMessage;
    }

    public void setExitMessage(String exitMessage) {
        this.exitMessage = exitMessage;
    }

    public ZonedDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(ZonedDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public Long getBatchJobExecutionId() {
        return batchJobExecutionId;
    }

    public void setBatchJobExecutionId(Long batchJobExecutionId) {
        this.batchJobExecutionId = batchJobExecutionId;
    }

    public List<BatchStepExecutionContextDTO> getBatchStepExecutionContexts() {
        return BatchStepExecutionContexts;
    }

    public void setBatchStepExecutionContexts(List<BatchStepExecutionContextDTO> batchStepExecutionContexts) {
        BatchStepExecutionContexts = batchStepExecutionContexts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        BatchStepExecutionDTO batchStepExecutionDTO = (BatchStepExecutionDTO) o;
        if (batchStepExecutionDTO.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), batchStepExecutionDTO.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "BatchStepExecutionDTO{" +
            "id=" + getId() +
            ", version=" + getVersion() +
            ", stepName='" + getStepName() + "'" +
            ", startTime='" + getStartTime() + "'" +
            ", endTime='" + getEndTime() + "'" +
            ", status='" + getStatus() + "'" +
            ", commitCount=" + getCommitCount() +
            ", readCount=" + getReadCount() +
            ", filterCount=" + getFilterCount() +
            ", writeCount=" + getWriteCount() +
            ", readSkipCount=" + getReadSkipCount() +
            ", writeSkipCount=" + getWriteSkipCount() +
            ", processSkipCount=" + getProcessSkipCount() +
            ", rollbackCount=" + getRollbackCount() +
            ", exitCode='" + getExitCode() + "'" +
            ", exitMessage='" + getExitMessage() + "'" +
            ", lastUpdated='" + getLastUpdated() + "'" +
            ", batchJobExecutionId=" + getBatchJobExecutionId() +
            "}";
    }
}
