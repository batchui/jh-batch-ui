package com.ag04.batchui.service.dto;

import com.ag04.batchui.jackson.CroatianBigDecimalDeserializer;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.math.BigDecimal;
import java.time.LocalDate;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ExchangeRateDTO {

    @JsonDeserialize(using = CroatianBigDecimalDeserializer.class, as = BigDecimal.class)
    @JsonProperty("Srednji za devize")
    private BigDecimal rate;

    @JsonProperty("Valuta")
    private String currencyCode;

    private LocalDate date;

    public ExchangeRateDTO() {
        //
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public String getCurrencyCode() {
        return currencyCode;
    }

    public void setCurrencyCode(String currencyCode) {
        this.currencyCode = currencyCode;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }
}
