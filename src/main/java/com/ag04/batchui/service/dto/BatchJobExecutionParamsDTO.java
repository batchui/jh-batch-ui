package com.ag04.batchui.service.dto;

import java.time.ZonedDateTime;
import javax.validation.constraints.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link com.ag04.batchui.domain.BatchJobExecutionParams} entity.
 */
public class BatchJobExecutionParamsDTO implements Serializable {

    @NotNull
    @Size(max = 6)
    private String typeCd;

    @NotNull
    @Size(max = 100)
    private String keyName;

    @Size(max = 250)
    private String stringVal;

    private ZonedDateTime dateVal;

    private Long longVal;

    private Double doubleVal;

    @NotNull
    @Size(max = 1)
    private String identifying;

    public String getTypeCd() {
        return typeCd;
    }

    public void setTypeCd(String typeCd) {
        this.typeCd = typeCd;
    }

    public String getKeyName() {
        return keyName;
    }

    public void setKeyName(String keyName) {
        this.keyName = keyName;
    }

    public String getStringVal() {
        return stringVal;
    }

    public void setStringVal(String stringVal) {
        this.stringVal = stringVal;
    }

    public ZonedDateTime getDateVal() {
        return dateVal;
    }

    public void setDateVal(ZonedDateTime dateVal) {
        this.dateVal = dateVal;
    }

    public Long getLongVal() {
        return longVal;
    }

    public void setLongVal(Long longVal) {
        this.longVal = longVal;
    }

    public Double getDoubleVal() {
        return doubleVal;
    }

    public void setDoubleVal(Double doubleVal) {
        this.doubleVal = doubleVal;
    }

    public String getIdentifying() {
        return identifying;
    }

    public void setIdentifying(String identifying) {
        this.identifying = identifying;
    }

    @Override
    public String toString() {
        return "BatchJobExecutionParamsDTO{" +
            "typeCd='" + getTypeCd() + "'" +
            ", keyName='" + getKeyName() + "'" +
            ", stringVal='" + getStringVal() + "'" +
            ", dateVal='" + getDateVal() + "'" +
            ", longVal=" + getLongVal() +
            ", doubleVal=" + getDoubleVal() +
            ", identifying='" + getIdentifying() + "'" +
            "}";
    }
}
