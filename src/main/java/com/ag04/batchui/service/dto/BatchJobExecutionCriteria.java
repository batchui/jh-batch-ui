package com.ag04.batchui.service.dto;

import java.io.Serializable;
import java.util.Objects;
import io.github.jhipster.service.Criteria;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;
import io.github.jhipster.service.filter.InstantFilter;

/**
 * Criteria class for the {@link com.ag04.batchui.domain.BatchJobExecution} entity. This class is used
 * in {@link com.ag04.batchui.web.rest.BatchJobExecutionResource} to receive all the possible filtering options from
 * the Http GET request parameters.
 * For example the following could be a valid request:
 * {@code /batch-job-executions?id.greaterThan=5&attr1.contains=something&attr2.specified=false}
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class BatchJobExecutionCriteria implements Serializable, Criteria {

    private static final long serialVersionUID = 1L;

    private LongFilter id;

    private LongFilter jobExecutionId;

    private LongFilter version;

    private InstantFilter createTime;

    private InstantFilter startTime;

    private InstantFilter endTime;

    private StringFilter status;

    private StringFilter exitCode;

    private StringFilter exitMessage;

    private InstantFilter lastUpdated;

    private StringFilter jobConfigurationLocation;

    private LongFilter batchStepExecutionId;

    private LongFilter batchJobInstanceId;

    private StringFilter jobName;

    public BatchJobExecutionCriteria() {
    }

    public BatchJobExecutionCriteria(BatchJobExecutionCriteria other) {
        this.id = other.id == null ? null : other.id.copy();
        this.jobExecutionId = other.jobExecutionId == null ? null : other.jobExecutionId.copy();
        this.version = other.version == null ? null : other.version.copy();
        this.createTime = other.createTime == null ? null : other.createTime.copy();
        this.startTime = other.startTime == null ? null : other.startTime.copy();
        this.endTime = other.endTime == null ? null : other.endTime.copy();
        this.status = other.status == null ? null : other.status.copy();
        this.exitCode = other.exitCode == null ? null : other.exitCode.copy();
        this.exitMessage = other.exitMessage == null ? null : other.exitMessage.copy();
        this.lastUpdated = other.lastUpdated == null ? null : other.lastUpdated.copy();
        this.jobConfigurationLocation = other.jobConfigurationLocation == null ? null : other.jobConfigurationLocation.copy();
        this.batchStepExecutionId = other.batchStepExecutionId == null ? null : other.batchStepExecutionId.copy();
        this.batchJobInstanceId = other.batchJobInstanceId == null ? null : other.batchJobInstanceId.copy();
        this.jobName = other.jobName == null ? null : other.jobName.copy();
    }

    @Override
    public BatchJobExecutionCriteria copy() {
        return new BatchJobExecutionCriteria(this);
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public LongFilter getJobExecutionId() {
        return jobExecutionId;
    }

    public void setJobExecutionId(LongFilter jobExecutionId) {
        this.jobExecutionId = jobExecutionId;
    }

    public LongFilter getVersion() {
        return version;
    }

    public void setVersion(LongFilter version) {
        this.version = version;
    }

    public InstantFilter getCreateTime() {
        return createTime;
    }

    public void setCreateTime(InstantFilter createTime) {
        this.createTime = createTime;
    }

    public InstantFilter getStartTime() {
        return startTime;
    }

    public void setStartTime(InstantFilter startTime) {
        this.startTime = startTime;
    }

    public InstantFilter getEndTime() {
        return endTime;
    }

    public void setEndTime(InstantFilter endTime) {
        this.endTime = endTime;
    }

    public StringFilter getStatus() {
        return status;
    }

    public void setStatus(StringFilter status) {
        this.status = status;
    }

    public StringFilter getExitCode() {
        return exitCode;
    }

    public void setExitCode(StringFilter exitCode) {
        this.exitCode = exitCode;
    }

    public StringFilter getExitMessage() {
        return exitMessage;
    }

    public void setExitMessage(StringFilter exitMessage) {
        this.exitMessage = exitMessage;
    }

    public InstantFilter getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(InstantFilter lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public StringFilter getJobConfigurationLocation() {
        return jobConfigurationLocation;
    }

    public void setJobConfigurationLocation(StringFilter jobConfigurationLocation) {
        this.jobConfigurationLocation = jobConfigurationLocation;
    }

    public LongFilter getBatchStepExecutionId() {
        return batchStepExecutionId;
    }

    public void setBatchStepExecutionId(LongFilter batchStepExecutionId) {
        this.batchStepExecutionId = batchStepExecutionId;
    }

    public LongFilter getBatchJobInstanceId() {
        return batchJobInstanceId;
    }

    public void setBatchJobInstanceId(LongFilter batchJobInstanceId) {
        this.batchJobInstanceId = batchJobInstanceId;
    }

    public StringFilter getJobName() {
        return jobName;
    }

    public void setJobName(StringFilter jobName) {
        this.jobName = jobName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final BatchJobExecutionCriteria that = (BatchJobExecutionCriteria) o;
        return
            Objects.equals(id, that.id) &&
            Objects.equals(jobExecutionId, that.jobExecutionId) &&
            Objects.equals(version, that.version) &&
            Objects.equals(createTime, that.createTime) &&
            Objects.equals(startTime, that.startTime) &&
            Objects.equals(endTime, that.endTime) &&
            Objects.equals(status, that.status) &&
            Objects.equals(exitCode, that.exitCode) &&
            Objects.equals(exitMessage, that.exitMessage) &&
            Objects.equals(lastUpdated, that.lastUpdated) &&
            Objects.equals(jobConfigurationLocation, that.jobConfigurationLocation) &&
            Objects.equals(batchStepExecutionId, that.batchStepExecutionId) &&
            Objects.equals(batchJobInstanceId, that.batchJobInstanceId) &&
            Objects.equals(jobName, that.jobName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
        id,
        jobExecutionId,
        version,
        createTime,
        startTime,
        endTime,
        status,
        exitCode,
        exitMessage,
        lastUpdated,
        jobConfigurationLocation,
        batchStepExecutionId,
        batchJobInstanceId,
        jobName
        );
    }

    @Override
    public String toString() {
        return "BatchJobExecutionCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (jobExecutionId != null ? "jobExecutionId=" + jobExecutionId + ", " : "") +
                (version != null ? "version=" + version + ", " : "") +
                (createTime != null ? "createTime=" + createTime + ", " : "") +
                (startTime != null ? "startTime=" + startTime + ", " : "") +
                (endTime != null ? "endTime=" + endTime + ", " : "") +
                (status != null ? "status=" + status + ", " : "") +
                (exitCode != null ? "exitCode=" + exitCode + ", " : "") +
                (exitMessage != null ? "exitMessage=" + exitMessage + ", " : "") +
                (lastUpdated != null ? "lastUpdated=" + lastUpdated + ", " : "") +
                (jobConfigurationLocation != null ? "jobConfigurationLocation=" + jobConfigurationLocation + ", " : "") +
                (batchStepExecutionId != null ? "batchStepExecutionId=" + batchStepExecutionId + ", " : "") +
                (batchJobInstanceId != null ? "batchJobInstanceId=" + batchJobInstanceId + ", " : "") +
                (jobName != null ? "jobName=" + jobName + ", " : "") +
            "}";
    }

}
