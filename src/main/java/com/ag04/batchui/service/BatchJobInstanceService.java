package com.ag04.batchui.service;

import com.ag04.batchui.service.dto.BatchJobInstanceDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.ag04.batchui.domain.BatchJobInstance}.
 */
public interface BatchJobInstanceService {

    /**
     * Save a batchJobInstance.
     *
     * @param batchJobInstanceDTO the entity to save.
     * @return the persisted entity.
     */
    BatchJobInstanceDTO save(BatchJobInstanceDTO batchJobInstanceDTO);

    /**
     * Get all the batchJobInstances.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BatchJobInstanceDTO> findAll(Pageable pageable);

    /**
     * Get the "id" batchJobInstance.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BatchJobInstanceDTO> findOne(Long id);

    /**
     * Delete the "id" batchJobInstance.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    List<String> getAllJobNames();

    void deleteBulk(Long[] ids);
}
