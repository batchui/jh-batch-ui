package com.ag04.batchui.service;

import com.ag04.batchui.service.dto.BatchJobExecutionDTO;
import com.ag04.batchui.service.dto.JobExecutionStatusReport;
import jdk.vm.ci.meta.Local;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.ag04.batchui.domain.BatchJobExecution}.
 */
public interface BatchJobExecutionService {

    /**
     * Save a batchJobExecution.
     *
     * @param batchJobExecutionDTO the entity to save.
     * @return the persisted entity.
     */
    BatchJobExecutionDTO save(BatchJobExecutionDTO batchJobExecutionDTO);

    /**
     * Get all the batchJobExecutions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<BatchJobExecutionDTO> findAll(Pageable pageable);

    /**
     * Get the "id" batchJobExecution.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BatchJobExecutionDTO> findOne(Long id);

    /**
     * Delete the "id" batchJobExecution.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    /**
     *
     * @param from
     * @param to
     * @return
     */
    JobExecutionStatusReport statusReport(LocalDate from, LocalDate to);

    /**
     *
     * @param targetDate
     * @return
     */
    JobExecutionStatusReport statusReport(LocalDate targetDate);

    /**
     * Creates report for the last 24 hours
     * @return
     */
    JobExecutionStatusReport statusReport();

}
