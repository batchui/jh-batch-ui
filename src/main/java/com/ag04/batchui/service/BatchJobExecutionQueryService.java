package com.ag04.batchui.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import com.ag04.batchui.service.mapper.BatchJobExecutionListMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.ag04.batchui.domain.BatchJobExecution;
import com.ag04.batchui.domain.*; // for static metamodels
import com.ag04.batchui.repository.BatchJobExecutionRepository;
import com.ag04.batchui.service.dto.BatchJobExecutionCriteria;
import com.ag04.batchui.service.dto.BatchJobExecutionDTO;
import com.ag04.batchui.service.mapper.BatchJobExecutionMapper;

/**
 * Service for executing complex queries for {@link BatchJobExecution} entities in the database.
 * The main input is a {@link BatchJobExecutionCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link BatchJobExecutionDTO} or a {@link Page} of {@link BatchJobExecutionDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class BatchJobExecutionQueryService extends QueryService<BatchJobExecution> {

    private final Logger log = LoggerFactory.getLogger(BatchJobExecutionQueryService.class);

    private final BatchJobExecutionRepository batchJobExecutionRepository;

    private final BatchJobExecutionListMapper batchJobExecutionListMapper;

    public BatchJobExecutionQueryService(BatchJobExecutionRepository batchJobExecutionRepository, BatchJobExecutionListMapper batchJobExecutionListMapper) {
        this.batchJobExecutionRepository = batchJobExecutionRepository;
        this.batchJobExecutionListMapper = batchJobExecutionListMapper;
    }

    /**
     * Return a {@link List} of {@link BatchJobExecutionDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<BatchJobExecutionDTO> findByCriteria(BatchJobExecutionCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<BatchJobExecution> specification = createSpecification(criteria);
        return batchJobExecutionListMapper.toDto(batchJobExecutionRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link BatchJobExecutionDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<BatchJobExecutionDTO> findByCriteria(BatchJobExecutionCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<BatchJobExecution> specification = createSpecification(criteria);
        return batchJobExecutionRepository.findAll(specification, page)
            .map(batchJobExecutionListMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(BatchJobExecutionCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<BatchJobExecution> specification = createSpecification(criteria);
        return batchJobExecutionRepository.count(specification);
    }

    /**
     * Function to convert {@link BatchJobExecutionCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<BatchJobExecution> createSpecification(BatchJobExecutionCriteria criteria) {
        Specification<BatchJobExecution> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), BatchJobExecution_.id));
            }
            if (criteria.getVersion() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getVersion(), BatchJobExecution_.version));
            }
            if (criteria.getCreateTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getCreateTime(), BatchJobExecution_.createTime));
            }
            if (criteria.getStartTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getStartTime(), BatchJobExecution_.startTime));
            }
            if (criteria.getEndTime() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getEndTime(), BatchJobExecution_.endTime));
            }
            if (criteria.getStatus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getStatus(), BatchJobExecution_.status));
            }
            if (criteria.getExitCode() != null) {
                specification = specification.and(buildStringSpecification(criteria.getExitCode(), BatchJobExecution_.exitCode));
            }
            if (criteria.getExitMessage() != null) {
                specification = specification.and(buildStringSpecification(criteria.getExitMessage(), BatchJobExecution_.exitMessage));
            }
            if (criteria.getLastUpdated() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getLastUpdated(), BatchJobExecution_.lastUpdated));
            }
            if (criteria.getJobConfigurationLocation() != null) {
                specification = specification.and(buildStringSpecification(criteria.getJobConfigurationLocation(), BatchJobExecution_.jobConfigurationLocation));
            }
            if (criteria.getBatchStepExecutionId() != null) {
                specification = specification.and(buildSpecification(criteria.getBatchStepExecutionId(),
                    root -> root.join(BatchJobExecution_.batchStepExecutions, JoinType.LEFT).get(BatchStepExecution_.id)));
            }
            if (criteria.getBatchJobInstanceId() != null) {
                specification = specification.and(buildSpecification(criteria.getBatchJobInstanceId(),
                    root -> root.join(BatchJobExecution_.batchJobInstance, JoinType.LEFT).get(BatchJobInstance_.id)));
            }
            if (criteria.getJobName() != null) {
                specification = specification.and(buildSpecification(criteria.getJobName(),
                        root -> root.join(BatchJobExecution_.batchJobInstance, JoinType.LEFT).get(BatchJobInstance_.jobName)));
            }
        }
        return specification;
    }
}
