package com.ag04.batchui.service;

import com.ag04.batchui.service.dto.BatchJobExecutionParamsDTO;
import com.ag04.batchui.service.dto.StartJobCommandDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.ag04.batchui.dbqueue.domain.StartJobCommand}.
 */
public interface StartJobCommandService {

    Long scheduleNew(String jobBeanName, List<BatchJobExecutionParamsDTO> paramsList) throws Exception;

    /**
     * Save a startJobCommand.
     *
     * @param startJobCommandDTO the entity to save.
     * @return the persisted entity.
     */
    StartJobCommandDTO save(StartJobCommandDTO startJobCommandDTO);

    /**
     * Get all the startJobCommands.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<StartJobCommandDTO> findAll(Pageable pageable);

    /**
     * Get the "id" startJobCommand.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<StartJobCommandDTO> findOne(Long id);

    /**
     * Delete the "id" startJobCommand.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
