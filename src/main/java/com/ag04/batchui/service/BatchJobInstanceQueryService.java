package com.ag04.batchui.service;

import java.util.List;

import javax.persistence.criteria.JoinType;

import com.ag04.batchui.service.mapper.BatchJobInstanceListMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.ag04.batchui.domain.BatchJobInstance;
import com.ag04.batchui.domain.*; // for static metamodels
import com.ag04.batchui.repository.BatchJobInstanceRepository;
import com.ag04.batchui.service.dto.BatchJobInstanceCriteria;
import com.ag04.batchui.service.dto.BatchJobInstanceDTO;
import com.ag04.batchui.service.mapper.BatchJobInstanceMapper;

/**
 * Service for executing complex queries for {@link BatchJobInstance} entities in the database.
 * The main input is a {@link BatchJobInstanceCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link BatchJobInstanceDTO} or a {@link Page} of {@link BatchJobInstanceDTO} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class BatchJobInstanceQueryService extends QueryService<BatchJobInstance> {

    private final Logger log = LoggerFactory.getLogger(BatchJobInstanceQueryService.class);

    private final BatchJobInstanceRepository batchJobInstanceRepository;

    //private final BatchJobInstanceMapper batchJobInstanceMapper;

    private final BatchJobInstanceListMapper batchJobInstanceListMapper;

    public BatchJobInstanceQueryService(
            BatchJobInstanceRepository batchJobInstanceRepository,
            BatchJobInstanceListMapper batchJobInstanceListMapper
    ) {
        this.batchJobInstanceRepository = batchJobInstanceRepository;
        this.batchJobInstanceListMapper = batchJobInstanceListMapper;
    }

    /**
     * Return a {@link List} of {@link BatchJobInstanceDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<BatchJobInstanceDTO> findByCriteria(BatchJobInstanceCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<BatchJobInstance> specification = createSpecification(criteria);
        return batchJobInstanceListMapper.toDto(batchJobInstanceRepository.findAll(specification));
    }

    /**
     * Return a {@link Page} of {@link BatchJobInstanceDTO} which matches the criteria from the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<BatchJobInstanceDTO> findByCriteria(BatchJobInstanceCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<BatchJobInstance> specification = createSpecification(criteria);
        return batchJobInstanceRepository.findAll(specification, page)
            .map(batchJobInstanceListMapper::toDto);
    }

    /**
     * Return the number of matching entities in the database.
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the number of matching entities.
     */
    @Transactional(readOnly = true)
    public long countByCriteria(BatchJobInstanceCriteria criteria) {
        log.debug("count by criteria : {}", criteria);
        final Specification<BatchJobInstance> specification = createSpecification(criteria);
        return batchJobInstanceRepository.count(specification);
    }

    /**
     * Function to convert {@link BatchJobInstanceCriteria} to a {@link Specification}
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching {@link Specification} of the entity.
     */
    protected Specification<BatchJobInstance> createSpecification(BatchJobInstanceCriteria criteria) {
        Specification<BatchJobInstance> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getId(), BatchJobInstance_.id));
            }
            if (criteria.getVersion() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getVersion(), BatchJobInstance_.version));
            }
            if (criteria.getJobName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getJobName(), BatchJobInstance_.jobName));
            }
            if (criteria.getJobKey() != null) {
                specification = specification.and(buildStringSpecification(criteria.getJobKey(), BatchJobInstance_.jobKey));
            }
            if (criteria.getBatchJobExecutionId() != null) {
                specification = specification.and(buildSpecification(criteria.getBatchJobExecutionId(),
                    root -> root.join(BatchJobInstance_.batchJobExecutions, JoinType.LEFT).get(BatchJobExecution_.id)));
            }
        }
        return specification;
    }
}
