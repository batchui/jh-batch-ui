package com.ag04.batchui.service;

import com.ag04.batchui.service.dto.BatchStepExecutionDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.ag04.batchui.domain.BatchStepExecution}.
 */
public interface BatchStepExecutionService {

    /**
     * Save a batchStepExecution.
     *
     * @param batchStepExecutionDTO the entity to save.
     * @return the persisted entity.
     */
    BatchStepExecutionDTO save(BatchStepExecutionDTO batchStepExecutionDTO);

    /**
     * Get all the batchStepExecutions.
     *
     * @return the list of entities.
     */
    List<BatchStepExecutionDTO> findAll();

    /**
     * Get the "id" batchStepExecution.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<BatchStepExecutionDTO> findOne(Long id);

    /**
     * Delete the "id" batchStepExecution.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
