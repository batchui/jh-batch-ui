package com.ag04.batchui.service.impl;

import com.ag04.batchui.dbqueue.domain.StartJobCommand;
import com.ag04.batchui.dbqueue.repository.StartJobCommandRepository;
import com.ag04.batchui.service.StartJobCommandService;
import com.ag04.batchui.service.dto.BatchJobExecutionParamsDTO;
import com.ag04.batchui.service.dto.StartJobCommandDTO;
import com.ag04.batchui.service.mapper.StartJobCommandMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link StartJobCommand}.
 */
@Service
@Transactional
public class StartJobCommandServiceImpl implements StartJobCommandService {
    private final Logger log = LoggerFactory.getLogger(StartJobCommandServiceImpl.class);

    private final StartJobCommandRepository startJobCommandRepository;

    private final StartJobCommandMapper startJobCommandMapper;

    private final ObjectMapper objectMapper;

    public StartJobCommandServiceImpl(
            StartJobCommandRepository startJobCommandRepository,
            StartJobCommandMapper startJobCommandMapper,
            ObjectMapper objectMapper
    ) {
        this.startJobCommandRepository = startJobCommandRepository;
        this.startJobCommandMapper = startJobCommandMapper;
        this.objectMapper = objectMapper;
    }

    @Override
    public Long scheduleNew(String jobBeanName, List<BatchJobExecutionParamsDTO> paramsList) throws Exception {
        StartJobCommand sjc = new StartJobCommand();
        sjc.setJobName(jobBeanName);

        // convert paramsList to json
        String jobParams = objectMapper.writeValueAsString(paramsList);
        sjc.setJobParams(jobParams);

        sjc.getSendingState().scheduleNextAttempt(LocalDateTime.now());
        startJobCommandRepository.save(sjc);
        return sjc.getId();
    }

    /**
     * Save a startJobCommand.
     *
     * @param startJobCommandDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public StartJobCommandDTO save(StartJobCommandDTO startJobCommandDTO) {
        log.debug("Request to save StartJobCommand : {}", startJobCommandDTO);
        StartJobCommand startJobCommand = startJobCommandMapper.toEntity(startJobCommandDTO);
        startJobCommand = startJobCommandRepository.save(startJobCommand);
        return startJobCommandMapper.toDto(startJobCommand);
    }

    /**
     * Get all the startJobCommands.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<StartJobCommandDTO> findAll(Pageable pageable) {
        log.debug("Request to get all StartJobCommands");
        return startJobCommandRepository.findAll(pageable)
            .map(startJobCommandMapper::toDto);
    }

    /**
     * Get one startJobCommand by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<StartJobCommandDTO> findOne(Long id) {
        log.debug("Request to get StartJobCommand : {}", id);
        return startJobCommandRepository.findById(id)
            .map(startJobCommandMapper::toDto);
    }

    /**
     * Delete the startJobCommand by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete StartJobCommand : {}", id);
        startJobCommandRepository.deleteById(id);
    }
}
