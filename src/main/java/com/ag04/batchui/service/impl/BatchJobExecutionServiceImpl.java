package com.ag04.batchui.service.impl;

import com.ag04.batchui.repository.BatchJobInstanceRepository;
import com.ag04.batchui.service.BatchJobExecutionService;
import com.ag04.batchui.domain.BatchJobExecution;
import com.ag04.batchui.repository.BatchJobExecutionRepository;
import com.ag04.batchui.service.dto.BatchJobExecutionDTO;
import com.ag04.batchui.service.dto.JobExecutionStatusReport;
import com.ag04.batchui.service.dto.JobStatusCount;
import com.ag04.batchui.service.mapper.BatchJobExecutionListMapper;
import com.ag04.batchui.service.mapper.BatchJobExecutionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.*;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link BatchJobExecution}.
 */
@Service
public class BatchJobExecutionServiceImpl implements BatchJobExecutionService {

    private final Logger log = LoggerFactory.getLogger(BatchJobExecutionServiceImpl.class);

    private final BatchJobExecutionRepository batchJobExecutionRepository;

    private final BatchJobInstanceRepository batchJobInstanceRepository;

    private final BatchJobExecutionMapper batchJobExecutionMapper;

    private final BatchJobExecutionListMapper batchJobExecutionListMapper;

    private final CacheManager cacheManager;

    public BatchJobExecutionServiceImpl(
            BatchJobExecutionRepository batchJobExecutionRepository,
            BatchJobExecutionMapper batchJobExecutionMapper,
            BatchJobExecutionListMapper batchJobExecutionListMapper,
            BatchJobInstanceRepository batchJobInstanceRepository,
            CacheManager cacheManager
    ) {
        this.batchJobExecutionRepository = batchJobExecutionRepository;
        this.batchJobExecutionMapper = batchJobExecutionMapper;
        this.batchJobExecutionListMapper = batchJobExecutionListMapper;
        this.batchJobInstanceRepository = batchJobInstanceRepository;
        this.cacheManager = cacheManager;
    }

    /**
     * Save a batchJobExecution.
     *
     * @param batchJobExecutionDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    @Transactional
    public BatchJobExecutionDTO save(BatchJobExecutionDTO batchJobExecutionDTO) {
        log.debug("Request to save BatchJobExecution : {}", batchJobExecutionDTO);
        BatchJobExecution batchJobExecution = batchJobExecutionMapper.toEntity(batchJobExecutionDTO);
        batchJobExecution = batchJobExecutionRepository.save(batchJobExecution);
        return batchJobExecutionMapper.toDto(batchJobExecution);
    }

    /**
     * Get all the batchJobExecutions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<BatchJobExecutionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BatchJobExecutions");
        return batchJobExecutionRepository.findAll(pageable).map(batchJobExecutionListMapper::toDto);
    }

    /**
     * Get one batchJobExecution by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<BatchJobExecutionDTO> findOne(Long id) {
        log.debug("Request to get BatchJobExecution : {}", id);
        return batchJobExecutionRepository.findById(id)
            .map(batchJobExecutionMapper::toDto);
    }

    /**
     * Delete the batchJobExecution by id.
     *
     * @param id the id of the entity.
     */
    @Override
    @Transactional
    public void delete(Long id) {
        log.debug("Request to delete BatchJobExecution : {}", id);
        batchJobExecutionRepository.deleteById(id);

        // TODO: fine grade this
        cacheManager.getCache(com.ag04.batchui.domain.BatchJobInstance.class.getName()+ ".batchJobExecutions").invalidate();
        //cacheManager.getCache(com.ag04.batchui.domain.BatchJobInstance.class.getName()).invalidate();
    }

    @Override
    public JobExecutionStatusReport statusReport() {
        ZonedDateTime fromDateTime = ZonedDateTime.now().minusDays(1);
        log.debug("Generating status report for period between '{}' and now.", fromDateTime);

        JobExecutionStatusReport jobStatusReport24h = new JobExecutionStatusReport();
        List<String> jobNames = batchJobInstanceRepository.getAllJobNames();

        List<JobStatusCount> batchStatuses = batchJobExecutionRepository.getBatchStatusesCountFromDate(fromDateTime.toInstant());
        List<JobStatusCount> exitCodes = batchJobExecutionRepository.getExitCodesCountFromDate(fromDateTime.toInstant());

        jobStatusReport24h.setBatchStatuses(batchStatuses);
        jobStatusReport24h.setExitCodes(exitCodes);
        jobStatusReport24h.setJobCount(jobNames.size());
        return jobStatusReport24h;
    }

    @Override
    public JobExecutionStatusReport statusReport(LocalDate from, LocalDate to) {
        JobExecutionStatusReport jobStatusReport = new JobExecutionStatusReport();
        List<String> jobNames = batchJobInstanceRepository.getAllJobNames();

        Instant fromDateTime = from.atStartOfDay(ZoneId.systemDefault()).toInstant();
        Instant toDateTime = to.plusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant();

        List<JobStatusCount> batchStatuses = batchJobExecutionRepository.getBatchStatusesCount(fromDateTime, toDateTime);
        List<JobStatusCount> exitCodes = batchJobExecutionRepository.getExitCodesCount(fromDateTime, toDateTime);

        jobStatusReport.setBatchStatuses(batchStatuses);
        jobStatusReport.setExitCodes(exitCodes);
        jobStatusReport.setJobCount(jobNames.size());
        jobStatusReport.setFrom(from.atStartOfDay());
        jobStatusReport.setTo(to.plusDays(1).atStartOfDay());
        return jobStatusReport;
    }

    @Override
    public JobExecutionStatusReport statusReport(LocalDate targetDate) {
        JobExecutionStatusReport jobStatusReport = new JobExecutionStatusReport();
        List<String> jobNames = batchJobInstanceRepository.getAllJobNames();

        Instant fromDateTime = targetDate.atStartOfDay(ZoneId.systemDefault()).toInstant();
        Instant toDateTime = targetDate.plusDays(1).atStartOfDay(ZoneId.systemDefault()).toInstant();

        List<JobStatusCount> batchStatuses = batchJobExecutionRepository.getBatchStatusesCount(fromDateTime, toDateTime);
        List<JobStatusCount> exitCodes = batchJobExecutionRepository.getExitCodesCount(fromDateTime, toDateTime);

        jobStatusReport.setBatchStatuses(batchStatuses);
        jobStatusReport.setExitCodes(exitCodes);
        jobStatusReport.setJobCount(jobNames.size());
        jobStatusReport.setFrom(targetDate.atStartOfDay());
        jobStatusReport.setTo(targetDate.plusDays(1).atStartOfDay());
        return jobStatusReport;
    }
}
