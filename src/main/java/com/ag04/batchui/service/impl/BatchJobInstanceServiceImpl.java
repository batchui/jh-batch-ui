package com.ag04.batchui.service.impl;

import com.ag04.batchui.service.BatchJobInstanceService;
import com.ag04.batchui.domain.BatchJobInstance;
import com.ag04.batchui.repository.BatchJobInstanceRepository;
import com.ag04.batchui.service.dto.BatchJobInstanceDTO;
import com.ag04.batchui.service.mapper.BatchJobInstanceListMapper;
import com.ag04.batchui.service.mapper.BatchJobInstanceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link BatchJobInstance}.
 */
@Service
@Transactional
public class BatchJobInstanceServiceImpl implements BatchJobInstanceService {

    private final Logger log = LoggerFactory.getLogger(BatchJobInstanceServiceImpl.class);

    private final BatchJobInstanceRepository batchJobInstanceRepository;

    private final BatchJobInstanceMapper batchJobInstanceMapper;

    private final BatchJobInstanceListMapper batchJobInstanceListMapper;

    public BatchJobInstanceServiceImpl(BatchJobInstanceRepository batchJobInstanceRepository,
        BatchJobInstanceMapper batchJobInstanceMapper,
        BatchJobInstanceListMapper batchJobInstanceListMapper
    ) {
        this.batchJobInstanceRepository = batchJobInstanceRepository;
        this.batchJobInstanceMapper = batchJobInstanceMapper;
        this.batchJobInstanceListMapper = batchJobInstanceListMapper;
    }

    /**
     * Save a batchJobInstance.
     *
     * @param batchJobInstanceDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public BatchJobInstanceDTO save(BatchJobInstanceDTO batchJobInstanceDTO) {
        log.debug("Request to save BatchJobInstance : {}", batchJobInstanceDTO);
        BatchJobInstance batchJobInstance = batchJobInstanceMapper.toEntity(batchJobInstanceDTO);
        batchJobInstance = batchJobInstanceRepository.save(batchJobInstance);
        return batchJobInstanceMapper.toDto(batchJobInstance);
    }

    /**
     * Get all the batchJobInstances.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public Page<BatchJobInstanceDTO> findAll(Pageable pageable) {
        log.debug("Request to get all BatchJobInstances");
        return batchJobInstanceRepository.findAll(pageable)
            .map(batchJobInstanceListMapper::toDto);
    }

    /**
     * Get one batchJobInstance by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<BatchJobInstanceDTO> findOne(Long id) {
        log.debug("Request to get BatchJobInstance : {}", id);
        return batchJobInstanceRepository.findById(id)
            .map(batchJobInstanceMapper::toDto);
    }

    /**
     * Delete the batchJobInstance by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete BatchJobInstance : {}", id);
        batchJobInstanceRepository.deleteById(id);
    }

    @Override
    public List<String> getAllJobNames() {
        return batchJobInstanceRepository.getAllJobNames();
    }

    @Override
    public void deleteBulk(Long[] ids) {
        batchJobInstanceRepository.deleteByIdIn(ids);
    }
}
