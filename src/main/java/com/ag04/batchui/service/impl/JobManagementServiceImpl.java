package com.ag04.batchui.service.impl;

import com.ag04.batchui.service.JobManagementService;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

public class JobManagementServiceImpl implements JobManagementService {

    @Autowired
    private ApplicationContext ctx;

    private final JobLauncher jobLauncher;

    public JobManagementServiceImpl(JobLauncher jobLauncher) {
        this.jobLauncher = jobLauncher;
    }

    @Override
    public Long startNewJob(String jobBeanName, JobParameters jobParameters ) throws Exception {
        Job job = (Job) ctx.getBean(jobBeanName);

        JobExecution jobExecution = jobLauncher.run(job, jobParameters);
        return jobExecution.getId();
    }
}
