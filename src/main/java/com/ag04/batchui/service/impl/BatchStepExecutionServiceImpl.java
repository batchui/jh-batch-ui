package com.ag04.batchui.service.impl;

import com.ag04.batchui.service.BatchStepExecutionService;
import com.ag04.batchui.domain.BatchStepExecution;
import com.ag04.batchui.repository.BatchStepExecutionRepository;
import com.ag04.batchui.service.dto.BatchStepExecutionDTO;
import com.ag04.batchui.service.mapper.BatchStepExecutionMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link BatchStepExecution}.
 */
@Service
@Transactional
public class BatchStepExecutionServiceImpl implements BatchStepExecutionService {

    private final Logger log = LoggerFactory.getLogger(BatchStepExecutionServiceImpl.class);

    private final BatchStepExecutionRepository batchStepExecutionRepository;

    private final BatchStepExecutionMapper batchStepExecutionMapper;

    public BatchStepExecutionServiceImpl(BatchStepExecutionRepository batchStepExecutionRepository, BatchStepExecutionMapper batchStepExecutionMapper) {
        this.batchStepExecutionRepository = batchStepExecutionRepository;
        this.batchStepExecutionMapper = batchStepExecutionMapper;
    }

    /**
     * Save a batchStepExecution.
     *
     * @param batchStepExecutionDTO the entity to save.
     * @return the persisted entity.
     */
    @Override
    public BatchStepExecutionDTO save(BatchStepExecutionDTO batchStepExecutionDTO) {
        log.debug("Request to save BatchStepExecution : {}", batchStepExecutionDTO);
        BatchStepExecution batchStepExecution = batchStepExecutionMapper.toEntity(batchStepExecutionDTO);
        batchStepExecution = batchStepExecutionRepository.save(batchStepExecution);
        return batchStepExecutionMapper.toDto(batchStepExecution);
    }

    /**
     * Get all the batchStepExecutions.
     *
     * @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<BatchStepExecutionDTO> findAll() {
        log.debug("Request to get all BatchStepExecutions");
        return batchStepExecutionRepository.findAll().stream()
            .map(batchStepExecutionMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    /**
     * Get one batchStepExecution by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<BatchStepExecutionDTO> findOne(Long id) {
        log.debug("Request to get BatchStepExecution : {}", id);
        return batchStepExecutionRepository.findById(id)
            .map(batchStepExecutionMapper::toDto);
    }

    /**
     * Delete the batchStepExecution by id.
     *
     * @param id the id of the entity.
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete BatchStepExecution : {}", id);
        batchStepExecutionRepository.deleteById(id);
    }
}
