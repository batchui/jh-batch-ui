package com.ag04.batchui.repository;

import com.ag04.batchui.domain.BatchJobInstance;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the BatchJobInstance entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BatchJobInstanceRepository extends JpaRepository<BatchJobInstance, Long>, JpaSpecificationExecutor<BatchJobInstance> {

    @Query("SELECT DISTINCT ji.jobName FROM BatchJobInstance ji")
    List<String> getAllJobNames();

    @Modifying
    @Query("DELETE FROM BatchJobInstance ji WHERE ji.id = (SELECT bje.batchJobInstance.id from BatchJobExecution bje WHERE bje.id=:jobId)")
    void deleteByJobExecutionId(@Param("jobId") Long jobId);

    void deleteByIdIn(Long[] ids);

}
