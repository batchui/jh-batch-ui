package com.ag04.batchui.repository;

import com.ag04.batchui.domain.BatchJobExecution;

import com.ag04.batchui.service.dto.JobStatusCount;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Spring Data  repository for the BatchJobExecution entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BatchJobExecutionRepository extends JpaRepository<BatchJobExecution, Long>, JpaSpecificationExecutor<BatchJobExecution> {

    /**
     * Counts number of JobExecutions associated with the jobInstance of this JobExecution.
     *
     * @param jobId
     * @return
     */
    @Query("select count(*) from BatchJobExecution bje where bje.batchJobInstance.id = (select bje2.batchJobInstance.id from BatchJobExecution bje2 where bje2.id=:jobId)")
    int countInBatchJobInstance(@Param("jobId") Long jobId);

    @Query("select bje.batchJobInstance.id from BatchJobExecution bje where bje.id=:jobId")
    Long selectJobInstanceId(@Param("jobId") Long jobId);

    @Query("select new com.ag04.batchui.service.dto.JobStatusCount(count(bje.exitCode), bje.exitCode) from BatchJobExecution bje where bje.createTime >= :fromDate group by bje.exitCode")
    List<JobStatusCount> getExitCodesCountFromDate(@Param("fromDate") Instant fromDate);

    @Query("select new com.ag04.batchui.service.dto.JobStatusCount(count(bje.status), bje.status) from BatchJobExecution bje where bje.createTime >= :fromDate group by bje.status")
    List<JobStatusCount> getBatchStatusesCountFromDate(@Param("fromDate") Instant fromDate);

    @Query("select new com.ag04.batchui.service.dto.JobStatusCount(count(bje.exitCode), bje.exitCode) from BatchJobExecution bje where bje.createTime >= :fromDate AND bje.createTime < :toDate group by bje.exitCode")
    List<JobStatusCount> getExitCodesCount(@Param("fromDate") Instant fromDate, @Param("toDate") Instant toDate);

    @Query("select new com.ag04.batchui.service.dto.JobStatusCount(count(bje.status), bje.status) from BatchJobExecution bje where bje.createTime >= :fromDate AND bje.createTime < :toDate group by bje.status")
    List<JobStatusCount> getBatchStatusesCount(@Param("fromDate") Instant fromDate, @Param("toDate") Instant toDate);

}
