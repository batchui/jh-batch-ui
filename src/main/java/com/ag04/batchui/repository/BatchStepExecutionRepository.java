package com.ag04.batchui.repository;

import com.ag04.batchui.domain.BatchStepExecution;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the BatchStepExecution entity.
 */
@SuppressWarnings("unused")
@Repository
public interface BatchStepExecutionRepository extends JpaRepository<BatchStepExecution, Long> {
}
