# Spring BatchUi

<img style="float: right;" src="./support/assets/spring-batch-logo.png"> Welcome to the (Spring) **Batch UI**, admin front end application for **Spring Batch** Job Executions. It offers the following functionalities:

- Browse and search through list of Job Execution(s)
- Detailed view of particular Job Execution
- Browse through the list of Job Instances
- Detailed view of a particular Job Instance
- Re-running of Job Executions
- Web form to start a new Job Execution
- Bulk delete of Job Instances (Not yet available!)

To use BatchUI you need to (1) deploy it and (2) connect it to a database of the target application whose Spring Batch Job activities you would like to monitor.
For more details see the **Configure and setup BatchUI** section of the documentation bellow.

For more on Spring Batch (especially its data model) see the official documentation at: [https://docs.spring.io/spring-batch/docs/4.2.x/reference/html/index.html](https://docs.spring.io/spring-batch/docs/4.2.x/reference/html/index.html)

Spring **BatchUI** is Java/Spring application with Angular 8 frontend, and is provided as is, feel free to change it to suit your needs.
For more on how to customize it see the general guidelines in **Further (Custom) Develoment** section.

## Supported/Tested databases

BatchUI application was tested on the following databases:

- Postgres
- H2
- Mssql (2012 Server)

Furthermore, BatchUi docker image comes prepacked with JDBC drivers for all of the above database.

BatchUI should work with all major relational databases supported by Spring Batch and JHipster.
In order to do so their JDBC drivers needs to added to gradle.build and, optionally, if you would like for BatchUI to build Jhipster tables on which it relies, adecquate liquibase changesets needs to be added.

## Table of Contents

- [Quick run]
- [Building BatchUI](./BUILDING.md)
- [Configure and setup BatchUI](./support/docs/SETUP.md)
- [Furhter (custom) development](./support/docs/DEVELOPMENT.md)
- [GitLab CI Setup](./support/docs/GITLAB-CI.md)

### Quick run

More detailed instructions on how to run and setup can be found in "Configure and setup batchUI" sections.

Pull the image from the docker Hub.

```bash
docker pul ...
```

yml file

```yaml
version: '2'
services:
  batchui-app:
    image: ag04/batchui-app:latest
    network_mode: 'host' # uncomment this if on linux you want to connect to local instance
    environment:
      _JAVA_OPTIONS: '-Xmx512m -Xms256m'
      SPRING_PROFILES_ACTIVE: 'prod,swagger'
      MANAGEMENT_METRICS_EXPORT_PROMETHEUS_ENABLED: 'true'
      SERVER_SERVLET_CONTEXT_PATH: '/'
      SERVER_PORT: '9080'
      JHIPSTER_SLEEP: '1' # gives time for other services to boot before the application
      DATASOURCE_URL: jdbc:sqlserver://localhost:1433;database=dbname
      DB_USER: 'user'
      DB_PWD: 'userpwd'
      DB_NAME: 'dbname'
      DB_SCHEMA: 'myschema'
      JPA_DATABASE_PLATFORM: 'org.hibernate.dialect.SQLServer2012Dialect'
      JPA_DATABASE: 'SQL_SERVER'
      MAIL_HOST: 'localhost'
      MAIL_PORT: 25
      MAIL_USERNAME: ''
      MAIL_PWD: ''
      APP_BASE_URL: 'localhost:9080/'
      LIQUIBASE_ENABLED: 'true'
      DBQUEUE_ENABLED: 'true'
      DBQUEUE_CONSUMER_ENABLED: 'false'
    ports:
      - 9080:9080
```

This will run BatchUI application connect it to your target database and build JobCommand table there to enable integration of BatchUI with your batch Jobs.

## Credits

- Domagoj Madunić (domagoj.madunic@ag04.com)
