# Configure and Setup BatchUI

## Running a local instance

- configure database
- configure IDE / environment vars

## Integrate BatchUI with an existing JHipster application
0. Stop target application
1. Configure BatchUI database connection (see bellow) to access target Jhipster app database.
2. Configure liquibase context based on Database setup
3. Run BatchUi application sp that it creates all necessary tables
4. Add **batchui-job-queue.jar** to an existing JHipster application
5. Add the following parameters to the application config files:
```yml
batchui:
  dbqueue:
    consumer:
      enabled: true
      polling-interval: 30 # poll interval in seconds
      polling-items-limit: 100
```
5. Run target application

## Database setup
The following two tasks should be done either by target application or by BatchUI:

**1. Create Jhipster default tables**
* jhi_user
* jhi_authority
* jhi_user_authority

**2. Create Spring batch tables**

### Liquibase contexts:
The following Liquibase context allow for flexible BatchUI configuration depending on the above setup.

- jh-initdb - will initialize JHipster tables
- batch-postgres : will create all spring batch tables for Postgres database
- batch-h2
- batch-mssql
- batch-mysql
- (all other databases: hsqldb, db2, oracle10g, sybase, ...)

## Running a docker image

Image is published in the docker hub registry.

Pull it
```
docker pull ag04/batchui-app:latest
```
For this image to be used several environment variables had to be configured.

### Environment variables:

The easiest way to do it is to write your own docker-compose yaml file.

Example of docker-compose yaml file:

```yaml
version: '2'
services:
  batchui-app:
    image: ag04/batchui-app:latest
    #network_mode: "host" uncomment this if on linux you want to connect to local instance
    environment:
      _JAVA_OPTIONS: '-Xmx512m -Xms256m'
      SPRING_PROFILES_ACTIVE: 'prod,batch-demo,swagger'
      MANAGEMENT_METRICS_EXPORT_PROMETHEUS_ENABLED: 'true'
      SERVER_SERVLET_CONTEXT_PATH: '/'
      JHIPSTER_SLEEP: '3' # gives time for other services to boot before the application
      DATASOURCE_URL: jdbc:postgresql://localhost:5432/batchui
      DB_USER: 'batchui'
      DB_PWD: 'batchui'
      DB_NAME: 'batchui'
      DB_SCHEMA: 'public'
      JPA_DATABASE_PLATFORM: 'io.github.jhipster.domain.util.FixedPostgreSQL10Dialect'
      JPA_DATABASE: 'POSTGRESQL'
      MAIL_HOST: 'localhost'
      MAIL_PORT: 25
      MAIL_USERNAME: ''
      MAIL_PWD: ''
      APP_BASE_URL: 'localhost:8080/'
      LIQUIBASE_ENABLED: 'true'
      LIQUIBASE_CONTEXTS: 'batch-postgresql'
    ports:
      - 8080:8080
```

To run it simply issue the following command:

```bash
docker-compose -f batchui.yml
```


## -

## -
