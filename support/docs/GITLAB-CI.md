# GitLab CI Configuration

Project also contains a simple .gitlab-ci.yml file that configures GitLab CI/CD pipeline for this project.
This document explains how to setup your GitLab repository for this CI/CD configuraton to work.

## GitLab Environment variables

For this build to work you need to configure the following GitLab variables:

### Sonar

- SONAR_HOST_URL (Url to your sonarqube instance where collected analitic data will be pushed, for example: https://sonar.ag04.io/)
- SONAR_TOKEN (Access token for your sonar instance)
  For more on generating Sonar token see the official Sonarqube docs.

If you would like to skip this step, simply delete entire **sonarqube-check** step.

### Docker Registry configuration

This CI/CD pipeline configuration also contains a **docker-push** step. This step is configured to build and push two docker images, one with the tag equal to the current project version and the other with the latest tag.

For this to work you need to configure the following environment variables on your GitLab repository:

- REGISTRY_URL (URL of the docker registry where you would like the images to be pushed, ie. registry.hub.docker.com)
- REGISTRY_USER (target docker registry username)
- REGISTRY_PWD (target docker registry password)

## Build features
Every push to the **master** branch will also end up in docker image with the 'latest' tag being pushed to the configured docker registry.

While, every push on the **release** branch will also end up un docker image with the tag equal to the project version pushed to the configured docker registry.  