
CREATE TABLE start_job_command (
	id bigint IDENTITY(1,1) NOT NULL,
	job_name varchar(80) NOT NULL,
	job_params varchar(MAX)  NULL,
	status varchar(255) NULL,
	next_attempt_time datetime NULL,
	attempt_count int NULL,
	last_attempt_time datetime NULL,
	last_attempt_error_message varchar(500) NULL,
	CONSTRAINT PK_START_JOB_COMMAND PRIMARY KEY (id)
) GO
 CREATE NONCLUSTERED INDEX idx_start_job_queue_polling_fields ON start_job_command (  next_attempt_time ASC  )
	 WITH (  PAD_INDEX = OFF ,FILLFACTOR = 100  ,SORT_IN_TEMPDB = OFF , IGNORE_DUP_KEY = OFF , STATISTICS_NORECOMPUTE = OFF , ONLINE = OFF , ALLOW_ROW_LOCKS = ON , ALLOW_PAGE_LOCKS = ON  )
	 ON [PRIMARY ]  GO